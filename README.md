# **부천 계남 고등학교 (Gyenam High School)** #

계남고등학교 어플리케이션의 **오픈소스 코드** 입니다.

Made By GSP - 계남고등학교 컴퓨터 동아리

실제 어플리케이션 : https://play.google.com/store/apps/details?id=com.gsp.gyenamhighschool

질문은 gspdev77@gmail.com 으로 해주시면 답변해드리겠습니다 :)

코드 정리가 아직 다 끝나지 않았는데 수정해서 업데이트 하도록 하겠습니다.

또한 이 프로젝트를 그대로 다운받아서 빌드하시면 오류, 팅김현상이 발생할 겁니다.
모두 다 그대로 쓰시지 말고 원하시는 부분만 수정해서 써주시면 됩니다 ㅎㅎ







## **Screen Shots** ##

![unnamed.png](https://bitbucket.org/repo/9p55kd8/images/392078038-unnamed.png)

![unnamed2.png](https://bitbucket.org/repo/9p55kd8/images/2624207749-unnamed2.png)

![unnamed3.png](https://bitbucket.org/repo/9p55kd8/images/56778252-unnamed3.png)


## **참고, 사용, 활용한 사이트 및 라이브러리** ##
(앱안의 오픈소스 라이센스에도 기재되어 있습니다)
(아래 자료들을 제작하신 분들 모두에게 감사드립니다)

**ViewPager Indicator**

  https://github.com/JakeWharton/ViewPagerIndicator


**PhotoView**

  https://github.com/chrisbanes/PhotoView


**Material Dialogs**

  https://github.com/afollestad/material-dialogs


**AppIntro**

  https://github.com/apl-devs/AppIntro


**Glide**

  https://github.com/bumptech/glide


**DateTimePicker**

  https://github.com/flavienlaurent/datetimepicker

**TedPermission**

  https://github.com/ParkSangGwon/TedPermission


**Firebase**

  https://firebase.google.com/


**Jericho HTML Parser**

  http://jericho.htmlparser.net/docs/index.html


**Android Support Library**

  com.android.support:appcompat-v7:23.1.1
  com.android.support:design:23.1.1
  com.android.support:cardview-v7:23.1.1
  com.android.support:recyclerview-v7:23.1.1
  com.android.support.suppot-v4:23.1.1


**Android Meal Library, WondangHighSchool**

  https://bitbucket.org/whdghks913/wondanghighschool
  http://itmir.tistory.com/486
  http://blog.naver.com/rimal/140186322365
  http://itmir.tistory.com/


**org.apache.http**

 org.jbundle.util.osgi.wrapped:org.jbundle.util.osgi.wrapped.org.apache.http.client:4.1.2