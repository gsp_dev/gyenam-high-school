package com.gsp.gyenamhighschool.Bap;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.gsp.gyenamhighschool.Bap.BapAdapter;
import com.gsp.gyenamhighschool.R;

import java.util.Calendar;

import com.gsp.gyenamhighschool.tool.BapTool;
import com.gsp.gyenamhighschool.tool.Preference;
import com.gsp.gyenamhighschool.tool.ProcessTask;
import com.gsp.gyenamhighschool.tool.Tools;

import static com.gsp.gyenamhighschool.tool.Tools.isOnline_Fixed;

public class BapActivity extends AppCompatActivity {

    ListView mListView;
    BapAdapter mAdapter;

    Calendar mCalendar;
    int YEAR, MONTH, DAY;
    int DAY_OF_WEEK;

    BapDownloadTask mProcessTask;
    MaterialDialog materialDialog;

    SwipeRefreshLayout mSwipeRefreshLayout;

    com.gsp.gyenamhighschool.tool.Preference prefTheme;
    int color;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefTheme = new com.gsp.gyenamhighschool.tool.Preference(getApplicationContext(),"theme"); // 테마 색상 코드를 불러옴

        color = prefTheme.getInt("theme",0); // 색상 코드를 정수형으로 변환

        if (color == Color.parseColor("#F44336")) // 빨간색이면
            setTheme(R.style.AppTheme_Red);
        if (color == Color.parseColor("#E91E63")) // 분홍색이면
            setTheme(R.style.AppTheme_Pink);
        if (color == Color.parseColor("#FFC107")) // 연한 주황색(노란색)이면
            setTheme(R.style.AppTheme_Yellow);
        if (color == Color.parseColor("#8BC34A")) // 연두색이면
            setTheme(R.style.AppTheme_Green);
        if (color == Color.parseColor("#03A9F4")) // 하늘색이면
            setTheme(R.style.AppTheme);
        if (color == Color.parseColor("#3F51B5")) // 남색이면
            setTheme(R.style.AppTheme_Indigo);

        setContentView(R.layout.activity_bap);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.mToolbar);
        setSupportActionBar(mToolbar);

        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setHomeButtonEnabled(true);
            mActionBar.setDisplayHomeAsUpEnabled(true);

            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        getCalendarInstance(true);
        mListView = (ListView) findViewById(R.id.mListView);
        mAdapter = new BapAdapter(this);
        mListView.setAdapter(mAdapter);

        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                BapListData mData = mAdapter.getItem(position);
      //          String mShareBapMsg = String.format(getString(R.string.shareBap_message_msg),
   //                     mData.mCalender, mData.mLunch, mData.mKcal_Lunch, mData.mDinner, mData.mKcal_Dinner);
//                withShare(mShareBapMsg);

                return true;
            }
        });

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.mSwipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getCalendarInstance(true);
                getBapList(true);

                if (mSwipeRefreshLayout.isRefreshing())
                    mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        if (color == Color.parseColor("#F44336"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.red);
        else if (color == Color.parseColor("#E91E63"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.pink);
        else if (color == Color.parseColor("#FFC107"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.yellow);
        else if (color == Color.parseColor("#8BC34A"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.green);
        else if (color == Color.parseColor("#03A9F4"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        else if (color == Color.parseColor("#3F51B5"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.indigo);
        else
            mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary); // 아무 지정도 안하면 기본(하늘색) 지정



        FloatingActionButton mFab = (FloatingActionButton) findViewById(R.id.mFab);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setCalenderBap();
            }
        });

        getBapList(true);


    }

    private void getCalendarInstance(boolean getInstance) {
        if (getInstance || (mCalendar == null))
            mCalendar = Calendar.getInstance();
        YEAR = mCalendar.get(Calendar.YEAR);
        MONTH = mCalendar.get(Calendar.MONTH);
        DAY = mCalendar.get(Calendar.DAY_OF_MONTH);
        DAY_OF_WEEK = mCalendar.get(Calendar.DAY_OF_WEEK);
    }

    private void getBapList(boolean isUpdate) {
        boolean isNetwork = Tools.isOnline(this);

        mAdapter.clearData();
        mAdapter.notifyDataSetChanged();

        getCalendarInstance(false);

        final Calendar mToday = Calendar.getInstance();
        final int TodayYear = mToday.get(Calendar.YEAR);
        final int TodayMonth = mToday.get(Calendar.MONTH);
        final int TodayDay = mToday.get(Calendar.DAY_OF_MONTH);

        // 이번주 월요일 날짜를 가져온다
        mCalendar.add(Calendar.DATE, 2 - DAY_OF_WEEK);

        for (int i = 0; i < 5; i++) {
            int year = mCalendar.get(Calendar.YEAR);
            int month = mCalendar.get(Calendar.MONTH);
            int day = mCalendar.get(Calendar.DAY_OF_MONTH);

            BapTool.restoreBapDateClass mData =
                    BapTool.restoreBapData(this, year, month, day);

            if (mData.isBlankDay) {
                if (isUpdate && isNetwork) {
                    if (isOnline_Fixed()) {

                        materialDialog = new MaterialDialog.Builder(this)
                                .title("로딩중")
                                .progress(false, 100, true)
                                .cancelable(true)
                                .show();

                        mProcessTask = new BapDownloadTask(this);
                        mProcessTask.execute(year, month, day);
                    } else {
                         materialDialog.dismiss();
                        Toast.makeText(this, "Wifi나 데이터가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                } else {
                    MaterialDialog.Builder builder = new MaterialDialog.Builder(this);
                    builder.title(R.string.no_network_title);
                    builder.titleColorRes(R.color.red);
                    builder.content(getString(R.string.no_network_msg));
                    builder.contentColorRes(R.color.red);
                    builder.positiveText(android.R.string.ok);
                    builder.positiveColorRes(R.color.red);
                    builder.show();
                }

                return;
            }

            // if day equals today
            if ((year == TodayYear) && (month == TodayMonth) && (day == TodayDay)) {
                mAdapter.addItem(mData.Calender, mData.DayOfTheWeek, mData.Lunch, mData.Kcal_Lunch, mData.Dinner, mData.Kcal_Dinner, true);
            } else {
                mAdapter.addItem(mData.Calender, mData.DayOfTheWeek, mData.Lunch, mData.Kcal_Lunch, mData.Dinner, mData.Kcal_Dinner, false);
            }

            mCalendar.add(Calendar.DATE, 1);
        }

        mCalendar.set(YEAR, MONTH, DAY);
        mAdapter.notifyDataSetChanged();
        setCurrentItem();
    }

    private void setCurrentItem() {
        if (DAY_OF_WEEK > 1 && DAY_OF_WEEK < 7) {
            if (mAdapter.getCount() == 5)
                mListView.setSelection(DAY_OF_WEEK - 2);
        } else {
            mListView.setSelection(0);
        }
    }

    public void setCalenderBap() {
        getCalendarInstance(false);

        int year = mCalendar.get(Calendar.YEAR);
        int month = mCalendar.get(Calendar.MONTH);
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
                mCalendar.set(year, month, day);
                getCalendarInstance(false);

                getBapList(true);
            }
        }, year, month, day, false);
        datePickerDialog.setYearRange(2006, 2030);
        datePickerDialog.setCloseOnSingleTapDay(false);
        datePickerDialog.show(getSupportFragmentManager(), "Tag");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_bap, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_calender) {
            setCalenderBap();

            return true;

        } else if (id == R.id.action_refresh) {
            boolean isNetwork = Tools.isOnline(this);

            if (isNetwork) {
                getCalendarInstance(false);

                mCalendar.add(Calendar.DATE, 2 - DAY_OF_WEEK);

                String mPrefLunchName = BapTool.getBapStringFormat(YEAR, MONTH, DAY, BapTool.TYPE_LUNCH);
                String mPrefKcal_LunchName = BapTool.getBapStringFormat(YEAR, MONTH, DAY, BapTool.TYPE_KCAL_LUNCH);
                String mPrefDinnerName = BapTool.getBapStringFormat(YEAR, MONTH, DAY, BapTool.TYPE_DINNER);
                String mPrefKcal_DinnerName = BapTool.getBapStringFormat(YEAR, MONTH, DAY, BapTool.TYPE_KCAL_DINNER);

                Preference mPref = new Preference(getApplicationContext(), BapTool.BAP_PREFERENCE_NAME);
                mPref.remove(mPrefLunchName);
                mPref.remove(mPrefKcal_LunchName);
                mPref.remove(mPrefDinnerName);
                mPref.remove(mPrefKcal_DinnerName);

                getBapList(true);
            } else {

                MaterialDialog.Builder builder = new MaterialDialog.Builder(this);
                builder.title(R.string.no_network_title);
                builder.titleColorRes(R.color.red);
                builder.content(getString(R.string.no_network_msg));
                builder.contentColorRes(R.color.red);
                builder.positiveText(android.R.string.ok);
                builder.positiveColorRes(R.color.red);
                builder.show();

               /* AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatErrorAlertDialogStyle);
                builder.setTitle(R.string.no_network_title);
                builder.setMessage(R.string.no_network_msg);
                builder.setPositiveButton(android.R.string.ok, null);
                builder.show();*/
            }

            return true;

        } else if (id == R.id.action_dialog) {
            new MaterialDialog.Builder(this)
                    .title("알레르기 정보")
                    .content("①난류 ②우유 ③메밀 ④땅콩 ⑤대두 ⑥밀 ⑦고등어 ⑧게 ⑨새우 ⑩돼지고기 ⑪복숭아 ⑫토마토 ⑬아황산류 ⑭호두 ⑮닭고기 ⑯쇠고기 ⑰오징어 ⑱조개류(굴,전복,홍합 포함)")
                    .positiveText("닫기")
                    .show();

            return true;

        } else if (id == R.id.action_today) {
            getCalendarInstance(true);
            getBapList(true);

            return true;
        }

        // else if (id == R.id.action_show_star) {
        //        startActivity(new Intent(this, BapStarActivity.class).putExtra("starType", 2));
        //       }

        // else if (id == R.id.action_show_star) {
    //        startActivity(new Intent(this, BapStarActivity.class).putExtra("starType", 2));
 //       }

        return super.onOptionsItemSelected(item);
    }

    public class BapDownloadTask extends ProcessTask {
        public BapDownloadTask(Context mContext) {
            super(mContext);
        }

        @Override
        public void onPreDownload() {

        }

        @Override
        public void onUpdate(int progress) {
            materialDialog.setProgress(progress);
        }

        @Override
        public void onFinish(long result) {
            if (materialDialog != null)
                materialDialog.dismiss();

            if (result == -1) {

                MaterialDialog.Builder builder = new MaterialDialog.Builder(BapActivity.this);
                builder.title(getString(R.string.I_do_not_know_the_error_title));
                builder.titleColorRes(R.color.red);
                builder.content(getString(R.string.I_do_not_know_the_error_message));
                builder.contentColorRes(R.color.red);
                builder.positiveText(android.R.string.ok);
                builder.positiveColorRes(R.color.red);
                builder.show();

                return;
            }

            getBapList(false);

            if (mSwipeRefreshLayout.isRefreshing())
                mSwipeRefreshLayout.setRefreshing(false);
        }
    }

        private void withShare(String mShareBapMsg) {
            Intent msg = new Intent(Intent.ACTION_SEND);
            msg.addCategory(Intent.CATEGORY_DEFAULT);
            msg.putExtra(Intent.EXTRA_TITLE, getString(R.string.shareBap_title));
            msg.putExtra(Intent.EXTRA_TEXT, mShareBapMsg);
            msg.setType("text/plain");
            startActivity(Intent.createChooser(msg, getString(R.string.shareBap_title)));
        }

}
