package com.gsp.gyenamhighschool.Bap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import com.gsp.gyenamhighschool.tool.BapTool;
import com.gsp.gyenamhighschool.R;

class BapViewHolder {
    public TextView mCalender;
    public TextView mDayOfTheWeek;
    public TextView mLunch;
    public TextView mKcal_Lunch;
    public TextView mDinner;
    public TextView mKcal_Dinner;
    public LinearLayout starLayout;
}

class BapListData {
    public String mCalender;
    public String mDayOfTheWeek;
    public String mLunch;
    public String mKcal_Lunch;
    public String mDinner;
    public String mKcal_Dinner;
    public boolean isToday;
}

public class BapAdapter extends BaseAdapter {
    private Context mContext = null;
    private ArrayList<BapListData> mListData = new ArrayList<BapListData>();

    final View.OnClickListener mStarListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
/*            Intent mIntent = new Intent(mContext, BapStarActivity.class);

            if (v.getId() == R.id.giveStar) {
                mIntent.putExtra("starType", 1);
            } else {
                mIntent.putExtra("starType", 2);
            }

            mContext.startActivity(mIntent); */
        }
    };

    public BapAdapter(Context mContext) {
        super();

        this.mContext = mContext;
    }

    public void addItem(String mCalender, String mDayOfTheWeek, String mLunch, String mKcal_Lunch, String mDinner, String mKcal_Dinner, boolean isToday) {
        BapListData addItemInfo = new BapListData();
        addItemInfo.mCalender = mCalender;
        addItemInfo.mDayOfTheWeek = mDayOfTheWeek;
        addItemInfo.mLunch = mLunch;
        addItemInfo.mKcal_Lunch = mKcal_Lunch;
        addItemInfo.mDinner = mDinner;
        addItemInfo.mKcal_Dinner = mKcal_Dinner;
        addItemInfo.isToday = isToday;

        mListData.add(addItemInfo);
    }

    public void clearData() {
        mListData.clear();
    }

    @Override
    public int getCount() {
        return mListData.size();
    }

    @Override
    public BapListData getItem(int position) {
        return mListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        BapViewHolder mHolder;


        if (convertView == null) {
            mHolder = new BapViewHolder();

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_bap_item, null);

            com.gsp.gyenamhighschool.tool.Preference prefTheme;
            int color;
            prefTheme = new com.gsp.gyenamhighschool.tool.Preference(mContext,"theme");

            color = prefTheme.getInt("theme",0);

            CardView cardView = (CardView) convertView.findViewById(R.id.bap_cardview);

            if (color == Color.parseColor("#F44336")) {
                cardView.setCardBackgroundColor(Color.parseColor("#F44336"));
            } else if (color == Color.parseColor("#E91E63")) {
                cardView.setCardBackgroundColor(Color.parseColor("#E91E63"));
            } else if (color == Color.parseColor("#FFC107")) {
                cardView.setCardBackgroundColor(Color.parseColor("#FFC107"));
            } else if (color == Color.parseColor("#8BC34A")) {
                cardView.setCardBackgroundColor(Color.parseColor("#8BC34A"));
            } else if (color == Color.parseColor("#03A9F4")) {
                cardView.setCardBackgroundColor(Color.parseColor("#03A9F4"));
            } else if (color == Color.parseColor("#3F51B5")) {
                cardView.setCardBackgroundColor(Color.parseColor("#3F51B5"));
            } else {
                cardView.setCardBackgroundColor(Color.parseColor("#03A9F4"));
            }

            mHolder.mCalender = (TextView) convertView.findViewById(R.id.mCalender);
            mHolder.mDayOfTheWeek = (TextView) convertView.findViewById(R.id.mDayOfTheWeek);
            mHolder.mLunch = (TextView) convertView.findViewById(R.id.mLunch);
            mHolder.mKcal_Lunch = (TextView) convertView.findViewById(R.id.kcal_lunch);
            mHolder.mDinner = (TextView) convertView.findViewById(R.id.mDinner);
            mHolder.mKcal_Dinner = (TextView) convertView.findViewById(R.id.kcal_dinner);
            mHolder.starLayout = (LinearLayout) convertView.findViewById(R.id.starLayout);

            convertView.setTag(mHolder);
        } else {
            mHolder = (BapViewHolder) convertView.getTag();
        }

        BapListData mData = mListData.get(position);

        String mCalender = mData.mCalender;
        String mDayOfTheWeek = mData.mDayOfTheWeek;
        String mLunch = mData.mLunch;
        String mKcal_Lunch = mData.mKcal_Lunch;
        String mDinner = mData.mDinner;
        String mKcal_Dinner = mData.mKcal_Dinner;

        if (BapTool.mStringCheck(mLunch)) {
            mLunch = mData.mLunch = mContext.getResources().getString(R.string.no_data_lunch);
        }
        if (BapTool.mStringCheck(mKcal_Lunch)) {
            mKcal_Lunch = mData.mKcal_Lunch = "칼로리 정보가 없어요";
        }
        if (BapTool.mStringCheck(mDinner)) {
            mDinner = mData.mDinner = mContext.getResources().getString(R.string.no_data_dinner);
        }
        if (BapTool.mStringCheck(mKcal_Dinner)) {
            mKcal_Dinner = mData.mKcal_Dinner = "칼로리 정보가 없어요";
        }

        mHolder.mCalender.setText(mCalender);
        mHolder.mDayOfTheWeek.setText(mDayOfTheWeek);
        mHolder.mLunch.setText(mLunch);
        mHolder.mKcal_Lunch.setText(mKcal_Lunch);
        mHolder.mDinner.setText(mDinner);
        mHolder.mKcal_Dinner.setText(mKcal_Dinner);
/*
        if (mData.isToday) {
            mHolder.starLayout.setVisibility(View.VISIBLE);
            convertView.findViewById(R.id.giveStar).setOnClickListener(mStarListener);
            convertView.findViewById(R.id.showStar).setOnClickListener(mStarListener);
        } else { */
            mHolder.starLayout.setVisibility(View.GONE);
  //      }

        return convertView;
    }
}