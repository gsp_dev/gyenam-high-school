package com.gsp.gyenamhighschool.BroadCast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.gsp.gyenamhighschool.BroadCast.BroadCastFragment.BroadCastFragment_Notice;
import com.gsp.gyenamhighschool.BroadCast.BroadCastFragment.BroadCastFragment_request;
import com.gsp.gyenamhighschool.BroadCast.BroadCastFragment.BroadCastFragment_story;
import com.gsp.gyenamhighschool.R;
import com.gsp.gyenamhighschool.tool.Preference;

import java.util.ArrayList;
import java.util.List;

import static com.gsp.gyenamhighschool.tool.Tools.isOnline_Fixed;

public class BroadCastActivity extends AppCompatActivity {

    com.gsp.gyenamhighschool.tool.Preference prefTheme;
    int color;
    ViewPager viewPager;
    TabLayout tabLayout;
    boolean isAdmin_broad;
    CharSequence[] strings;
    private ConnectivityManager cManager;
    private NetworkInfo mobile;
    private NetworkInfo wifi;

    @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            prefTheme = new com.gsp.gyenamhighschool.tool.Preference(getApplicationContext(), "theme");

            color = prefTheme.getInt("theme", 0);

            if (color == Color.parseColor("#F44336"))
                setTheme(R.style.AppTheme_Red);
            if (color == Color.parseColor("#E91E63"))
                setTheme(R.style.AppTheme_Pink);
            if (color == Color.parseColor("#FFC107"))
                setTheme(R.style.AppTheme_Yellow);
            if (color == Color.parseColor("#8BC34A"))
                setTheme(R.style.AppTheme_Green);
            if (color == Color.parseColor("#03A9F4"))
                setTheme(R.style.AppTheme);
            if (color == Color.parseColor("#3F51B5"))
                setTheme(R.style.AppTheme_Indigo);

            setContentView(R.layout.broadcast);

            Toolbar mToolbar = (Toolbar) findViewById(R.id.mToolbar);
            setSupportActionBar(mToolbar);

            ActionBar mActionBar = getSupportActionBar();
            if (mActionBar != null) {
                mActionBar.setHomeButtonEnabled(true);
                mActionBar.setDisplayHomeAsUpEnabled(true);

                mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
            }

        isAdmin_broad = new Preference(getApplicationContext()).getBoolean("userAdmin_broad", false);

        Adapter mAdapter = new Adapter(getSupportFragmentManager());

        mAdapter.addFragment("공지사항", BroadCastFragment_Notice.getInstance(1));
        mAdapter.addFragment("신청곡", BroadCastFragment_request.getInstance(2));
        mAdapter.addFragment("사연 제보", BroadCastFragment_story.getInstance(3));

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        viewPager.setOffscreenPageLimit(4);
        viewPager.setAdapter(mAdapter);

        tabLayout = (TabLayout) findViewById(R.id.mTabLayout);
        tabLayout.setupWithViewPager(viewPager);

        strings = getResources().getStringArray(R.array.broadcast_values);

        FloatingActionButton floatingActionButton_song = (FloatingActionButton) findViewById(R.id.mFab_song); // Notice

        if (isAdmin_broad) { //방송부 권한이 있으면
            floatingActionButton_song.setVisibility(View.VISIBLE); //Floating Action Button을 나타나게 한다.
        }

        floatingActionButton_song.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                    if (isAdmin_broad) {

               // Toast.makeText(getApplicationContext(), "[권한 체크] 방송부 게시글 관리자 등급 입니다." ,Toast.LENGTH_SHORT).show();

                        AlertDialog.Builder builder = new AlertDialog.Builder(BroadCastActivity.this, R.style.AppCompatAlertDialogStyle);
                        builder.setTitle("무엇을 할까요?");
                        builder.setItems(strings, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which){
                                    case 0:
                                        if(isAdmin_broad) { // 방송부 권한 체크

                                            if(isInternetCon()) { //네트워크에 연결 되어있지 않으면
                                                Toast.makeText(BroadCastActivity.this, "Wifi나 데이터를 켜주세요 :)", Toast.LENGTH_SHORT).show();
                                            } else { // 인터넷이 연결 되어있으면

                                                if (isOnline_Fixed()) { // 인터넷이 올바른지 체크한다.
                                                    startActivity(new Intent(getApplicationContext(), BroadCastNoticeSendActivity.class).putExtra("userAdmin_1", true));
                                                    // BroadCastNoticeSendActivity를 실행하고 key : userAdmin_1, Value : true를 보낸다.
                                                } else {
                                                    Toast.makeText(BroadCastActivity.this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                                                }

                                            }

                                        }  else { //방송부 권한이 없으면
                                            Toast.makeText(BroadCastActivity.this, R.string.user_info_require_message_broadcast_read, Toast.LENGTH_SHORT).show();
                                        }
                                        break;

                                    case 1:
                                        if(isAdmin_broad) { // 방송부 권한 체크(위와 같은 원리)

                                            if(isInternetCon()) {
                                                Toast.makeText(BroadCastActivity.this, "Wifi나 데이터를 켜주세요 :)", Toast.LENGTH_SHORT).show();
                                            } else {
                                                if (isOnline_Fixed()) {
                                                    startActivity(new Intent(getApplicationContext(), BroadCastSongActivity.class).putExtra("userAdmin_1", true));
                                                } else {
                                                    Toast.makeText(BroadCastActivity.this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                        } else {
                                            Toast.makeText(BroadCastActivity.this, R.string.user_info_require_message_broadcast_read, Toast.LENGTH_SHORT).show();
                                        }
                                        break;

                                    case 2:
                                        if (isAdmin_broad) {

                                            if(isInternetCon()) {
                                                Toast.makeText(BroadCastActivity.this, "Wifi나 데이터를 켜주세요 :)", Toast.LENGTH_SHORT).show();
                                            } else {
                                                if (isOnline_Fixed()) {
                                                    startActivity(new Intent(getApplicationContext(), StoryActivity.class).putExtra("userAdmin_1", true));
                                                } else {
                                                    Toast.makeText(BroadCastActivity.this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                        } else {
                                            Toast.makeText(BroadCastActivity.this, R.string.user_info_require_message_broadcast_read, Toast.LENGTH_SHORT).show();
                                        }
                                        break;
                                }

                            }
                        });
                        builder.setNegativeButton(android.R.string.cancel, null);
                        builder.show();

                } else {
                    Snackbar.make(view, R.string.user_info_require_message_broadcast_notice, Snackbar.LENGTH_SHORT).show();
                }
            }
        });

    }

    public class Adapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        public void addFragment(String mTitle, Fragment mFragment) {
            mFragments.add(mFragment);
            mFragmentTitles.add(mTitle);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_broadcast, menu);
        return true;
    }

    private boolean isInternetCon() {
        cManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        mobile = cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE); //모바일 데이터 여부
        wifi = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI); //와이파이 여부
        return !mobile.isConnected() && !wifi.isConnected(); //결과값을 리턴
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            new MaterialDialog.Builder(this)
                    .title(R.string.notice_title)
                    .content(getString(R.string.broadcast_description))
                    .negativeText("닫기")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        }
                    })
                    .show();
            return true;
        } else if (id == R.id.action_knbs_facebook) {

            if (isInternetCon()) {
                Toast.makeText(this, "Wifi나 데이터를 켜주세요 :)", Toast.LENGTH_SHORT).show();
            } else {
                if (isOnline_Fixed()) {
                    startActivity(new Intent(this, KNBS_FacebookActivity.class));
                } else {
                    Toast.makeText(BroadCastActivity.this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                }
            }
        }

        return super.onOptionsItemSelected(item);
    }


}