package com.gsp.gyenamhighschool.BroadCast.BroadCastFragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gsp.gyenamhighschool.R;
import com.gsp.gyenamhighschool.notice.NoticeAdapter;
import com.gsp.gyenamhighschool.spreadsheets.GoogleSheetTask;
import com.gsp.gyenamhighschool.tool.Database;
import com.gsp.gyenamhighschool.tool.TimeTableTool;
import com.gsp.gyenamhighschool.tool.Tools;

import java.io.File;
import java.util.ArrayList;

import static com.gsp.gyenamhighschool.tool.Tools.isOnline_Fixed;

public class BroadCastFragment_Notice extends Fragment {

    public static final String NoticeDBName = "BroadCastFragment_Notice.db";
    public static final String NoticeTableName = "BroadCastNoticeInfo";

    boolean isAdmin;

    RecyclerView recyclerView;
    NoticeAdapter mAdapter;
    ProgressDialog mDialog;

    SwipeRefreshLayout mSwipeRefreshLayout;

    private ConnectivityManager cManager;
    private NetworkInfo mobile;
    private NetworkInfo wifi;

    int d = 0;

    ArrayList<NoticeAdapter.NoticeAdapterInfo> mListData = new ArrayList<>();

    com.gsp.gyenamhighschool.tool.Preference prefTheme;
    int color;

    public static Fragment getInstance(int index) {
        BroadCastFragment_Notice frg = new BroadCastFragment_Notice();

        Bundle args = new Bundle();
        args.putInt("code", index);
        frg.setArguments(args);

        return frg;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        prefTheme = new com.gsp.gyenamhighschool.tool.Preference(getActivity(),"theme");

        color = prefTheme.getInt("theme",0);

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.app, container, false);


        recyclerView = (RecyclerView) rootView.findViewById(R.id.app_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new NoticeAdapter(getActivity(), mListData); // * 매우 중요!!! recyclerView가 선언 되기 전에 선언해주어야한다.
        recyclerView.setAdapter(mAdapter);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.mSwipeRefreshLayout);

        if (color == Color.parseColor("#F44336"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.red);
        else if (color == Color.parseColor("#E91E63"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.pink);
        else if (color == Color.parseColor("#FFC107"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.yellow);
        else if (color == Color.parseColor("#8BC34A"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.green);
        else if (color == Color.parseColor("#03A9F4"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        else if (color == Color.parseColor("#3F51B5"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.indigo);
        else
            mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showNoticeData(true);

                if (mSwipeRefreshLayout.isRefreshing())
                    mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        d = d + 1;
        if (d == 1) {
            showNoticeData(false);
        }

        return rootView;
    }

    private void showNoticeData(boolean forceUpdate) {
        mAdapter.clearData();
        mAdapter.notifyDataSetChanged();

        if (Tools.isOnline(getActivity())) {

            if (Tools.isWifi(getActivity()) || forceUpdate) {

                if (isOnline_Fixed()) {
                    getNoticeDownloadTask mTask = new getNoticeDownloadTask();
                    mTask.execute(""); // 불러올 구글 스프레드 시트 URL을 입력해주시면 됩니다
                } else {
                    Toast.makeText(getActivity(),"Wifi나 데이터가 올바르지 않아요..\n네트워크 상태를 체크해주세요 :)",Toast.LENGTH_SHORT).show();
                }
            } else {

                if (isOnline_Fixed()) {
                    getNoticeDownloadTask mTask = new getNoticeDownloadTask();
                    mTask.execute(""); // 불러올 구글 스프레드 시트 URL을 입력해주시면 됩니다
                } else {
                    Toast.makeText(getActivity(),"Wifi나 데이터가 올바르지 않아요..\n네트워크 상태를 체크해주세요 :)",Toast.LENGTH_SHORT).show();
                }

            }
        } else {
            offlineData();
        }
    }

    private void offlineData() {
        if (new File(TimeTableTool.mFilePath + NoticeDBName).exists()) {
            showListViewDate();
        } else {
            MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
            builder.title(R.string.no_network_title);
            builder.titleColorRes(R.color.red);
            builder.content(getString(R.string.no_network_msg));
            builder.contentColorRes(R.color.red);
            builder.positiveText(android.R.string.ok);
            builder.positiveColorRes(R.color.red);
            builder.show();
        }

        if (mSwipeRefreshLayout.isRefreshing())
            mSwipeRefreshLayout.setRefreshing(false);
    }

    class getNoticeDownloadTask extends GoogleSheetTask {
        private Database mDatabase;
        private String[] columnFirstRow;

        @Override
        public void onPreDownload() {
            mDialog = new ProgressDialog(getActivity());
            mDialog.setIndeterminate(true);
            mDialog.setMessage(getString(R.string.loading_title));
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.show();

            new File(TimeTableTool.mFilePath + NoticeDBName).delete();
            mDatabase = new Database();
        }

        @Override
        public void onFinish(long result) {
            if (mDialog != null) {
                mDialog.dismiss();
                mDialog = null;
            }

            if (result == -1) {

                MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
                builder.title(getString(R.string.I_do_not_know_the_error_title));
                builder.titleColorRes(R.color.red);
                builder.content(getString(R.string.I_do_not_know_the_error_message));
                builder.contentColorRes(R.color.red);
                builder.positiveText(android.R.string.ok);
                builder.positiveColorRes(R.color.red);
                builder.show();

                return;
            }

            if (mDatabase != null)
                mDatabase.release();

            showListViewDate();
        }

        @Override
        public void onRow(int startRowNumber, int position, String[] row) {
            if (startRowNumber == position) {
                columnFirstRow = row;

                StringBuilder Column = new StringBuilder();

                // remove deviceId
                for (int i = 0; i < row.length - 1; i++) {
                    Column.append(row[i]);
                    Column.append(" text, ");
                }

                mDatabase.openOrCreateDatabase(TimeTableTool.mFilePath, NoticeDBName, NoticeTableName, Column.substring(0, Column.length() - 2));
            } else {
                int length = row.length;
                for (int i = 0; i < length - 1; i++) {
                    mDatabase.addData(columnFirstRow[i], row[i]);
                }
                mDatabase.commit(NoticeTableName);
            }
        }
    }

    private void showListViewDate() {
        Database mDatabase = new Database();
        mDatabase.openDatabase(TimeTableTool.mFilePath, NoticeDBName);
        Cursor mCursor = mDatabase.getData(NoticeTableName, "*");

        for (int i = 0; i < mCursor.getCount(); i++) {
            mCursor.moveToNext();

            String date = mCursor.getString(1);
            String title = mCursor.getString(2);
            String message = mCursor.getString(3);

            mAdapter.addItem(title, message, date);
        }

        mAdapter.notifyDataSetChanged();

        if (mSwipeRefreshLayout.isRefreshing())
            mSwipeRefreshLayout.setRefreshing(false);
    }
    private boolean isInternetCon() {
        cManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        mobile = cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE); //모바일 데이터 여부
        wifi = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI); //와이파이 여부
        return !mobile.isConnected() && !wifi.isConnected(); //결과값을 리턴
    }


}
