package com.gsp.gyenamhighschool.BroadCast.BroadCastFragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.gsp.gyenamhighschool.BroadCast.SongAdapter;
import com.gsp.gyenamhighschool.R;
import com.gsp.gyenamhighschool.tool.Preference;
import com.gsp.gyenamhighschool.tool.SecurityXor;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Vector;

import static com.gsp.gyenamhighschool.tool.Tools.isOnline_Fixed;


public class BroadCastFragment_request extends Fragment {

    boolean isAdmin_stu, isAdmin_broad, isAdmin_student, isAdmin_developer;

    ProgressDialog mDialog;
    EditText mTitle, mMessage;
    String student, broadcast, school_student, developer;

    private String url = ""; // 파싱할 사이트를 입력해주시면 됩니다 :)
    private URL URL;

    AlertDialog.Builder builder1;

    private ConnectivityManager cManager;
    private NetworkInfo mobile;
    private NetworkInfo wifi;

    private Source source;
    private ProgressDialog progressDialog;
    CheckBox checkbox;

    com.gsp.gyenamhighschool.tool.Preference prefTheme;
    int color;

    public static Fragment getInstance(int index) {
        BroadCastFragment_request frg2 = new BroadCastFragment_request();

        Bundle args = new Bundle();
        args.putInt("code", index);
        frg2.setArguments(args);

        return frg2;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        prefTheme = new com.gsp.gyenamhighschool.tool.Preference(getActivity(), "theme");

        color = prefTheme.getInt("theme", 0);

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.broadcast_song_send, container, false);


        isAdmin_stu = new Preference(getActivity()).getBoolean("userAdmin_stu", false);
        isAdmin_broad = new Preference(getActivity()).getBoolean("userAdmin_broad", false);
        isAdmin_student = new Preference(getActivity()).getBoolean("userAdmin_student", false);
        isAdmin_developer = new Preference(getActivity()).getBoolean("userAdmin_developer", false);

        if (isAdmin_stu || isAdmin_broad || isAdmin_student || isAdmin_developer) { // 권한이 계남인 이상인 경우

            LinearLayout linearLayout = (LinearLayout) rootView.findViewById(R.id.broadcast_send_need_permission_layout);
            linearLayout.setVisibility(View.GONE); // 권한이 필요하다는 Layout 사라짐

            LinearLayout linearLayout2 = (LinearLayout) rootView.findViewById(R.id.broadcast_send_layout);
            linearLayout2.setVisibility(View.VISIBLE); // 전송할 수 있는 Layout 나타남

                mTitle = (EditText) rootView.findViewById(R.id.mTitle);
                mMessage = (EditText) rootView.findViewById(R.id.mMessage);
                checkbox = (CheckBox) rootView.findViewById(R.id.checkbox);

                Button btn = (Button) rootView.findViewById(R.id.send);

                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (mTitle.length() == 0 || mMessage.length() == 0) {
                            Toast.makeText(getActivity(), "내용을 입력해주세요 :)", Toast.LENGTH_SHORT).show();
                        } else {

                            if (isInternetCon()) { // 네트워크 연결 안됨
                                Toast.makeText(getActivity(), "Wifi나 데이터를 켜주세요 :)", Toast.LENGTH_SHORT).show();

                            } else { // 네트워크 연결됨

                                if (isOnline_Fixed()) { // 네트워크가 올바른지 체크
                                    try {
                                        process(); //네트워크 관련은 따로 쓰레드를 생성해야 UI 쓰레드와 겹치지 않는다. 그러므로 Thread 가 선언된 process 메서드를 호출한다.
                                    } catch (Exception e) {
                                        Log.d("ERROR", e + "");
                                    }

                                    builder1 = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
                                    builder1.setTitle(R.string.user_info_class_up_title);

                                    final EditText input = new EditText(getActivity());
                                    builder1.setView(input);

                                    builder1.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            dialog.dismiss();
                                            String value = input.getText().toString();
                                            SecurityXor securityXor = new SecurityXor();

                                            if (student.equals(securityXor.getSecurityXor(value, 1654))
                                                    || broadcast.equals(securityXor.getSecurityXor(value, 2378))
                                                    || school_student.equals(securityXor.getSecurityXor(value, 4431))
                                                    || developer.equals(securityXor.getSecurityXor(value, 7345))) { //암호화 된 코드와 비교해서 일치하면(계남인 이상의 권한)

                                                MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
                                                builder.title("신청곡 전송");
                                                builder.content(getString(R.string.post_song_alert));
                                                builder.negativeText(android.R.string.cancel);
                                                builder.positiveText(android.R.string.ok);
                                                builder.onPositive(new MaterialDialog.SingleButtonCallback() {
                                                    @Override
                                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                        // TODO
                                                        String title = mTitle.getText().toString().trim();
                                                        String message = mMessage.getText().toString().trim();
                                                        String mv_want = "뮤비 O".trim();
                                                        String mv_no_want = "뮤비 X".trim();
                                                        if (!title.isEmpty() && !message.isEmpty()) { // 제목과 내용 모두 입력됐을경우
                                                            if (checkbox.isChecked()) { // 뮤비 시청여부가 체크 되었다면
                                                                (new HttpTask()).execute(title, message, mv_want); // 제목, 내용, 뮤비 O를 HttpTask에 넘겨줌
                                                            } else {
                                                                (new HttpTask()).execute(title, message, mv_no_want); // 제목, 내용, 뮤비 X를 HttpTask에 넘겨줌
                                                            }
                                                        }
                                                    }
                                                });

                                                builder.show();
                                            }

                                        }
                                    });
                                    builder1.setNegativeButton(android.R.string.cancel, null);
                                } else {// 네트워크가 올바르지 않으면
                                    Toast.makeText(getActivity(), "Wifi나 데이터가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                });

        }
        return rootView;

    }

    private void process() throws IOException {

        new Thread() {

            @Override
            public void run() {

                Handler Progress = new Handler(Looper.getMainLooper()); //네트워크 쓰레드와 별개로 따로 핸들러를 이용하여 쓰레드를 생성한다.
                Progress.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog = ProgressDialog.show(getActivity(), "", "암호를 불러오는 중 입니다 :)");
                    }
                }, 0);

                try {
                    URL = new URL(url);
                    InputStream html = URL.openStream();
                    source = new Source(new InputStreamReader(html, "UTF-8")); //소스를 UTF-8 인코딩으로 불러온다.
                    source.fullSequentialParse(); //순차적으로 구문분석
                } catch (Exception e) {
                    Log.d("ERROR", e + "");
                }

                Element BBS_TABLE = (Element) source.getAllElements(HTMLElementName.TABLE).get(0); // 첫번째 TABLE을 불러옴
                Element BBS_TBODY = (Element) BBS_TABLE.getAllElements(HTMLElementName.TBODY).get(0); // 첫번째 TABLE의 첫번째 TBODY를 불러옴


                // 소스의 효율성을 위해서는 for 문을 사용하는것이 좋지만 , 이해를 돕기위해 소스를 일부로 늘려 두었다.

                try {
                    Element BBS_TR = (Element) BBS_TBODY.getAllElements(HTMLElementName.TR).get(1); //TBODY의 2번째 TR을 불러옴

                    Element TD_STUDENT = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(0); //2번째 TR의 1번째 TD를 불러옴
                    Element TD_BROADCAST = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(1); //2번째 TR의 2번째 TD를 불러옴
                    Element TD_SCHOOL_STUDENT = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(2); //2번째 TR의 3번째 TD를 불러옴
                    Element TD_DEVELOPER = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(3);//2번째 TR의 4번째 TD를 불러옴

                    student = TD_STUDENT.getAllElements(HTMLElementName.DIV).get(0).getContent().toString(); //TD_STUDENT(1번째 TD)의 1번째 DIV를 불러옴
                    broadcast = TD_BROADCAST.getAllElements(HTMLElementName.DIV).get(0).getContent().toString(); //TD_BROADCAST(2번째 TD)의 1번째 DIV를 불러옴
                    school_student = TD_SCHOOL_STUDENT.getAllElements(HTMLElementName.DIV).get(0).getContent().toString();//TD_SCHOOL_STUDENT(3번째 TD)의 1번째 DIV를 불러옴
                    developer = TD_DEVELOPER.getAllElements(HTMLElementName.DIV).get(0).getContent().toString();//TD_DEVELOPER(4번째 TD)의 1번째 DIV를 불러옴

                    /* HTML의 형태로 바꾸고 문자열로 변환 값을 저장함 */
                    student = Html.fromHtml(student).toString();
                    broadcast = Html.fromHtml(broadcast).toString();
                    school_student = Html.fromHtml(school_student).toString();
                    developer = Html.fromHtml(developer).toString();


                }catch(Exception e){
                    Log.d("BCSERROR",e+"");
                }
                Handler mHandler = new Handler(Looper.getMainLooper());
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss(); //모든 작업이 끝나면 다이어로그 종료
                        builder1.show();
                    }
                }, 0);

            }

        }.start();


    }

    private boolean isInternetCon() {
        cManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        mobile = cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE); //모바일 데이터 여부
        wifi = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI); //와이파이 여부
        return !mobile.isConnected() && !wifi.isConnected(); //결과값을 리턴
    }


    private class HttpTask extends AsyncTask<String, Void, Boolean> {

        /** http://itmir.tistory.com/613를 참조해주세요. */

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mDialog = new ProgressDialog(getActivity());
            mDialog.setIndeterminate(true);
            mDialog.setMessage(getString(R.string.post_song_posting));
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpPost postRequest = new HttpPost(""); //원하시는 스크립트 URL을 입력해주시면 됩니다.

                //전달할 값들
                Vector<NameValuePair> nameValue = new Vector<>();
                nameValue.add(new BasicNameValuePair("", "")); //sheet_name, sheet_name
                nameValue.add(new BasicNameValuePair("", params[0])); //첫 번째 내용
                nameValue.add(new BasicNameValuePair("", params[1])); //두 번째 내용
                nameValue.add(new BasicNameValuePair("", params[2])); //세 번째 내용
                nameValue.add(new BasicNameValuePair("deviceId", Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID)));

//                nameValue.add(new BasicNameValuePair("code", HiddenCode.getHiddenCode()));

                //웹 접속 - UTF-8으로
                HttpEntity Entity = new UrlEncodedFormEntity(nameValue, "UTF-8");
                postRequest.setEntity(Entity);

                HttpClient mClient = new DefaultHttpClient();
                mClient.execute(postRequest);

                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }

        protected void onPostExecute(Boolean value) {
            super.onPostExecute(value);

            if (mDialog != null) {
                mDialog.dismiss();
                mDialog = null;
            }

            if (value) {
                MaterialDialog.Builder dialog = new MaterialDialog.Builder(getActivity());
                        dialog.title(R.string.post_song_title);
                        dialog.content(R.string.post_song_success);
                        dialog.positiveText(android.R.string.ok);
                        dialog.show();

                mTitle.setText("");
                mMessage.setText("");
            } else {

                MaterialDialog.Builder dialog = new MaterialDialog.Builder(getActivity());
                dialog.title(R.string.post_song_title);
                dialog.titleColorRes(R.color.red);
                dialog.content(R.string.post_song_failed);
                dialog.titleColorRes(R.color.red);
                dialog.positiveText(android.R.string.ok);
                dialog.positiveColorRes(R.color.red);
                dialog.show();

            }
        }
    }
}