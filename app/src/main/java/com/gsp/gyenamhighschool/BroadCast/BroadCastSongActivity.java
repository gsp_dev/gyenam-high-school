package com.gsp.gyenamhighschool.BroadCast;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gsp.gyenamhighschool.MainActivity;
import com.gsp.gyenamhighschool.R;
import com.gsp.gyenamhighschool.spreadsheets.GoogleSheetTask;
import com.gsp.gyenamhighschool.tool.Database;
import com.gsp.gyenamhighschool.tool.Preference;
import com.gsp.gyenamhighschool.tool.TimeTableTool;
import com.gsp.gyenamhighschool.tool.Tools;

import java.io.File;
import java.util.ArrayList;

import static com.gsp.gyenamhighschool.tool.Tools.isOnline_Fixed;


public class BroadCastSongActivity extends AppCompatActivity{
    public static final String NoticeDBName = "BroadCastSongRequest.db";
    public static final String NoticeTableName = "BroadCastSongRequestInfo";

    boolean isAdmin_broad;

    RecyclerView recyclerView;
    SongAdapter mAdapter;
    ProgressDialog mDialog;

    SwipeRefreshLayout mSwipeRefreshLayout;

    private ConnectivityManager cManager;
    private NetworkInfo mobile;
    private NetworkInfo wifi;

    int e = 0;

    ArrayList<SongAdapter.SongAdapterInfo> mListData = new ArrayList<>();

    com.gsp.gyenamhighschool.tool.Preference prefTheme;
    int color;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefTheme = new com.gsp.gyenamhighschool.tool.Preference(getApplicationContext(), "theme");

        color = prefTheme.getInt("theme", 0);

        if (color == Color.parseColor("#F44336"))
            setTheme(R.style.AppTheme_Red);
        if (color == Color.parseColor("#E91E63"))
            setTheme(R.style.AppTheme_Pink);
        if (color == Color.parseColor("#FFC107"))
            setTheme(R.style.AppTheme_Yellow);
        if (color == Color.parseColor("#8BC34A"))
            setTheme(R.style.AppTheme_Green);
        if (color == Color.parseColor("#03A9F4"))
            setTheme(R.style.AppTheme);
        if (color == Color.parseColor("#3F51B5"))
            setTheme(R.style.AppTheme_Indigo);

        setContentView(R.layout.activity_reader);

        recyclerView = (RecyclerView) findViewById(R.id.app_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new SongAdapter(this, mListData); // * 매우 중요!!! recyclerView가 선언 되기 전에 선언해주어야한다.
        recyclerView.setAdapter(mAdapter);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.mToolbar);
        setSupportActionBar(mToolbar);

        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setHomeButtonEnabled(true);
            mActionBar.setDisplayHomeAsUpEnabled(true);

            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.mFab_reader);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNoticeData(true);
            }
        });

        boolean isAdmin = getIntent().getBooleanExtra("userAdmin_1", false); // BroadCastActivity에서 보냈던 값을 얻어온다.
        isAdmin_broad = new Preference(getApplicationContext()).getBoolean("userAdmin_broad", false); // 방송부 권한을 얻어온다.

        if (!isAdmin) { // isAdmin이 False라면

            finish(); // 액티비티 종료

        } else { // isAdmin이 True라면

            if(!isAdmin_broad) { // isAdmin_broad가 False라면

                finish(); // 액티비티 종료
            }
        }

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.mSwipeRefreshLayout);

        if (color == Color.parseColor("#F44336"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.red);
        else if (color == Color.parseColor("#E91E63"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.pink);
        else if (color == Color.parseColor("#FFC107"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.yellow);
        else if (color == Color.parseColor("#8BC34A"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.green);
        else if (color == Color.parseColor("#03A9F4"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        else if (color == Color.parseColor("#3F51B5"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.indigo);
        else
            mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showNoticeData(true);

                if (mSwipeRefreshLayout.isRefreshing())
                    mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        showNoticeData(false);
    }

    private void showNoticeData(boolean forceUpdate) {
        mAdapter.clearData();
        mAdapter.notifyDataSetChanged();

        if (Tools.isOnline(this)) {
            if (Tools.isWifi(this) || forceUpdate) {
                if (isOnline_Fixed()) {
                    getNoticeDownloadTask mTask = new getNoticeDownloadTask();
                    mTask.execute(""); //파싱할 구글 스프레드 시트 URL을 입력해주세요 :)
                } else {
                    Toast.makeText(this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (isOnline_Fixed()) {
                    getNoticeDownloadTask mTask = new getNoticeDownloadTask();
                    mTask.execute(""); // 파싱할 구글 스프레드 시트 URL을 입력해주세요 :)
                } else {
                    Toast.makeText(this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            offlineData();
        }
    }

    private void offlineData() {
        if (new File(TimeTableTool.mFilePath + NoticeDBName).exists()) {
            showListViewDate();
        } else {

            MaterialDialog.Builder builder = new MaterialDialog.Builder(this);
            builder.title(R.string.no_network_title);
            builder.titleColorRes(R.color.red);
            builder.content(getString(R.string.no_network_msg));
            builder.contentColorRes(R.color.red);
            builder.positiveText(android.R.string.ok);
            builder.positiveColorRes(R.color.red);
            builder.show();

        }

        if (mSwipeRefreshLayout.isRefreshing())
            mSwipeRefreshLayout.setRefreshing(false);
    }

    class getNoticeDownloadTask extends GoogleSheetTask {
        private Database mDatabase;
        private String[] columnFirstRow;

        @Override
        public void onPreDownload() {
            mDialog = new ProgressDialog(BroadCastSongActivity.this);
            mDialog.setIndeterminate(true);
            mDialog.setMessage(getString(R.string.loading_title));
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.show();

            new File(TimeTableTool.mFilePath + NoticeDBName).delete();
            mDatabase = new Database();
        }


        @Override
        public void onFinish(long result) {
            if (mDialog != null) {
                mDialog.dismiss();
                mDialog = null;
            }

            if (mDatabase != null)
                mDatabase.release();

            showListViewDate();
        }

        @Override
        public void onRow(int startRowNumber, int position, String[] row) {
            if (startRowNumber == position) {
                columnFirstRow = row;

                StringBuilder Column = new StringBuilder();

                // remove deviceId
                for (int i = 0; i < row.length; i++) {
                    Column.append(row[i]);
                    Column.append(" text, ");
                }

                mDatabase.openOrCreateDatabase(TimeTableTool.mFilePath, NoticeDBName, NoticeTableName, Column.substring(0, Column.length() - 2));
            } else {
                int length = row.length;
                for (int i = 0; i < length/* - 1*/; i++) {
                    mDatabase.addData(columnFirstRow[i], row[i]);
                }
                mDatabase.commit(NoticeTableName);
            }
        }
    }

    private void showListViewDate() {
        Database mDatabase = new Database();
        mDatabase.openDatabase(TimeTableTool.mFilePath, NoticeDBName);
        Cursor mCursor = mDatabase.getData(NoticeTableName, "*");

        for (int i = 0; i < mCursor.getCount(); i++) {
            mCursor.moveToNext();

            String date = mCursor.getString(1);
            String title = mCursor.getString(2);
            String message = mCursor.getString(3);
            String music_video = mCursor.getString(4);

            mAdapter.addItem(title, message, date, music_video);
        }

        mAdapter.notifyDataSetChanged();

        if (mSwipeRefreshLayout.isRefreshing())
            mSwipeRefreshLayout.setRefreshing(false);
    }
    private boolean isInternetCon() {
        cManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        mobile = cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE); //모바일 데이터 여부
        wifi = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI); //와이파이 여부
        return !mobile.isConnected() && !wifi.isConnected(); //결과값을 리턴
    }


}
