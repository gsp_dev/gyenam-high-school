package com.gsp.gyenamhighschool.BroadCast;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.gsp.gyenamhighschool.R;

public class KNBS_FacebookActivity extends AppCompatActivity {

    private final static String MAIN_URL = "https://www.facebook.com/%EA%B3%84%EB%82%A8%EA%B3%A0-%EB%B0%A9%EC%86%A1%EB%B6%80-KNBS-333366636844419/";
    private long backKeyPressedTime = 0;

    private WebView web;
    private ProgressBar progress;

    Toast toast;
    com.gsp.gyenamhighschool.tool.Preference prefTheme;
    int color;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefTheme = new com.gsp.gyenamhighschool.tool.Preference(getApplicationContext(), "theme");

        color = prefTheme.getInt("theme", 0);

        if (color == Color.parseColor("#F44336"))
            setTheme(R.style.AppTheme_Red);
        if (color == Color.parseColor("#E91E63"))
            setTheme(R.style.AppTheme_Pink);
        if (color == Color.parseColor("#FFC107"))
            setTheme(R.style.AppTheme_Yellow);
        if (color == Color.parseColor("#8BC34A"))
            setTheme(R.style.AppTheme_Green);
        if (color == Color.parseColor("#03A9F4"))
            setTheme(R.style.AppTheme);
        if (color == Color.parseColor("#3F51B5"))
            setTheme(R.style.AppTheme_Indigo);

        setContentView(R.layout.activity_afterschoolprogram);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.mToolbar);
        setSupportActionBar(mToolbar);

        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setHomeButtonEnabled(true);
            mActionBar.setDisplayHomeAsUpEnabled(true);

            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        progress = (ProgressBar) findViewById(R.id.progressbar);
        web = (WebView) findViewById(R.id.webview);
        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setBuiltInZoomControls(true);
        web.setHorizontalScrollbarOverlay(true);
        web.setVerticalScrollbarOverlay(true);
        web.loadUrl(MAIN_URL);
        web.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onReceivedError(WebView view, int errorcode, String description, String fallingUrl) {
                Toast.makeText(KNBS_FacebookActivity.this, "오류 : " + description, Toast.LENGTH_SHORT).show();
            }

            // 페이지 로딩 시작시 호출
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                progress.setVisibility(View.VISIBLE);
            }

            //페이지 로딩 종료시 호출

            public void onPageFinished(WebView view, String Url) {
                progress.setVisibility(View.GONE);
            }
        });
    }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        // 디바이스의 키 이벤트가 발생했는데, 뒤로가기 이벤트일때
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            // 현재 페이지가 메인페이지 일경우 toast를 출력하고 종료여부를 결정한다.

            if (web.canGoBack() ) {
                web.goBack();
                return true;
            } else {
                if (System.currentTimeMillis() > backKeyPressedTime + 2000) {
                    backKeyPressedTime = System.currentTimeMillis();
                    toast = Toast.makeText(this, "\'뒤로\'버튼을 한번 더 누르시면 종료됩니다.", Toast.LENGTH_SHORT);
                    toast.show();
                    return true;
                }
                if (System.currentTimeMillis() <= backKeyPressedTime + 2000) {
                    finish();
                    toast.cancel();
                }
            }

            return true;
        }

        return super.onKeyDown(keyCode, event);






    }
}