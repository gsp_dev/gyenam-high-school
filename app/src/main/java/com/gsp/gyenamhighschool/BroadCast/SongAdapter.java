package com.gsp.gyenamhighschool.BroadCast;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gsp.gyenamhighschool.R;
import com.gsp.gyenamhighschool.notice.NoticeAdapter;

import java.util.ArrayList;

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.SongAdapterViewHolder> {
    //    private int mBackground;
    private Context mContext = null;
    private ArrayList<SongAdapterInfo> mValues = new ArrayList<>();

    public SongAdapter(Context mContext, ArrayList<SongAdapterInfo> itemsData) {
        super();
        this.mContext = mContext;
        this.mValues = itemsData;
    }


    public void addItem(String title, String message, String date, String music_video) {
        SongAdapterInfo addInfo = new SongAdapterInfo();

        addInfo.title = title;
        addInfo.message = message;
        addInfo.date = date;
        addInfo.music_video = music_video;

        mValues.add(0, addInfo);
    }

    public void clearData() {
        mValues.clear();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public SongAdapter.SongAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_song_item, parent, false);


//        mView.setBackgroundResource(mBackground);

        return new SongAdapterViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(final SongAdapterViewHolder holder, int position) {
        SongAdapterInfo mInfo = getItemData(position);

        String title = mInfo.title;
        String message = mInfo.message;
        String date = mInfo.date;
        String music_video = mInfo.music_video;

        holder.mTitle.setText(title);
        holder.mMessage.setText(message);
        holder.mDate.setText(date);
        holder.music_video.setText(music_video);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public SongAdapterInfo getItemData(int position) {
        return mValues.get(position);
    }

    public class SongAdapterViewHolder extends RecyclerView.ViewHolder {
        //        public final View mView;
        public final TextView mTitle, mMessage, mDate, music_video;

        public SongAdapterViewHolder(View mView) {
            super(mView);
//            this.mView = mView;

            mTitle = (TextView) mView.findViewById(R.id.mTitle);
            mMessage = (TextView) mView.findViewById(R.id.mMessage);
            mDate = (TextView) mView.findViewById(R.id.mDate);
            music_video = (TextView) mView.findViewById(R.id.music_video);
        }
    }

    public class SongAdapterInfo {
        public String title;
        public String message;
        public String date;
        public String music_video;
    }
}

/**사용하지 않는 코드이므로 무시하세요*/

/*
class NoticeViewHolder {
    public TextView mTitle;
    public TextView mMessage;
    public TextView mDate;
}



class NoticeShowData {
    public String title;
    public String message;
    public String date;
}

public class NoticeAdapter extends RecyclerView.Adapter<NoticeAdapter.ViewHolder> {

    com.gsp.gyenamhighschool.tool.Preference prefTheme;
    int color;
    private ArrayList<NoticeShowData> mListData;
    private Context mContext = null;


public NoticeAdapter(Context context, ArrayList<NoticeShowData> itemsData) {
        this.mListData = itemsData;
        this.mContext = context;


        }


    public void addItem(String title, String message, String date) {
        NoticeShowData addItemInfo = new NoticeShowData();
        addItemInfo.title = title;
        addItemInfo.message = message;
        addItemInfo.date = date;

        mListData.add(0, addItemInfo);
    }

    public void clearData() {
        mListData.clear();
    }

    @Override
    public int getItemCount() {
        return this.mListData.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
/*
    @Override
    public NoticeShowData getItem(int position) {
        return this.mListData.get(position);
    }
*/

// Create new views (invoked by the layout manager)
/*    @Override
    public NoticeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        prefTheme = new com.gsp.gyenamhighschool.tool.Preference(mContext, "theme");

        color = prefTheme.getInt("theme", 0);

        View sampleItemLayout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_notice_item, parent, false);

        if (color == Color.parseColor("#F44336")) {
            LinearLayout linearLayout = (LinearLayout) sampleItemLayout.findViewById(R.id.linear); // 그대로
            linearLayout.setBackgroundResource(R.drawable.selector_red);
        } else if (color == Color.parseColor("#E91E63")){
            LinearLayout linearLayout = (LinearLayout) sampleItemLayout.findViewById(R.id.linear); // 그대로
            linearLayout.setBackgroundResource(R.drawable.selector_pink);
        } else if (color == Color.parseColor("#FFC107")){
            LinearLayout linearLayout = (LinearLayout) sampleItemLayout.findViewById(R.id.linear); // 그대로
            linearLayout.setBackgroundResource(R.drawable.selector_yellow);
        } else if (color == Color.parseColor("#8BC34A")){
            LinearLayout linearLayout = (LinearLayout) sampleItemLayout.findViewById(R.id.linear); // 그대로
            linearLayout.setBackgroundResource(R.drawable.selector_green);
        } else if (color == Color.parseColor("#03A9F4")){
            LinearLayout linearLayout = (LinearLayout) sampleItemLayout.findViewById(R.id.linear); // 그대로
            linearLayout.setBackgroundResource(R.drawable.selector);
        } else if (color == Color.parseColor("#3F51B5")){
            LinearLayout linearLayout = (LinearLayout) sampleItemLayout.findViewById(R.id.linear); // 그대로
            linearLayout.setBackgroundResource(R.drawable.selector_indigo);
        } else {
            LinearLayout linearLayout = (LinearLayout) sampleItemLayout.findViewById(R.id.linear); // 그대로
            linearLayout.setBackgroundResource(R.drawable.selector);
        }

        return new NoticeAdapter.ViewHolder(sampleItemLayout);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        NoticeShowData mData = mListData.get(position);

        //     viewHolder.mTitle.setText(listData.get(position).mTitle);
        viewHolder.mTitle.setText(mData.title);
        viewHolder.mMessage.setText(mData.message);
        viewHolder.mDate.setText(mData.date);
     /*   viewHolder.apply.setText(R.string.applys); */

//    }

// inner class to hold a reference to each item of RecyclerView
/*public class ViewHolder extends RecyclerView.ViewHolder{

    public TextView mTitle;
    public TextView mMessage;
    public TextView mDate;


    public ViewHolder(View view) {
        super(view);
        this.mTitle = (TextView)view.findViewById(R.id.item_title);
        this.mMessage = (TextView)view.findViewById(R.id.item_writer);
        this.mDate = (TextView)view.findViewById(R.id.item_date);
    }
    }

}
*/
