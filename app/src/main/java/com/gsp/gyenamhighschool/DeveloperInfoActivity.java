package com.gsp.gyenamhighschool;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

public class DeveloperInfoActivity extends AppCompatActivity {

    private View view1;
    private ViewGroup rowContainer;
    Toolbar mToolbar;

    public void fillText(View view, String title, String sub_text) {

        TextView txt = (TextView) view.findViewById(R.id.developer_title);
        txt.setText(title);
        TextView txt2 = (TextView) view.findViewById(R.id.developer_content);
        txt2.setText(sub_text);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.developer_info);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);


        final String[] strings = {"",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                ""}; // 12
        final String[] strings_yong = {"",
                "",
                "",
                "",
                ""};
        final String[] strings_yang = {"",
                "",
                ""};

        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setHomeButtonEnabled(true);
            mActionBar.setDisplayHomeAsUpEnabled(true);

            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        rowContainer = (LinearLayout) findViewById(R.id.developer_rowContainer);

        /** fillText(view, Title, Content) */

        view1 = rowContainer.findViewById(R.id.developer_item_one);
        fillText(view1, "", "");
        view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int r = (int)(Math.random()*12);
                Toast.makeText(DeveloperInfoActivity.this, strings[r], Toast.LENGTH_SHORT).show();
            }
        });

        view1 = rowContainer.findViewById(R.id.developer_item_two);
        fillText(view1, "", "");
        view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int r_yang = (int)(Math.random()*3);
                Toast.makeText(DeveloperInfoActivity.this, strings_yang[r_yang],Toast.LENGTH_SHORT).show();
            }
        });

        view1 = rowContainer.findViewById(R.id.developer_item_three);
        fillText(view1, "", "");
        view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int r_yong = (int)(Math.random()*5);
                Toast.makeText(DeveloperInfoActivity.this, strings_yong[r_yong] , Toast.LENGTH_SHORT).show();
            }
        });

        view1 = rowContainer.findViewById(R.id.developer_item_four);
        fillText(view1, "", "");
        view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DeveloperInfoActivity.this, ""  , Toast.LENGTH_SHORT).show();
            }
        });

        view1 = rowContainer.findViewById(R.id.developer_item_five);
        fillText(view1, "", "");
        view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DeveloperInfoActivity.this, "" , Toast.LENGTH_SHORT).show();
            }
        });

        view1 = rowContainer.findViewById(R.id.developer_item_six);
        fillText(view1, "", "");
        view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DeveloperInfoActivity.this,"",Toast.LENGTH_SHORT).show();
            }
        });

        view1 = rowContainer.findViewById(R.id.developer_item_seven);
        fillText(view1, "", "");
        view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DeveloperInfoActivity.this, "" , Toast.LENGTH_SHORT).show();
            }
        });

}


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_dialog_broadcast, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            new MaterialDialog.Builder(this)
                    .title("개발자 정보")
                    .content("" +
                            "" +
                            "" +
                            "" +
                            "" +
                            "" +
                            "" +
                            "" +
                            "" +
                            "")
                    .negativeText("닫기")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        }
                    })
                    .show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
