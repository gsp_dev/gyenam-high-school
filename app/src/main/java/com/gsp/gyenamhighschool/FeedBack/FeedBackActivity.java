package com.gsp.gyenamhighschool.FeedBack;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.gsp.gyenamhighschool.R;


public class FeedBackActivity extends AppCompatActivity {

    com.gsp.gyenamhighschool.tool.Preference prefTheme;
    int color;
    EditText mMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefTheme = new com.gsp.gyenamhighschool.tool.Preference(getApplicationContext(), "theme");

        color = prefTheme.getInt("theme", 0);


        if (color == Color.parseColor("#F44336"))
            setTheme(R.style.AppTheme_Red);
        if (color == Color.parseColor("#E91E63"))
            setTheme(R.style.AppTheme_Pink);
        if (color == Color.parseColor("#FFC107"))
            setTheme(R.style.AppTheme_Yellow);
        if (color == Color.parseColor("#8BC34A"))
            setTheme(R.style.AppTheme_Green);
        if (color == Color.parseColor("#03A9F4"))
            setTheme(R.style.AppTheme);
        if (color == Color.parseColor("#3F51B5"))
            setTheme(R.style.AppTheme_Indigo);

        setContentView(R.layout.feedback_send);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.mToolbar);
        setSupportActionBar(mToolbar);

        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setHomeButtonEnabled(true);
            mActionBar.setDisplayHomeAsUpEnabled(true);

            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        mMessage = (EditText) findViewById(R.id.mMessage);

        Button btn = (Button) findViewById(R.id.send);
//        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.feedback_linearlayout);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mMessage.length() > 0) {
                    try{
                        final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                        intent.setType("plain/text");
                        intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"gspdev77@gmail.com"});
                        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "이것을 문의합니다.");
                        String txt = mMessage.getText().toString();
                        intent.putExtra(android.content.Intent.EXTRA_TEXT, txt);
                        startActivity(Intent.createChooser(intent, "문의하기"));

                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(FeedBackActivity.this, "이메일을 보내게 하는 앱이 설치 되어 있지 않아요", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(FeedBackActivity.this, "내용을 작성해주세요 :)", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
