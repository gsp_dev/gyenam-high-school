package com.gsp.gyenamhighschool.FeedBack;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.gsp.gyenamhighschool.R;
import com.gsp.gyenamhighschool.tool.Preference;
import com.gsp.gyenamhighschool.tool.SecurityXor;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Vector;

public class SchoolFeedBackActivity extends AppCompatActivity {

    com.gsp.gyenamhighschool.tool.Preference prefTheme;
    int color;
    EditText mTitle, mMessage;
    ProgressDialog mDialog;
    String student, broadcast, school_student, developer;
    boolean isAdmin_stu, isAdmin_broad, isAdmin_student, isAdmin_developer;

    private String url = ""; //원하시는 URL을 입력
    private java.net.URL URL;

    AlertDialog.Builder builder;

    private Source source;
    private ProgressDialog progressDialog;

    private ConnectivityManager cManager;
    private NetworkInfo mobile;
    private NetworkInfo wifi;


    /**원리는 BroadCastFragment_request.java와 같으므로 그것의 주석을 참고해주세요*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefTheme = new com.gsp.gyenamhighschool.tool.Preference(getApplicationContext(), "theme");

        color = prefTheme.getInt("theme", 0);

//        String str = String.valueOf(color);

        if (color == Color.parseColor("#F44336"))
            setTheme(R.style.AppTheme_Red);
        if (color == Color.parseColor("#E91E63"))
            setTheme(R.style.AppTheme_Pink);
        if (color == Color.parseColor("#FFC107"))
            setTheme(R.style.AppTheme_Yellow);
        if (color == Color.parseColor("#8BC34A"))
            setTheme(R.style.AppTheme_Green);
        if (color == Color.parseColor("#03A9F4"))
            setTheme(R.style.AppTheme);
        if (color == Color.parseColor("#3F51B5"))
            setTheme(R.style.AppTheme_Indigo);

        setContentView(R.layout.schoolfeedback_send);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.mToolbar);
        setSupportActionBar(mToolbar);

        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setHomeButtonEnabled(true);
            mActionBar.setDisplayHomeAsUpEnabled(true);

            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        isAdmin_stu = new Preference(this).getBoolean("userAdmin_stu", false);
        isAdmin_broad = new Preference(this).getBoolean("userAdmin_broad", false);
        isAdmin_student = new Preference(this).getBoolean("userAdmin_student", false);
        isAdmin_developer = new Preference(this).getBoolean("userAdmin_developer", false);

        boolean isAdmin = getIntent().getBooleanExtra("userAdmin_1", false);

        if (!isAdmin) {
            finish();
        } else {
            if (isAdmin_stu || isAdmin_broad || isAdmin_student || isAdmin_developer) {
                LinearLayout linearLayout = (LinearLayout) findViewById(R.id.broadcast_send_need_permission_layout);
                linearLayout.setVisibility(View.GONE);

                LinearLayout linearLayout2 = (LinearLayout) findViewById(R.id.broadcast_send_layout);
                linearLayout2.setVisibility(View.VISIBLE);

                mTitle = (EditText) findViewById(R.id.mTitle);
                mMessage = (EditText) findViewById(R.id.mMessage);

                Button btn = (Button) findViewById(R.id.send);

                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (mTitle.length() == 0 || mMessage.length() == 0) {
                            Toast.makeText(SchoolFeedBackActivity.
                                    this, "내용을 입력해주세요 :)", Toast.LENGTH_SHORT).show();
                        } else {

                            if(isInternetCon()) {
                                Toast.makeText(SchoolFeedBackActivity.this, "Wifi나 데이터를 켜주세요 :)", Toast.LENGTH_SHORT).show();
                            } else {

                            builder = new AlertDialog.Builder(SchoolFeedBackActivity.this, R.style.AppCompatAlertDialogStyle);
                            builder.setTitle(R.string.user_info_class_up_title);
                            //                  builder.setMessage(R.string.no_network_msg);

                            // Set an EditText view to get user input
                            final EditText input = new EditText(SchoolFeedBackActivity.this);
                            builder.setView(input);

                            builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.dismiss();
                                    String value = input.getText().toString();
                                    SecurityXor securityXor = new SecurityXor();

                                    if (student.equals(securityXor.getSecurityXor(value, 8415))
                                            || broadcast.equals(securityXor.getSecurityXor(value, 3478))
                                            || school_student.equals(securityXor.getSecurityXor(value, 9641))
                                            || developer.equals(securityXor.getSecurityXor(value, 7841))) {

                                        dialog.dismiss();

                                        MaterialDialog.Builder builder = new MaterialDialog.Builder(SchoolFeedBackActivity.this);
                                        builder.title("교내 건의사항 전송");
                                        builder.content(getString(R.string.post_school_feedback_alert));
                                        builder.negativeText(android.R.string.cancel);
                                        builder.positiveText(android.R.string.ok);
                                        builder.onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                                String title = mTitle.getText().toString().trim();
                                                String message = mMessage.getText().toString().trim();

                                                if (!title.isEmpty() && !message.isEmpty()) {
                                                    (new HttpTask()).execute(title, message);
                                                }
                                            }
                                        });
                                        builder.show();
                                    }
                                }
                            });

                            try {
                                process(); //네트워크 관련은 따로 쓰레드를 생성해야 UI 쓰레드와 겹치지 않는다. 그러므로 Thread 가 선언된 process 메서드를 호출한다.
                            } catch (Exception e) {
                                Log.d("ERROR", e + "");
                            }

                        }
                    }
                }});
                        } else{
                            finish();

                    }}}


                    private void process() throws IOException {

                        new Thread() {

                            @Override
                            public void run() {

                                Handler Progress = new Handler(Looper.getMainLooper()); //네트워크 쓰레드와 별개로 따로 핸들러를 이용하여 쓰레드를 생성한다.
                                Progress.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressDialog = ProgressDialog.show(SchoolFeedBackActivity.this, "", "암호를 불러오는 중 입니다 :)");
                                    }
                                }, 0);

                                try {
                                    URL = new URL(url);
                                    InputStream html = URL.openStream();
                                    source = new Source(new InputStreamReader(html, "UTF-8")); //소스를 UTF-8 인코딩으로 불러온다.
                                    source.fullSequentialParse(); //순차적으로 구문분석
                                } catch (Exception e) {
                                    Log.d("ERROR", e + "");
                                }

                                Element BBS_TABLE = (Element) source.getAllElements(HTMLElementName.TABLE).get(0); //
                                Element BBS_TBODY = (Element) BBS_TABLE.getAllElements(HTMLElementName.TBODY).get(0); //


                                try {
                                    Element BBS_TR = (Element) BBS_TBODY.getAllElements(HTMLElementName.TR).get(1); //TR 접속

                                    Element TD_STUDENT = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(0);
                                    Element TD_BROADCAST = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(1);
                                    Element TD_SCHOOL_STUDENT = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(2);
                                    Element TD_DEVELOPER = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(3);

                                    student = TD_STUDENT.getAllElements(HTMLElementName.DIV).get(0).getContent().toString();
                                    broadcast = TD_BROADCAST.getAllElements(HTMLElementName.DIV).get(0).getContent().toString();
                                    school_student = TD_SCHOOL_STUDENT.getAllElements(HTMLElementName.DIV).get(0).getContent().toString();
                                    developer = TD_DEVELOPER.getAllElements(HTMLElementName.DIV).get(0).getContent().toString();

                                    student = Html.fromHtml(student).toString();
                                    broadcast = Html.fromHtml(broadcast).toString();
                                    school_student = Html.fromHtml(school_student).toString();
                                    developer = Html.fromHtml(developer).toString();

                                }catch(Exception e){
                                    Log.d("BCSERROR",e+"");

                                }
                                Handler mHandler = new Handler(Looper.getMainLooper());
                                mHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressDialog.dismiss(); //모든 작업이 끝나면 다이어로그 종료
                                        builder.show();
                                    }
                                }, 0);

                            }

                        }.start();


                    }

    private boolean isInternetCon() {
        cManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        mobile = cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE); //모바일 데이터 여부
        wifi = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI); //와이파이 여부
        return !mobile.isConnected() && !wifi.isConnected(); //결과값을 리턴
    }

    private class HttpTask extends AsyncTask<String, Void, Boolean> {

        /** http://itmir.tistory.com/613를 참조해주세요. */

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mDialog = new ProgressDialog(SchoolFeedBackActivity.this);
            mDialog.setIndeterminate(true);
            mDialog.setMessage(getString(R.string.post_school_feedback_posting));
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpPost postRequest = new HttpPost("");


                //전달할 값들
                Vector<NameValuePair> nameValue = new Vector<>();
                nameValue.add(new BasicNameValuePair("", ""));
                nameValue.add(new BasicNameValuePair("", params[0]));
                nameValue.add(new BasicNameValuePair("", params[1]));
                nameValue.add(new BasicNameValuePair("deviceId", Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID)));

//                nameValue.add(new BasicNameValuePair("code", HiddenCode.getHiddenCode()));

                //웹 접속 - UTF-8으로
                HttpEntity Entity = new UrlEncodedFormEntity(nameValue, "UTF-8");
                postRequest.setEntity(Entity);

                HttpClient mClient = new DefaultHttpClient();
                mClient.execute(postRequest);

                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }

        protected void onPostExecute(Boolean value) {
            super.onPostExecute(value);

            if (mDialog != null) {
                mDialog.dismiss();
                mDialog = null;
            }

            if (value) {

                MaterialDialog.Builder dialog = new MaterialDialog.Builder(SchoolFeedBackActivity.this);
                dialog.title(R.string.post_school_feedback_title);
                dialog.content(R.string.post_school_feedback_success);
                dialog.positiveText(android.R.string.ok);
                dialog.show();

                mTitle.setText("");
                mMessage.setText("");
            } else {

                MaterialDialog.Builder dialog = new MaterialDialog.Builder(SchoolFeedBackActivity.this);
                dialog.title(R.string.post_school_feedback_title);
                dialog.titleColorRes(R.color.red);
                dialog.content(R.string.post_school_feedback_failed);
                dialog.titleColorRes(R.color.red);
                dialog.positiveText(android.R.string.ok);
                dialog.positiveColorRes(R.color.red);
                dialog.show();

            }
        }
    }
}