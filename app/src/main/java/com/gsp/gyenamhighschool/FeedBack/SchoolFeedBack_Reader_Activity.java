package com.gsp.gyenamhighschool.FeedBack;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gsp.gyenamhighschool.R;
import com.gsp.gyenamhighschool.notice.NoticeAdapter;
import com.gsp.gyenamhighschool.spreadsheets.GoogleSheetTask;
import com.gsp.gyenamhighschool.tool.Database;
import com.gsp.gyenamhighschool.tool.Preference;
import com.gsp.gyenamhighschool.tool.TimeTableTool;
import com.gsp.gyenamhighschool.tool.Tools;

import java.io.File;
import java.util.ArrayList;

import static com.gsp.gyenamhighschool.tool.Tools.isOnline_Fixed;

public class SchoolFeedBack_Reader_Activity extends AppCompatActivity {
    public static final String NoticeDBName = "SchoolFeedback.db";
    public static final String NoticeTableName = "SchoolFeedback";

    boolean isAdmin_student;

    RecyclerView recyclerView;
    NoticeAdapter mAdapter;
    ProgressDialog mDialog;


    private ConnectivityManager cManager;
    private NetworkInfo mobile;
    private NetworkInfo wifi;

    com.gsp.gyenamhighschool.tool.Preference prefTheme;
    int color;

    SwipeRefreshLayout mSwipeRefreshLayout;

    ArrayList<NoticeAdapter.NoticeAdapterInfo> mListData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefTheme = new com.gsp.gyenamhighschool.tool.Preference(getApplicationContext(), "theme");

        color = prefTheme.getInt("theme", 0);


        if (color == Color.parseColor("#F44336"))
            setTheme(R.style.AppTheme_Red);
        if (color == Color.parseColor("#E91E63"))
            setTheme(R.style.AppTheme_Pink);
        if (color == Color.parseColor("#FFC107"))
            setTheme(R.style.AppTheme_Yellow);
        if (color == Color.parseColor("#8BC34A"))
            setTheme(R.style.AppTheme_Green);
        if (color == Color.parseColor("#03A9F4"))
            setTheme(R.style.AppTheme);
        if (color == Color.parseColor("#3F51B5"))
            setTheme(R.style.AppTheme_Indigo);

        setContentView(R.layout.activity_reader);


        Toolbar mToolbar = (Toolbar) findViewById(R.id.mToolbar);
        setSupportActionBar(mToolbar);

        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setHomeButtonEnabled(true);
            mActionBar.setDisplayHomeAsUpEnabled(true);

            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }


        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.mFab_reader);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNoticeData(true);
            }
        });

        boolean isAdmin = getIntent().getBooleanExtra("userAdmin_1", false); // true를 put했으므로.
        isAdmin_student = new Preference(getApplicationContext()).getBoolean("userAdmin_student", false);

        if (!isAdmin) {
            finish();
        } else {
            if(!isAdmin_student) {
                finish();
            }
        }

        recyclerView = (RecyclerView) findViewById(R.id.app_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new NoticeAdapter(this, mListData); // * 매우 중요!!! recyclerView가 setAdapter 되기 전에 선언해주어야한다.
        recyclerView.setAdapter(mAdapter);

        showNoticeData(false);


        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.mSwipeRefreshLayout);

        if (color == Color.parseColor("#F44336"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.red);
        else if (color == Color.parseColor("#E91E63"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.pink);
        else if (color == Color.parseColor("#FFC107"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.yellow);
        else if (color == Color.parseColor("#8BC34A"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.green);
        else if (color == Color.parseColor("#03A9F4"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        else if (color == Color.parseColor("#3F51B5"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.indigo);
        else
            mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showNoticeData(true);

                if (mSwipeRefreshLayout.isRefreshing())
                    mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    private void showNoticeData(boolean forceUpdate) {
        mAdapter.clearData();
        mAdapter.notifyDataSetChanged();

        if (Tools.isOnline(this)) {

            if (Tools.isWifi(this) || forceUpdate) {


                if (isOnline_Fixed()) {
                    getNoticeDownloadTask mTask = new getNoticeDownloadTask();
                    mTask.execute("");
                } else {
                    Toast.makeText(this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                }



            } else {
                if (isOnline_Fixed()) {
                    getNoticeDownloadTask mTask = new getNoticeDownloadTask();
                    mTask.execute("");
                } else {
                    Toast.makeText(this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                }
            }



        } else {
            offlineData();
        }
    }

    private void offlineData() {
        if (new File(TimeTableTool.mFilePath + NoticeDBName).exists()) {
            showListViewDate();
        } else {

            MaterialDialog.Builder builder = new MaterialDialog.Builder(this);
            builder.title(R.string.no_network_title);
            builder.titleColorRes(R.color.red);
            builder.content(getString(R.string.no_network_msg));
            builder.contentColorRes(R.color.red);
            builder.positiveText(android.R.string.ok);
            builder.positiveColorRes(R.color.red);
            builder.show();

        }

        if (mSwipeRefreshLayout.isRefreshing())
            mSwipeRefreshLayout.setRefreshing(false);
    }

    class getNoticeDownloadTask extends GoogleSheetTask {
        private Database mDatabase;
        private String[] columnFirstRow;

        @Override
        public void onPreDownload() {
            mDialog = new ProgressDialog(SchoolFeedBack_Reader_Activity.this);
            mDialog.setIndeterminate(true);
            mDialog.setMessage(getString(R.string.loading_title));
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.show();

            new File(TimeTableTool.mFilePath + NoticeDBName).delete();
            mDatabase = new Database();
        }


        @Override
        public void onFinish(long result) {
            if (mDialog != null) {
                mDialog.dismiss();
                mDialog = null;
            }

            if (mDatabase != null)
                mDatabase.release();

            showListViewDate();
        }

        @Override
        public void onRow(int startRowNumber, int position, String[] row) {
            if (startRowNumber == position) {
                columnFirstRow = row;

                StringBuilder Column = new StringBuilder();

                // remove deviceId
                for (int i = 0; i < row.length - 1; i++) {
                    Column.append(row[i]);
                    Column.append(" text, ");
                }

                mDatabase.openOrCreateDatabase(TimeTableTool.mFilePath, NoticeDBName, NoticeTableName, Column.substring(0, Column.length() - 2));
            } else {
                int length = row.length;
                for (int i = 0; i < length - 1; i++) {
                    mDatabase.addData(columnFirstRow[i], row[i]);
                }
                mDatabase.commit(NoticeTableName);
            }
        }
    }

    private void showListViewDate() {
        Database mDatabase = new Database();
        mDatabase.openDatabase(TimeTableTool.mFilePath, NoticeDBName);
        Cursor mCursor = mDatabase.getData(NoticeTableName, "*");

        for (int i = 0; i < mCursor.getCount(); i++) {
            mCursor.moveToNext();

            String date = mCursor.getString(1);
            String title = mCursor.getString(2);
            String message = mCursor.getString(3);

            mAdapter.addItem(title, message, date);
        }

        mAdapter.notifyDataSetChanged();

        if (mSwipeRefreshLayout.isRefreshing())
            mSwipeRefreshLayout.setRefreshing(false);
    }

    private boolean isInternetCon() {
        cManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        mobile = cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE); //모바일 데이터 여부
        wifi = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI); //와이파이 여부
        return !mobile.isConnected() && !wifi.isConnected(); //결과값을 리턴
    }

}