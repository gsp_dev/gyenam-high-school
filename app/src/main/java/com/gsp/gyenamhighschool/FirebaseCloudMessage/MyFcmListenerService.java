package com.gsp.gyenamhighschool.FirebaseCloudMessage;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.gsp.gyenamhighschool.MainActivity;
import com.gsp.gyenamhighschool.R;

import java.util.Map;

public class MyFcmListenerService extends FirebaseMessagingService {


    private static final String TAG = "FirebaseMsgService";

    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        String title = null, msg = null;
        if (remoteMessage.getData().containsKey("key")) {
            title = remoteMessage.getData().get("title");
            msg = remoteMessage.getData().get("message");
        }

        String from = remoteMessage.getFrom();
        Map<String, String> data = remoteMessage.getData();
  //      String title = data.get("title");
//        String msg = data.get("message");

            String key = "key";
            String value_save = remoteMessage.getData().get(key);
       // String key_dialog = "key_dialog";
  //      String value_dialog = remoteMessage.getData().get(key_dialog);

        sendPushNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody(), value_save);
    }

    private void sendPushNotification(String title, String message, String value) {
        System.out.println("received message : " + message);
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("key",value);
  //      intent.putExtra("key_dialog", value_dialog);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher))
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri) //.setLights(000000255,500,2000)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        PowerManager pm = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakelock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
        wakelock.acquire(5000);

        notificationManager.notify(5183, notificationBuilder.build());
    }

}


/*
    @Override
    public void onMessageReceived(RemoteMessage message) {
        String from = message.getFrom();
        Map<String, String> data = message.getData();
        String title = data.get("title");
        String msg = data.get("message");

        // 전달 받은 정보로 뭔가를 하면 된다.
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(""));
        startActivity(intent);


    }
}
*/