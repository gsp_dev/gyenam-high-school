package com.gsp.gyenamhighschool;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

@SuppressWarnings("ValidFragment")
public class ImageFragment extends Fragment {

    private int mPageNumber;


    public static ImageFragment create(int pageNumber) {
        ImageFragment fragment = new ImageFragment();
        Bundle args = new Bundle();
        args.putInt("page", pageNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt("page");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment, container, false);
        ImageView imageView = (ImageView) rootView.findViewById(R.id.img_fragment);

        if (mPageNumber == 0){
            Glide.with(this).load(R.drawable.gyenam_one).into(imageView);
        } else if (mPageNumber == 1){
            Glide.with(this).load(R.drawable.gyenam_two).into(imageView);
        } else if (mPageNumber == 2){
            Glide.with(this).load(R.drawable.gyenam_three).into(imageView);
        } else if (mPageNumber == 3){
            Glide.with(this).load(R.drawable.gyenam_four).into(imageView);
        }else if (mPageNumber == 4){
            Glide.with(this).load(R.drawable.gyenam_five).into(imageView);
        } else if (mPageNumber == 5){
            Glide.with(this).load(R.drawable.gyenam_six).into(imageView);
        } else if (mPageNumber == 6){
            Glide.with(this).load(R.drawable.gyenam_seven).into(imageView);
        } else if (mPageNumber == 7){
            Glide.with(this).load(R.drawable.gyenam_eight).into(imageView);
        }
        return rootView;
    }
}