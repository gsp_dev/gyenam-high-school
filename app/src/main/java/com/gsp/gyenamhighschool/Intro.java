package com.gsp.gyenamhighschool;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Toast;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntro2;
import com.github.paolorotolo.appintro.AppIntro2Fragment;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;
import android.Manifest;

public final class Intro extends AppIntro2 {

    private long backKeyPressedTime = 0;
    Toast toast;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    //단말기 OS 버전이 마시멜로 이상일 때.....처리 코드

                    Toast.makeText(Intro.this, "권한이 수락되었어요 :)", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(Intro.this, "권한이 거부되었어요..\n" /* + deniedPermissions.toString() */, Toast.LENGTH_SHORT).show();
        }


        };

        new TedPermission(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("권한을 거부하시면 파일 다운로드 기능을 사용할 수 없어요 ㅠㅠ\n\n설정 -> 권한에서 권한을 켜주세요 :)")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();


        // Instead of fragments, you can also use our default slide
        // Just set a title, description, background and image. AppIntro will do the rest.
        addSlide(AppIntro2Fragment.newInstance("계남고등학교 어플리케이션", "계남고 컴퓨터 동아리 GSP에서 제작한 계남고 어플리케이션 입니다.", R.drawable.intro_one, getResources().getColor(R.color.indigo)));
        addSlide(AppIntro2Fragment.newInstance("다양한 기능", "계남고등학교의 편의를 위한\n다양한 기능들이 존재해요\n믿고 써보세요!", R.drawable.intro_two, getResources().getColor(R.color.pink)));
        addSlide(AppIntro2Fragment.newInstance("테마 색상", "언제든지 앱의 테마 색상을\n원하는 색으로 적용이 가능해요", R.drawable.intro_three, getResources().getColor(R.color.blue)));
        addSlide(AppIntro2Fragment.newInstance("설정", "설정 기능은 앱의 기능을\n향상 시켜 줍니다. 잊지 마세요!", R.drawable.intro_four, getResources().getColor(R.color.green)));
        addSlide(AppIntro2Fragment.newInstance("등급", "계남인이라면, 암호를 입력하여 등급을 업그레이드 할 수 있어요\n계남인 만이 사용할 수 있는\n기능을 제공해요!", R.drawable.intro_five, getResources().getColor(R.color.yellow)));
        addSlide(AppIntro2Fragment.newInstance("마지막으로..", "최신 버전을 유지해주세요!\n계속해서 기능이 추가될 예정이에요\n이용해주셔서 감사합니다 :)", R.drawable.intro_six, getResources().getColor(R.color.red)));

        // OPTIONAL METHODS
        // Override bar/separator color.
  //      setBarColor(Color.parseColor("#3F51B5"));
//        setSeparatorColor(Color.parseColor("#2196F3"));

        // Hide Skip/Done button.

        setProgressButtonEnabled(true);
        setImageSkipButton(null);
 //       showSkipButton(false);


    }
/*
    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.
    }
*/
    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        finish();
        getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                .edit()
                .putBoolean("firstrun", false)
                .commit();
        // Do something when users tap on Done button.
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }
    @Override
    public void onBackPressed(){
        if (System.currentTimeMillis() > backKeyPressedTime + 2000) {
            backKeyPressedTime = System.currentTimeMillis();
            toast = Toast.makeText(this, "\'뒤로\'버튼을 한번 더 누르시면 종료됩니다.", Toast.LENGTH_SHORT);
            toast.show();
            return;
        }
        if (System.currentTimeMillis() <= backKeyPressedTime + 2000) {
            moveTaskToBack(true);
            finish();
            android.os.Process.killProcess(android.os.Process.myPid());
            toast.cancel();
        }
    }
}