package com.gsp.gyenamhighschool;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.gsp.gyenamhighschool.FeedBack.FeedBackActivity;
import com.gsp.gyenamhighschool.FeedBack.SchoolFeedBackActivity;
import com.gsp.gyenamhighschool.FeedBack.SchoolFeedBack_Reader_Activity;
import com.gsp.gyenamhighschool.notice.NoticeActivity;
import com.gsp.gyenamhighschool.tool.Preference;
import com.viewpagerindicator.CirclePageIndicator;

import static com.gsp.gyenamhighschool.tool.Tools.isOnline_Fixed;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    private AutoScrollViewPager mViewPager;
    AppBarLayout mAppBarLayout;
    SectionsPagerAdapter mSectionsPagerAdapter;
    NavigationView navigationView;
 //   MyPagerAdapter myPagerAdapter;
    float appbar_contents_height;
    float toolbar_height;
    float criteria;
    Toolbar toolbar;
    View view_ad_root;
    TextView tv_toolbar_title;
    CollapsingToolbarLayout ctl;
    com.gsp.gyenamhighschool.tool.Preference prefTheme;
    int color;

    String[] str, str2;

    boolean isAdmin_stu, isAdmin_broad, isAdmin_student, isAdmin_developer;

    private ConnectivityManager cManager;
    private NetworkInfo mobile;
    private NetworkInfo wifi;

    String value_save;// value_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        final boolean firstrun = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getBoolean("firstrun", true);
        if (firstrun)
        {
            startActivity(new Intent(MainActivity.this, Intro.class));
        }

        prefTheme = new com.gsp.gyenamhighschool.tool.Preference(getApplicationContext(),"theme");

        color = prefTheme.getInt("theme",0);

        if (color == Color.parseColor("#F44336"))
            setTheme(R.style.AppTheme_Red_Main);
        if (color == Color.parseColor("#E91E63"))
            setTheme(R.style.AppTheme_Pink_Main);
        if (color == Color.parseColor("#FFC107"))
            setTheme(R.style.AppTheme_Yellow_Main);
        if (color == Color.parseColor("#8BC34A"))
            setTheme(R.style.AppTheme_Green_Main);
        if (color == Color.parseColor("#03A9F4"))
            setTheme(R.style.AppTheme_Main);
        if (color == Color.parseColor("#3F51B5"))
            setTheme(R.style.AppTheme_Indigo_Main);

        FirebaseMessaging.getInstance().subscribeToTopic("notice");

        if (getIntent().getExtras() != null) {
            String key = "key";
            //       String key_dialog = "key_dialog";
            //  value_save = getIntent().getExtras().getString(key);
            value_save = getIntent().getStringExtra("key");
            //      value_dialog = getIntent().getStringExtra("key_dialog");
            Log.d("FCM_Activity", "Key: " + key + " Value: " + value_save); //"Key_Dialog :" + key_dialog + " Value : " + value_dialog);
        }

        setContentView(R.layout.main);

        if (getIntent().getExtras() != null) {
            String key = "key";
     //       String key_dialog = "key_dialog";
            //  value_save = getIntent().getExtras().getString(key);
            value_save = getIntent().getStringExtra("key");
      //      value_dialog = getIntent().getStringExtra("key_dialog");
            Log.d("FCM_Activity", "Key: " + key + " Value: " + value_save); //"Key_Dialog :" + key_dialog + " Value : " + value_dialog);
        }

        FirebaseMessaging.getInstance().subscribeToTopic("notice");

        if(value_save != null /*|| value_dialog != null*/) {
            if (value_save.equals("update")) { // key : update
                this.finish();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.gsp.gyenamhighschool"));
                startActivity(intent);
            } else if (value_save.equals("school_notice")) {
                // this.finish();
                startActivity(new Intent(this, NoticeActivity.class));
            }

        }


        toolbar = (Toolbar) findViewById(R.id.toolbar); // Toolbar id
        setSupportActionBar(toolbar); // Toolbar setSupportActionBar

        str = getResources().getStringArray(R.array.feedback_values);
        str2 = getResources().getStringArray(R.array.feedback_student_values);

        view_ad_root = findViewById(R.id.backdrop); // include

        tv_toolbar_title = (TextView) findViewById(R.id.tv_toolbar_title); // Toolbar TextView id
        tv_toolbar_title.setText("계남고"); // Toolbar Title(TextView)

        mAppBarLayout = (AppBarLayout)findViewById(R.id.appbar); // AppBarLayout id


        AppBarLayout.OnOffsetChangedListener mListener = new AppBarLayout.OnOffsetChangedListener() { // AppBarLayout에 OnOffsetListener를 하나 만들어준다.
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (toolbar_height == 0.0f) {
                    toolbar_height = (float) toolbar.getHeight();
                    criteria = toolbar_height;
                }
                if (appbar_contents_height == 0.0f) {
                    appbar_contents_height = (float) view_ad_root.getHeight();
                }
                tv_toolbar_title.setAlpha((((float) (-verticalOffset)) - ((appbar_contents_height - toolbar_height) - criteria)) / criteria);

            }
        };
        mAppBarLayout.addOnOffsetChangedListener(mListener); //Listener 적용(set)

        showUpdateNotification();

        mSectionsPagerAdapter = new SectionsPagerAdapter(getApplicationContext(), getSupportFragmentManager()); // FragmentAdapter

        mViewPager = (AutoScrollViewPager) findViewById(R.id.image_viewpager); // AutoScrollViewPager id
        mViewPager.setAdapter(mSectionsPagerAdapter); // AutoScrollViewPager에 SectionsPagerAdapter를 지정해준다.

        CirclePageIndicator circlePageIndicator = (CirclePageIndicator) findViewById(R.id.indicator); // Indicator
        circlePageIndicator.setViewPager(mViewPager); // ViewPager에 Indicator를 달아준다.
        mViewPager.setOffscreenPageLimit(1);

        mViewPager.setInterval(5000); // 3초마다 자동 스크롤
        mViewPager.startAutoScroll(); // 자동 스크롤 시작
        mViewPager.setBorderAnimation(false); // 맨 뒤의 Fragment가 맨 앞의 Fragment로 전환될 때 애니메이션 x
        mViewPager.setCurrentItem(0);
        mViewPager.setAutoScrollDurationFactor(4);


        ctl = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        ctl.setCollapsedTitleTextColor(Color.TRANSPARENT); // TitleColor TRANSPARENT
        ctl.setExpandedTitleColor(Color.TRANSPARENT); // TitleColor TRANSPARENT

        FragmentTransaction fragtrans = getSupportFragmentManager().beginTransaction();
        fragtrans.replace(R.id.container, new MainFragment());
        fragtrans.commitAllowingStateLoss();


        isAdmin_stu = new Preference(this).getBoolean("userAdmin_stu", false);
        isAdmin_broad = new Preference(this).getBoolean("userAdmin_broad", false);
        isAdmin_student = new Preference(this).getBoolean("userAdmin_student", false);
        isAdmin_developer = new Preference(this).getBoolean("userAdmin_developer", false);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                if (isAdmin_stu || isAdmin_broad || isAdmin_developer) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.AppCompatAlertDialogStyle);
                    builder.setTitle("문의 및 건의");
                    builder.setItems(str, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {

                                case 0:
                                    startActivity(new Intent(MainActivity.this, FeedBackActivity.class));
                                    // 앱에 대하여 문의하기
                                    break;
                                case 1:

                                    // 계남인, 방송부, 개발자 등급
                                    if (isAdmin_stu || isAdmin_broad || isAdmin_developer) {

                                        // 학교에 대하여 문의하기

                                        if(isInternetCon()) {
                                            Toast.makeText(MainActivity.this, "Wifi나 데이터를 켜주세요 :)", Toast.LENGTH_SHORT).show();
                                        } else {
                                            if (isOnline_Fixed()) {
                                                startActivity(new Intent(MainActivity.this, SchoolFeedBackActivity.class).putExtra("userAdmin_1", true));
                                            } else {
                                                Toast.makeText(MainActivity.this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                    } else {
                                        Snackbar.make(view, "계남인 이상의 권한이 필요해요. 설정에서 권한을 설정해주세요 :)", Snackbar.LENGTH_SHORT).show();
                                    }
                                    break;
                            }
                        }
                    }).show();

                } else if (isAdmin_student) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.AppCompatAlertDialogStyle);
                    builder.setTitle("문의 및 건의");
                    builder.setItems(str2, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    startActivity(new Intent(MainActivity.this, FeedBackActivity.class));
                                    break;
                                case 1:
                                    if (isAdmin_stu || isAdmin_broad || isAdmin_student || isAdmin_developer) {

                                        if(isInternetCon()) {
                                            Toast.makeText(MainActivity.this, "Wifi나 데이터를 켜주세요 :)", Toast.LENGTH_SHORT).show();
                                        } else {
                                            startActivity(new Intent(MainActivity.this, SchoolFeedBackActivity.class).putExtra("userAdmin_1", true));
                                        }

                                    } else {
                                        Snackbar.make(view, "계남인 이상의 권한이 필요해요. 설정에서 권한을 설정해주세요 :)", Snackbar.LENGTH_SHORT).show();
                                    }
                                    break;
                                case 2:
                                    if (isAdmin_student) {

                                        if(isInternetCon()) {
                                            Toast.makeText(MainActivity.this, "Wifi나 데이터를 켜주세요 :)", Toast.LENGTH_SHORT).show();
                                        } else {
                                            startActivity(new Intent(MainActivity.this, SchoolFeedBack_Reader_Activity.class).putExtra("userAdmin_1", true));
                                        }
                                    } else {
                                        Snackbar.make(view, getString(R.string.user_info_require_message_school_read), Snackbar.LENGTH_SHORT).show();
                                    }
                                    break;
                            }
                        }
                    }).show();

        } else if (!isAdmin_stu && !isAdmin_broad && !isAdmin_student && !isAdmin_developer) {
            startActivity(new Intent(MainActivity.this, FeedBackActivity.class));
        }
    }

});

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
        this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        }



    private void showUpdateNotification() {
        try {
            Preference mPref = new Preference(getApplicationContext());
            PackageManager packageManager = getPackageManager();
            PackageInfo info = packageManager.getPackageInfo(getPackageName(), PackageManager.GET_META_DATA);

            int versionCode = info.versionCode;

            if (mPref.getInt("versionCode", 0) != versionCode) {
                mPref.putInt("versionCode", versionCode);

                MaterialDialog.Builder builder =
                        new MaterialDialog.Builder(this);
                builder.title("공지 및 주의사항");
                builder.content(getString(R.string.update_content));
                builder.positiveText(android.R.string.ok);
                builder.show();
            }

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }



public void onStart() {
        super.onStart();

/*
    if (getIntent().getExtras() != null) {
        String key = "key";
      //  value_save = getIntent().getExtras().getString(key);
        value_save = getIntent().getStringExtra("key");
        Log.d("FCM_Activity", "Key: " + key + " Value: " + value_save);
    }

    FirebaseMessaging.getInstance().subscribeToTopic("notice");

    if(value_save != null) {
        if (value_save.equals("update")) { // key : update
            this.finish();
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.gsp.gyenamhighschool"));
            startActivity(intent);
        } else if (value_save.equals("school_notice")) {
            this.finish();
            startActivity(new Intent(this, NoticeActivity.class));
        }
    }
*/


        isAdmin_stu = new Preference(this).getBoolean("userAdmin_stu", false);
        isAdmin_broad = new Preference(this).getBoolean("userAdmin_broad", false);
        isAdmin_student = new Preference(this).getBoolean("userAdmin_student", false);
        isAdmin_developer = new Preference(this).getBoolean("userAdmin_developer", false);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                if (isAdmin_stu || isAdmin_broad || isAdmin_developer) { //학생회 권한을 제외한 다른 권한을 가지고 있을 경우
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.AppCompatAlertDialogStyle);
                    builder.setTitle("문의 및 건의");
                    builder.setItems(str, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    startActivity(new Intent(MainActivity.this, FeedBackActivity.class));
                                    break;
                                case 1:
                                    if (isAdmin_stu || isAdmin_broad || isAdmin_developer) {
                                        startActivity(new Intent(MainActivity.this, SchoolFeedBackActivity.class).putExtra("userAdmin_1", true));
                                    } else {
                                        Snackbar.make(view, "계남인 이상의 권한이 필요해요. 설정에서 권한을 설정해주세요 :)", Snackbar.LENGTH_SHORT).show();
                                    }
                                    break;
                            }
                        }
                    }).show();

                } else if (isAdmin_student) { //학생회 권한을 가지고 있을 경우
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.AppCompatAlertDialogStyle);
                    builder.setTitle("문의 및 건의");
                    builder.setItems(str2, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    startActivity(new Intent(MainActivity.this, FeedBackActivity.class));
                                    break;
                                case 1:
                                    if (isAdmin_stu || isAdmin_broad || isAdmin_student || isAdmin_developer) {
                                        startActivity(new Intent(MainActivity.this, SchoolFeedBackActivity.class).putExtra("userAdmin_1", true));
                                    } else {
                                        Snackbar.make(view, "계남인 이상의 권한이 필요해요. 설정에서 권한을 설정해주세요 :)", Snackbar.LENGTH_SHORT).show();
                                    }
                                    break;
                                case 2:
                                    if (isAdmin_student) {
                                        if (isInternetCon()) {
                                            Snackbar.make(view, "Wifi나 데이터를 켜주세요 :)", Snackbar.LENGTH_SHORT).show();
                                        } else {
                                            startActivity(new Intent(MainActivity.this, SchoolFeedBack_Reader_Activity.class).putExtra("userAdmin_1", true));
                                        }
                                    } else {
                                        Snackbar.make(view, getString(R.string.user_info_require_message_school_read), Snackbar.LENGTH_SHORT).show();
                                    }
                                    break;
                            }

                        }
                    }).show();

                } else if (!isAdmin_stu && !isAdmin_broad && !isAdmin_student && !isAdmin_developer) {
                    startActivity(new Intent(MainActivity.this, FeedBackActivity.class));
                }
            }
        });

        }

@Override
public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
        drawer.closeDrawer(GravityCompat.START);
        }
    if (!drawer.isDrawerOpen(GravityCompat.START)) {
        new MaterialDialog.Builder(this)
                .content("종료 할까요?")
                .positiveText("아니오")
                .negativeText("예")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        // TODO

                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        // TODO
                        moveTaskToBack(true);
                        finish();
                        android.os.Process.killProcess(android.os.Process.myPid());
                    }
                })
                .show();
    }

      //      super.onBackPressed();
        }


@Override
public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
        }

@Override
public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
        startActivity(new Intent(this,SettingActivity.class));
        return true;
        }

        return super.onOptionsItemSelected(item);
        }

@SuppressWarnings("StatementWithEmptyBody")
@Override
public boolean onNavigationItemSelected(MenuItem item) {
    // Handle navigation view item clicks here.
    int id = item.getItemId();

    if (id == R.id.rate_play_store) {

        Uri uri = Uri.parse("market://details?id=com.gsp.gyenamhighschool");
        Intent it = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(it);

    } else if (id == R.id.change_log) {
        // showChangeLog
        new MaterialDialog.Builder(this)
                .title("업데이트 내역")
                .content(getString(R.string.changeLog_msg))
                .positiveText("닫기")
                .show();

        } else if (id == R.id.setting) {

            startActivity(new Intent(this, SettingActivity.class));

        } else if (id == R.id.share) {

            Intent intent1 = new Intent(Intent.ACTION_SEND);
            intent1.putExtra(Intent.EXTRA_SUBJECT, "계남고등학교 어플리케이션");
            intent1.setType("text/plain");
            intent1.putExtra(Intent.EXTRA_TEXT, "계남고등학교 어플리케이션으로 계남고에 대한 여러가지 정보를 얻어보세요!\n" +
                    "계남고를 위한 다양한 편의기능을 제공하니 많은 이용 바랄게요 :)"+"\n\nhttps://play.google.com/store/apps/details?id=com.gsp.gyenamhighschool");
            startActivity(Intent.createChooser(intent1, "공유하기"));

        } else if (id == R.id.license) {
            //showDialog

        if (isInternetCon()) { //false 반환시 if 문안의 로직 실행
            Toast.makeText(this, "Wifi나 데이터를 켜주세요 :)", Toast.LENGTH_SHORT).show();
        } else {

            MaterialDialog dialog = new MaterialDialog.Builder(MainActivity.this)
                    .title("오픈소스 라이선스")
                    .customView(R.layout.open_source_license, true)
                    .positiveText("닫기")
                    .build();
            WebView webView = (WebView) dialog.getCustomView().findViewById(R.id.viewpager_indicator);
            webView.loadUrl("file:///android_asset/viewpager_indicator.html");
            WebView webView2 = (WebView) dialog.getCustomView().findViewById(R.id.photoview);
            webView2.loadUrl("https://raw.githubusercontent.com/chrisbanes/PhotoView/master/LICENSE");
            WebView webView3 = (WebView) dialog.getCustomView().findViewById(R.id.material_dialog_webview);
            webView3.loadUrl("https://raw.githubusercontent.com/afollestad/material-dialogs/master/LICENSE.txt");
            WebView webView4 = (WebView) dialog.getCustomView().findViewById(R.id.app_intro);
            webView4.loadUrl("https://raw.githubusercontent.com/paolorotolo/AppIntro/master/LICENSE");
            WebView webView5 = (WebView) dialog.getCustomView().findViewById(R.id.glide);
            webView5.loadUrl("https://raw.githubusercontent.com/bumptech/glide/master/LICENSE");
            WebView webView6 = (WebView) dialog.getCustomView().findViewById(R.id.date_time_picker);
            webView6.loadUrl("file:///android_asset/date_time_picker.html");
            WebView webView7 = (WebView) dialog.getCustomView().findViewById(R.id.auto_scroll_viewpager);
            webView7.loadUrl("https://raw.githubusercontent.com/Trinea/android-auto-scroll-view-pager/master/LICENSE");
            WebView webView8 = (WebView) dialog.getCustomView().findViewById(R.id.tedpermission);
            webView8.loadUrl("file:///android_asset/tedpermission.html");

            dialog.show();
        }
        } else if (id == R.id.developer_info) {
        startActivity(new Intent(this, DeveloperInfoActivity.class));
        } else if(id == R.id.close_drawer) {

    }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private boolean isInternetCon() {
        cManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        mobile = cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE); //모바일 데이터 여부
        wifi = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI); //와이파이 여부
        return !mobile.isConnected() && !wifi.isConnected(); //결과값을 리턴
    }

public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

    Context mContext;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        return ImageFragment.create(position);
    }

    @Override
    public int getCount() {
        // Show 8 total pages.
        return 8;
    }
}

    @Override
    protected void onPause() {
        super.onPause();
        // stop auto scroll when onPause
        mViewPager.stopAutoScroll();
       // mAppBarLayout.addOnOffsetChangedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // start auto scroll when onResume
        mViewPager.startAutoScroll();
      //  mAppBarLayout.removeOnOffsetChangedListener(this);
    }
}