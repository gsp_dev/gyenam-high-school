package com.gsp.gyenamhighschool;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("ValidFragment")
public class MainFragment extends Fragment {

    private Context mContext;

    TabLayout tabLayout;
    ViewPager pager;

    public static MainFragment newInstance(int index) {
        MainFragment pane = new MainFragment();

        Bundle args = new Bundle();
        args.putInt("code", index);
        pane.setArguments(args);

        return pane;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.main_fragment, container, false);

        Adapter mAdapter = new Adapter(getChildFragmentManager());

        mAdapter.addFragment(getString(R.string.tab1), fragment1.getInstance(1));
        mAdapter.addFragment(getString(R.string.tab2), fragment2.getInstance(2));

        pager = (ViewPager) rootView.findViewById(R.id.pager);
        pager.setAdapter(mAdapter);

        tabLayout = (TabLayout) rootView.findViewById(R.id.tab);
        tabLayout.setupWithViewPager(pager);

        return rootView;


    }


    public class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        public void addFragment(String mTitle, Fragment mFragment) {
            mFragments.add(mFragment);
            mFragmentTitles.add(mTitle);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }

}