package com.gsp.gyenamhighschool.Schedule;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gsp.gyenamhighschool.R;
import com.gsp.gyenamhighschool.tool.RecyclerItemClickListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class ScheduleFragment extends Fragment {

    public static Fragment getInstance(int month) {
        ScheduleFragment mFragment = new ScheduleFragment();

        Bundle args = new Bundle();
        args.putInt("month", month);
        mFragment.setArguments(args);

        return mFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RecyclerView recyclerView = (RecyclerView) inflater.inflate(R.layout.recyclerview, container, false);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));

        final ScheduleAdapter mAdapter = new ScheduleAdapter();
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View mView, int position) {


/*
                try {
                    String date = mAdapter.getItemData(position).date;
                    SimpleDateFormat mFormat = new SimpleDateFormat("yyyy.MM.dd (E)", Locale.KOREA);

                    Calendar mCalendar = Calendar.getInstance();
                    long nowTime = mCalendar.getTimeInMillis();

                    mCalendar.setTime(mFormat.parse(date));
                    long touchTime = mCalendar.getTimeInMillis();

                    long diff = (touchTime - nowTime);

                    boolean isPast = false;
                    if (diff < 0) {
                        diff = -diff;
                        isPast = true;
                    }

                    int diffDays = (int) (diff/= 24 * 60 * 60 * 1000);
                    String mText = "";

                    if (diffDays == 0)
                        mText += "오늘 일정입니다.";
                    else if (isPast)
                        mText = "선택하신 날짜는 " + diffDays + "일전 날짜입니다.";
                    else
                        mText = "선택하신 날짜까지 " + diffDays + "일 남았습니다.";

                    Snackbar.make(mView, mText, Snackbar.LENGTH_SHORT).show();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                */

            }
        }));

        Bundle args = getArguments();
        int month = args.getInt("month");

        switch (month) {
            case 3:
                mAdapter.addItem("3.1절", "2017.03.01 (수)", true);
                mAdapter.addItem("개학식 및 입학식", "2017.03.02 (목)");
                mAdapter.addItem("전국 연합 학력 평가 (3학년)", "2017.03.09 (목)");
                mAdapter.addItem("학부모 총회 / 학부모 상담주간", "2017.03.15 (수)");
                mAdapter.addItem("학부모 상담주간","2017.03.16 (목)");
                mAdapter.addItem("학부모 상담주간","2017.03.17 (금)");
                mAdapter.addItem("학부모 상담주간","2017.03.20 (월)");
                mAdapter.addItem("학부모 상담주간","2017.03.21 (화)");
                break;
            case 4:
                mAdapter.addItem("수업 공개의 날","2017.04.11 (화)");
                mAdapter.addItem("전국 연합 학력 평가 (3학년)", "2017.04.12 (수)");
                mAdapter.addItem("신체검사 (2학년, 3학년) / PAPS", "2017.04.14 (금)");
                mAdapter.addItem("영어 듣기 평가 (1학년)", "2017.04.18 (화)");
                mAdapter.addItem("영어 듣기 평가 (2학년)", "2017.04.19 (수)");
                mAdapter.addItem("영어 듣기 평가 (3학년)", "2017.04.20 (목)");
                mAdapter.addItem("1차 지필평가", "2017.04.26 (수)");
                mAdapter.addItem("1차 지필평가", "2017.04.27 (목)");
                mAdapter.addItem("1차 지필평가", "2017.04.28 (금)");
                break;
            case 5:
                mAdapter.addItem("1차 지필평가", "2017.05.01 (월)");
                mAdapter.addItem("석가탄신일", "2017.05.03 (수)", true);
                mAdapter.addItem("재량휴업일", "2017.05.04 (목)", true);
                mAdapter.addItem("어린이날", "2017.05.05 (금)", true);
                mAdapter.addItem("테마형 수련활동 (1학년) / 주제별 체험학습 (2학년)", "2017.05.15 (월)");
                mAdapter.addItem("테마형 수련활동 (1학년) / 주제별 체험학습 (2학년)", "2017.05.16 (화)");
                mAdapter.addItem("테마형 수련활동 (1학년) / 주제별 체험학습 (2학년)", "2017.05.17 (수)");
                mAdapter.addItem("체육대회 (1학년, 2학년) / 현장 체험학습 (3학년)", "2017.05.19 (금)");
                break;
            case 6:
                mAdapter.addItem("전국연합 학력 평가 (1학년, 2학년) / 대수능 모의평가 (3학년)", "2017.06.01 (목)");
                mAdapter.addItem("현충일", "2017.06.06 (월)", true);
                break;
            case 7:
                mAdapter.addItem("2차 지필평가", "2017.07.03 (월)");
                mAdapter.addItem("2차 지필평가", "2017.07.04 (화)");
                mAdapter.addItem("2차 지필평가", "2017.07.05 (수)");
                mAdapter.addItem("2차 지필평가", "2017.07.06 (목)");
                mAdapter.addItem("전국 연합 학력 평가 (3학년)", "2017.07.12 (수)");
                mAdapter.addItem("화요일 수업 진행", "2017.07.13 (목)");
                mAdapter.addItem("평가회", "2017.07.20 (목)");
                mAdapter.addItem("방학식", "2017.07.21 (금)");
                break;
            case 8:
                mAdapter.addItem("광복절","2017.08.15 (화)", true);
                mAdapter.addItem("개학식", "2017.08.16 (수)");
                break;
            case 9:
                mAdapter.addItem("대수능 모의평가 (3학년)", "2017.09.06 (수)");
                mAdapter.addItem("영어 듣기 평가 (1학년)", "2017.09.19 (화)");
                mAdapter.addItem("영어 듣기 평가 (2학년)", "2017.09.20 (수)");
                mAdapter.addItem("영어 듣기 평가 (3학년)", "2017.09.21 (목)");
                break;
            case 10:
                mAdapter.addItem("재량휴업일", "2017.10.02 (월)", true);
                mAdapter.addItem("개천절", "2017.10.03 (화)", true);
                mAdapter.addItem("추석", "2017.10.04 (수)", true);
                mAdapter.addItem("추석", "2017.10.05 (목)", true);
                mAdapter.addItem("대체휴일", "2017.10.06 (금)", true);
                mAdapter.addItem("한글날", "2017.10.09 (월)", true);
                mAdapter.addItem("학부모 상담주간","2017.10.16 (월)");
                mAdapter.addItem("전국 연합 학력 평가 (3학년) / 학부모 상담주간","2017.10.17 (화)");
                mAdapter.addItem("1차 지필평가 / 학부모 상담주간", "2017.10.18 (수)");
                mAdapter.addItem("1차 지필평가 / 학부모 상담주간", "2017.10.19 (목)");
                mAdapter.addItem("1차 지필평가 / 학부모 상담주간", "2017.10.20 (금)");
                mAdapter.addItem("1차 지필평가 / 학부모 상담주간", "2017.10.23 (월)");
                break;
            case 11:
                mAdapter.addItem("동아리 발표회", "2017.11.03 (금)");
                mAdapter.addItem("대학 수학 능력 시험", "2017.11.16 (목)", true);
                mAdapter.addItem("2차 지필평가 (3학년)", "2017.11.20 (월)");
                mAdapter.addItem("2차 지필평가 (3학년)", "2017.11.21 (화)");
                mAdapter.addItem("2차 지필평가 (3학년) / 전국 연합 학력 평가 (1학년, 2학년)", "2017.11.22 (수)");
                mAdapter.addItem("2차 지필평가 (3학년)", "2017.11.23 (목)");
                mAdapter.addItem("2차 지필평가 (3학년)", "2017.11.24 (금)");
                break;
            case 12:
                mAdapter.addItem("2차 지필평가 (1학년, 2학년)", "2017.12.08 (금)");
                mAdapter.addItem("2차 지필평가 (1학년, 2학년)", "2017.12.11 (월)");
                mAdapter.addItem("2차 지필평가 (1학년, 2학년)", "2017.12.12 (화)");
                mAdapter.addItem("2차 지필평가 (1학년, 2학년)", "2017.12.13 (수)");
                mAdapter.addItem("월요일 수업", "2017.12.19 (화)");
                mAdapter.addItem("대통령 선거", "2017.12.20 (수)", true);
                mAdapter.addItem("성탄절", "2017.12.25 (월)", true);
                mAdapter.addItem("평가회", "2017.12.27 (수)");
                mAdapter.addItem("방학식", "2017.12.28 (목)");
                break;
            case 1:
                mAdapter.addItem("신정", "2018.01.01 (월)", true);
                break;
            case 2:
                mAdapter.addItem("개학식 / 신입생 예비소집", "2018.02.01 (수)");
                mAdapter.addItem("졸업식", "2018.02.08 (목)");
                mAdapter.addItem("종업식", "2018.02.09 (금)");
                break;
        }

        return recyclerView;
    }
}
