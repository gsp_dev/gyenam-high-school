package com.gsp.gyenamhighschool;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.provider.Settings;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.color.ColorChooserDialog;
import com.gsp.gyenamhighschool.autoupdate.updateAlarm;
import com.gsp.gyenamhighschool.tool.SecurityXor;
import com.gsp.gyenamhighschool.tool.SwitchCompatPreference;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;
import net.htmlparser.jericho.StartTag;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.Objects;


public class SettingActivity extends AppCompatActivity implements ColorChooserDialog.ColorCallback{

    int accentPreselect;
    Toolbar mToolbar;
      com.gsp.gyenamhighschool.tool.Preference prefTheme;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefTheme = new com.gsp.gyenamhighschool.tool.Preference(getApplicationContext(),"theme");

        int color = prefTheme.getInt("theme",0);

        if (color == Color.parseColor("#F44336"))
            setTheme(R.style.AppTheme_Red);
        if (color == Color.parseColor("#E91E63"))
            setTheme(R.style.AppTheme_Pink);
        if (color == Color.parseColor("#FFC107"))
            setTheme(R.style.AppTheme_Yellow);
        if (color == Color.parseColor("#8BC34A"))
            setTheme(R.style.AppTheme_Green);
        if (color == Color.parseColor("#03A9F4"))
            setTheme(R.style.AppTheme);
        if (color == Color.parseColor("#3F51B5"))
            setTheme(R.style.AppTheme_Indigo);



        setContentView(R.layout.setting);
        mToolbar = (Toolbar) findViewById(R.id.mToolbar);
        setSupportActionBar(mToolbar);




        if (color == Color.parseColor("#F44336"))
            mToolbar.setBackgroundColor(color);
        if (color == Color.parseColor("#E91E63"))
            mToolbar.setBackgroundColor(color);
        if (color == Color.parseColor("#FFC107"))
            mToolbar.setBackgroundColor(color);
        if (color == Color.parseColor("#8BC34A"))
            mToolbar.setBackgroundColor(color);
        if (color == Color.parseColor("#03A9F4"))
            mToolbar.setBackgroundColor(color);
        if (color == Color.parseColor("#3F51B5"))
            mToolbar.setBackgroundColor(color);


        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setHomeButtonEnabled(true);
            mActionBar.setDisplayHomeAsUpEnabled(true);

            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction().replace(R.id.container, new PrefsFragment()).commit();
    }



    public static class PrefsFragment extends PreferenceFragment{

        private ConnectivityManager cManager;
        private NetworkInfo mobile;
        private NetworkInfo wifi;

        private String url = ""; //파싱할 URL
        private URL URL;

        private Source source;
        private ProgressDialog progressDialog;
        private AlertDialog.Builder builder;
        public static String student, broadcast, school_student, developer;

        private boolean isInternetCon() {
            cManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            mobile = cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE); //모바일 데이터 여부
            wifi = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI); //와이파이 여부
            return !mobile.isConnected() && !wifi.isConnected(); //결과값을 리턴
        }
/*
        public String RemoveHTMLTag(String changeStr) {
            if (changeStr != null && !changeStr.equals("")) {
                changeStr = changeStr.replaceAll("<(/)?([a-zA-Z]*)(\\s[a-zA-Z]*=[^>]*)?(\\s)*(/)?>", "");
            } else {
                changeStr = "";
            }
            return changeStr;
        }

        public static String toText(String str) {
            if (str == null)
                return null;

            String returnStr = str;
            returnStr = returnStr.replaceAll("<br>","\n");
            returnStr = returnStr.replaceAll("&gt;",">");
            returnStr = returnStr.replaceAll("&lt;","<");
            returnStr = returnStr.replaceAll("&quot;","\"");
            returnStr = returnStr.replaceAll("&nbsp;"," ");
            returnStr = returnStr.replaceAll("&amp;","&");
            returnStr = returnStr.replaceAll("&#34;", "\"");
            returnStr = returnStr.replaceAll("&#39;","\'");

            return returnStr;
        }
*/
        private void process() throws IOException{

            new Thread() {

                @Override
                public void run() {

                    Handler Progress = new Handler(Looper.getMainLooper()); //네트워크 쓰레드와 별개로 따로 핸들러를 이용하여 쓰레드를 생성한다.
                    Progress.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog = ProgressDialog.show(getActivity(), "", "암호를 불러오는 중 입니다 :)");
                        }
                    }, 0);

                    try {
                        URL = new URL(url);
                        InputStream html = URL.openStream();
                        source = new Source(new InputStreamReader(html, "UTF-8")); //소스를 UTF-8 인코딩으로 불러온다.
                        source.fullSequentialParse(); //순차적으로 구문분석
                    } catch (Exception e) {
                        Log.d("ERROR", e + "");
                    }

                    Element BBS_TABLE = (Element) source.getAllElements(HTMLElementName.TABLE).get(0); //
                    Element BBS_TBODY = (Element) BBS_TABLE.getAllElements(HTMLElementName.TBODY).get(0); //

                    try {
                        Element BBS_TR = (Element) BBS_TBODY.getAllElements(HTMLElementName.TR).get(1); //TR 접속

                        Element TD_STUDENT = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(0);
                        Element TD_BROADCAST = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(1);
                        Element TD_SCHOOL_STUDENT = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(2);
                        Element TD_DEVELOPER = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(3);

                        student = TD_STUDENT.getAllElements(HTMLElementName.DIV).get(0).getContent().toString();
                        broadcast = TD_BROADCAST.getAllElements(HTMLElementName.DIV).get(0).getContent().toString();
                        school_student = TD_SCHOOL_STUDENT.getAllElements(HTMLElementName.DIV).get(0).getContent().toString();
                        developer = TD_DEVELOPER.getAllElements(HTMLElementName.DIV).get(0).getContent().toString();

                        student = Html.fromHtml(student).toString();
                        broadcast = Html.fromHtml(broadcast).toString();
                        school_student = Html.fromHtml(school_student).toString();
                        developer = Html.fromHtml(developer).toString();

                    }catch(Exception e){
                        Log.d("BCSERROR",e+"");

                    }
                    Handler mHandler = new Handler(Looper.getMainLooper());
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss(); //모든 작업이 끝나면 다이어로그 종료
                            builder.show();
                        }
                    }, 0);

                }

            }.start();


        }


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.pref_settings);

            final SwitchCompatPreference switchCompatPreference = (SwitchCompatPreference) findPreference("theme");
            switchCompatPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {

                    if (switchCompatPreference.isChecked()) {

                        com.gsp.gyenamhighschool.tool.Preference prefTheme = new com.gsp.gyenamhighschool.tool.Preference(getActivity(), "theme");
                        int color = prefTheme.getInt("theme",0);
                        if (color == Color.parseColor("#F44336") || color == Color.parseColor("#E91E63")
                                || color == Color.parseColor("#FFC107") || color == Color.parseColor("#8BC34A")
                                        || color == Color.parseColor("#03A9F4") || color == Color.parseColor("#3F51B5")) {

                            prefTheme.remove("theme");

                            new MaterialDialog.Builder(getActivity())
                                    .title(R.string.off_title)
                                    .content(R.string.off_text)
                                    .cancelable(false)
                                    .positiveText("적용")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            // TODO
                                            System.exit(0);
                                            startActivity(new Intent(getActivity(), MainActivity.class));

                                        }
                                    })
                                    .show();
                        } else {
                            prefTheme.remove("theme");
                        }
                    } else {
                        com.gsp.gyenamhighschool.tool.Preference mPref = new com.gsp.gyenamhighschool.tool.Preference(getActivity());

                        if (mPref.getBoolean("firstOfTheme", true) && preference.getKey().equals("theme")){
                            mPref.putBoolean("firstOfTheme", false); // false 값을 put 함으로써 true에 잡하지 않는다.

                            new MaterialDialog.Builder(getActivity())
                                    .title("테마 사용 안내")
                                    .content("테마 사용을 하실 경우 색상을 선택하실 수 있습니다.\n설정 하지 않을 경우 기본색인 하늘색이 적용 됩니다.\n현재는 6색상만 적용가능하며 추후 업데이트를 통하여 추가하도록 하겠습니다.")
                                    .positiveText("닫기")
                                    //.titleColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary))
                                    .show();

                        }
                    }

                    return true;
                }
            });


            boolean isAdmin_stu = getPreferenceManager().getSharedPreferences().getBoolean("userAdmin_stu", false);
            boolean isAdmin_broad = getPreferenceManager().getSharedPreferences().getBoolean("userAdmin_broad", false);
            boolean isAdmin_student = getPreferenceManager().getSharedPreferences().getBoolean("userAdmin_student", false);
            boolean isAdmin_developer = getPreferenceManager().getSharedPreferences().getBoolean("userAdmin_developer", false);
            Preference proUpgrade = findPreference("proUpgrade");
            Preference initialization = findPreference("initialization");
            if (isAdmin_stu) {
                proUpgrade.setSummary(getString(R.string.user_info_student));
                proUpgrade.setEnabled(false);
                initialization.setEnabled(true);
            }
            if (isAdmin_broad) {
                proUpgrade.setSummary(getString(R.string.user_info_licensed_broadcast));
                proUpgrade.setEnabled(false);
                initialization.setEnabled(true);
            }
            if (isAdmin_student) {
                proUpgrade.setSummary(getString(R.string.user_info_licensed_student));
                proUpgrade.setEnabled(false);
                initialization.setEnabled(true);
            }
            if (isAdmin_developer) {
                proUpgrade.setSummary(getString(R.string.user_info_licensed_developer));
                proUpgrade.setEnabled(false);
                initialization.setEnabled(true);
            }


            findPreference("myDeviceId")
                    .setSummary(Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID));

            setOnPreferenceClick(findPreference("infoAutoUpdate"));
            setOnPreferenceClick(findPreference("openSource"));
            setOnPreferenceClick(findPreference("ChangeLog"));
            setOnPreferenceClick(findPreference("chooseColor"));
            setOnPreferenceClick(findPreference("proUpgrade"));
            setOnPreferenceClick(findPreference("introduce"));
            setOnPreferenceClick(findPreference("initialization"));
            setOnPreferenceChange(findPreference("autoBapUpdate"));
            setOnPreferenceChange(findPreference("updateLife"));
     //       setOnPreferenceChange(findPreference("theme"));


            try {
                PackageManager packageManager = getActivity().getPackageManager();
                PackageInfo info = packageManager.getPackageInfo(getActivity().getPackageName(), PackageManager.GET_META_DATA);
                findPreference("appVersion").setSummary(info.versionName);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }

        private void setOnPreferenceClick(Preference mPreference) {
            mPreference.setOnPreferenceClickListener(onPreferenceClickListener);
        }

        private void showColorChooserPrimary() {
            int[] primary = new int[]{
                    Color.parseColor("#F44336"),
                    Color.parseColor("#E91E63"),
                    Color.parseColor("#FFC107"),
                    Color.parseColor("#8BC34A"),
                    Color.parseColor("#03A9F4"),
                    Color.parseColor("#3F51B5")
            };
            new ColorChooserDialog.Builder((SettingActivity) getActivity(), R.string.color_palette)
              //      .titleSub(R.string.color_palette)
                    .customColors(primary, null)
                    .doneButton(R.string.done)
                    .cancelButton(R.string.cancel)
                    .allowUserColorInput(false)
                    .show();
        }



        private Preference.OnPreferenceClickListener onPreferenceClickListener = new Preference.OnPreferenceClickListener() {





            @Override
            public boolean onPreferenceClick(Preference preference) {
                String getKey = preference.getKey();


                if ("chooseColor".equals(getKey)) {
                    showColorChooserPrimary();
                }
                if ("introduce".equals(getKey)) {
                    new MaterialDialog.Builder(getActivity())
                            .title("테마 사용 안내")
                            .content("테마 사용을 하실 경우 색상을 선택하실 수 있습니다.\n설정 하지 않을 경우 기본색인 하늘색이 적용 됩니다.\n현재는 6색상만 적용가능하며 추후 업데이트를 통하여 추가하도록 하겠습니다.")
                            .positiveText("닫기")
   //                         .titleColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary))
                            .show();
                }


                if ("openSource".equals(getKey)) {


                    if (isInternetCon()) { //false 반환시 if 문안의 로직 실행
                        Toast.makeText(getActivity(), "Wifi나 데이터를 켜주세요 :)", Toast.LENGTH_SHORT).show();
                    } else {

                        MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                                .title("오픈소스 라이선스")
                                .customView(R.layout.open_source_license, true)
                                .positiveText("닫기")
                                .build();
                        WebView webView = (WebView) dialog.getCustomView().findViewById(R.id.viewpager_indicator);
                        webView.loadUrl("file:///android_asset/viewpager_indicator.html");
                        WebView webView2 = (WebView) dialog.getCustomView().findViewById(R.id.photoview);
                        webView2.loadUrl("https://raw.githubusercontent.com/chrisbanes/PhotoView/master/LICENSE");
                        WebView webView3 = (WebView) dialog.getCustomView().findViewById(R.id.material_dialog_webview);
                        webView3.loadUrl("https://raw.githubusercontent.com/afollestad/material-dialogs/master/LICENSE.txt");
                        WebView webView4 = (WebView) dialog.getCustomView().findViewById(R.id.app_intro);
                        webView4.loadUrl("https://raw.githubusercontent.com/paolorotolo/AppIntro/master/LICENSE");
                        WebView webView5 = (WebView) dialog.getCustomView().findViewById(R.id.glide);
                        webView5.loadUrl("https://raw.githubusercontent.com/bumptech/glide/master/LICENSE");
                        WebView webView6 = (WebView) dialog.getCustomView().findViewById(R.id.date_time_picker);
                        webView6.loadUrl("file:///android_asset/date_time_picker.html");
                        WebView webView7 = (WebView) dialog.getCustomView().findViewById(R.id.auto_scroll_viewpager);
                        webView7.loadUrl("https://raw.githubusercontent.com/Trinea/android-auto-scroll-view-pager/master/LICENSE");
                        WebView webView8 = (WebView) dialog.getCustomView().findViewById(R.id.tedpermission);
                        webView8.loadUrl("file:///android_asset/tedpermission.html");
                        dialog.show();
                        //.titleColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary))
                    }
                } else if ("ChangeLog".equals(getKey)) {
                    new MaterialDialog.Builder(getActivity())
                            .title(R.string.changeLog_title)
                            .content(getString(R.string.changeLog_msg))
                            .positiveText("닫기")
                        //.titleColor(ContextCompat.getColor(getActivity(),R.color.colorPrimary))
                            .show();
                } else if ("infoAutoUpdate".equals(getKey)) {
                    showNotification();
                } else if ("proUpgrade".equals(getKey)) {

                    if(isInternetCon()) {
                        Toast.makeText(getActivity(), "Wifi나 데이터를 켜주세요 :)", Toast.LENGTH_SHORT).show();
                    } else {
                        builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
                        builder.setTitle(R.string.user_info_class_up_title);
                        // builder.setMessage(R.string.no_network_msg);
                        // Set an EditText view to get user input
                        final EditText input = new EditText(getActivity());
                        builder.setView(input);

                        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String value = input.getText().toString();
                                SecurityXor securityXor = new SecurityXor();

                                /**
                                 * userAdmin_stu : 계남인
                                 * userAdmin_broad : 방송부 게시글 관리자
                                 * userAdmin_student : 학생회 게시글 관리자
                                 * userAdmin_developer : 개발자
                                 */

                                if (student.equals(securityXor.getSecurityXor(value, 1383))) {
                                    getPreferenceManager().getSharedPreferences().edit().putBoolean("userAdmin_stu", true).apply();
                                    // getPreferenceManager().getSharedPreferences().edit().putString("whatAdmin","student").apply();
                                    Toast.makeText(getActivity(), "계남인 등급이 되셨습니다.", Toast.LENGTH_SHORT).show();
                                    getActivity().finish();
                                }
                                if (broadcast.equals(securityXor.getSecurityXor(value, 3768))) {
                                    getPreferenceManager().getSharedPreferences().edit().putBoolean("userAdmin_broad", true).apply();
                                    // getPreferenceManager().getSharedPreferences().edit().putString("whatAdmin", "broadcastMaster").apply();
                                    Toast.makeText(getActivity(), "축하드립니다. 방송부 게시글 관리자 등급이 되셨습니다.", Toast.LENGTH_SHORT).show();
                                    getActivity().finish();
                                }
                                if (school_student.equals(securityXor.getSecurityXor(value, 1294))) {
                                    getPreferenceManager().getSharedPreferences().edit().putBoolean("userAdmin_student", true).apply();
                                    // getPreferenceManager().getSharedPreferences().edit().putString("whatAdmin", "studentAssociationMaster").apply();
                                    Toast.makeText(getActivity(), "축하드립니다. 학생회 게시글 관리자 등급이 되셨습니다.", Toast.LENGTH_SHORT).show();
                                    getActivity().finish();
                                }
                                if (developer.equals(securityXor.getSecurityXor(value, 8771))) {
                                    getPreferenceManager().getSharedPreferences().edit().putBoolean("userAdmin_developer", true).apply();
                                    // getPreferenceManager().getSharedPreferences().edit().putString("whatAdmin", "developer").apply();
                                    Toast.makeText(getActivity(), "축하드립니다. 개발자 등급이 되셨습니다.", Toast.LENGTH_SHORT).show();
                                    getActivity().finish();
                                }

                            }
                        });
                        builder.setNegativeButton(android.R.string.cancel, null);
                        try {
                            process(); //네트워크 관련은 따로 쓰레드를 생성해야 UI 쓰레드와 겹치지 않는다. 그러므로 Thread 가 선언된 process 메서드를 호출한다.
                        } catch (Exception e) {
                            Log.d("ERROR", e + "");
                        }
                    }
                } else if ("initialization".equals(getKey)){

                    new MaterialDialog.Builder(getActivity())
                            .title("등급 초기화")
                            .content("사용자의 등급을 초기화 합니다.\n초기화 하더라도 다시 암호를 입력하여 등급을 설정할 수 있습니다.")
                            .positiveText("초기화")
                            .negativeText("닫기")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // TODO

                                }
                            })
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // TODO

                                    boolean isAdmin_stu = getPreferenceManager().getSharedPreferences().getBoolean("userAdmin_stu", false);
                                    boolean isAdmin_broad = getPreferenceManager().getSharedPreferences().getBoolean("userAdmin_broad", false);
                                    boolean isAdmin_student = getPreferenceManager().getSharedPreferences().getBoolean("userAdmin_student", false);
                                    boolean isAdmin_developer = getPreferenceManager().getSharedPreferences().getBoolean("userAdmin_developer", false);

                                    if (isAdmin_stu) {
                                        getPreferenceManager().getSharedPreferences().edit().remove("userAdmin_stu").apply();
                                        getActivity().finish();
                                        Toast.makeText(getActivity(), "등급이 초기화 되었습니다", Toast.LENGTH_SHORT).show();
                                    }
                                    if (isAdmin_broad) {
                                        getPreferenceManager().getSharedPreferences().edit().remove("userAdmin_broad").apply();
                                        getActivity().finish();
                                        Toast.makeText(getActivity(), "등급이 초기화 되었습니다", Toast.LENGTH_SHORT).show();
                                    }
                                    if (isAdmin_student) {
                                        getPreferenceManager().getSharedPreferences().edit().remove("userAdmin_student").apply();
                                        getActivity().finish();
                                        Toast.makeText(getActivity(), "등급이 초기화 되었습니다", Toast.LENGTH_SHORT).show();
                                    }
                                    if (isAdmin_developer) {
                                        getPreferenceManager().getSharedPreferences().edit().remove("userAdmin_developer").apply();
                                        getActivity().finish();
                                        Toast.makeText(getActivity(), "등급이 초기화 되었습니다", Toast.LENGTH_SHORT).show();
                                    }

                                }
                            })
                            .show();
                }

                return true;
            }
        };

        private void setOnPreferenceChange(Preference mPreference) {
            mPreference.setOnPreferenceChangeListener(onPreferenceChangeListener);

            if (mPreference instanceof ListPreference) {
                    ListPreference listPreference = (ListPreference) mPreference;
                    int index = listPreference.findIndexOfValue(listPreference.getValue());
                    mPreference.setSummary(index >= 0 ? listPreference.getEntries()[index] : null);
                } else if (mPreference instanceof EditTextPreference) {
                    String values = ((EditTextPreference) mPreference).getText();
                    if (values == null) values = "";
                onPreferenceChangeListener.onPreferenceChange(mPreference, values);
            }
        }



        private Preference.OnPreferenceChangeListener onPreferenceChangeListener = new Preference.OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String stringValue = newValue.toString();



                if (preference instanceof EditTextPreference) {
                    preference.setSummary(stringValue);

                } else if (preference instanceof ListPreference) {

                    /**
                     * ListPreference의 경우 stringValue가 entryValues이기 때문에 바로 Summary를
                     * 적용하지 못한다 따라서 설정한 entries에서 String을 로딩하여 적용한다
                     */
                    ListPreference listPreference = (ListPreference) preference;
                    int index = listPreference.findIndexOfValue(stringValue);

                    preference.setSummary(index >= 0 ? listPreference.getEntries()[index] : null);

                    updateAlarm updateAlarm = new updateAlarm(getActivity());
                    updateAlarm.cancel();

                    if (index == 0) updateAlarm.autoUpdate();
                    else if (index == 1) updateAlarm.SaturdayUpdate();
                    else if (index == 2) updateAlarm.SundayUpdate();

                } else if (preference instanceof CheckBoxPreference) {
                    com.gsp.gyenamhighschool.tool.Preference mPref = new com.gsp.gyenamhighschool.tool.Preference(getActivity());

                    if (mPref.getBoolean("firstOfAutoUpdate", true) && preference.getKey().equals("autoBapUpdate")){
                        mPref.putBoolean("firstOfAutoUpdate", false); // false 값을 put 함으로써 true에 잡하지 않는다.
                        showNotification();
                    }


                    if (!mPref.getBoolean("autoBapUpdate", false) && preference.isEnabled()) {
                        int updateLife = Integer.parseInt(mPref.getString("updateLife", "0"));

                        updateAlarm updateAlarm = new updateAlarm(getActivity());
                        if (updateLife == 1) updateAlarm.autoUpdate();
                        else if (updateLife == 0) updateAlarm.SaturdayUpdate();
                        else if (updateLife == -1) updateAlarm.SundayUpdate();

                    } else {
                        updateAlarm updateAlarm = new updateAlarm(getActivity());
                        updateAlarm.cancel();
                    }
                }
                return true;
            }
        };

        private void showNotification() {
            new MaterialDialog.Builder(getActivity())
                    .title(R.string.info_autoUpdate_title)
                    .content(getString(R.string.info_autoUpdate_msg))
                    .positiveText("닫기")
                    .show();
        }

    }




    @Override
    public void onColorSelection(@NonNull ColorChooserDialog dialog, @ColorInt final int color) {
        // TODO

        new MaterialDialog.Builder(this)
                .title(R.string.theme_title)
                .content(R.string.theme_text)
                .positiveText("적용")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        // TODO

                        prefTheme = new com.gsp.gyenamhighschool.tool.Preference(getApplicationContext(),"theme");
                        accentPreselect = color;
                        prefTheme.putInt("theme",color);

                        System.exit(0);
                        startActivity(new Intent(SettingActivity.this, MainActivity.class));

                    }
                })
                .negativeText("닫기")
                .show();
    }

}

