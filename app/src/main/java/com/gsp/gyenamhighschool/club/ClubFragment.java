package com.gsp.gyenamhighschool.club;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class ClubFragment extends Fragment {

    public static Fragment getInstance(int club) {
        ClubFragment mFragment = new ClubFragment();

        Bundle args = new Bundle();
        args.putInt("club", club);
        mFragment.setArguments(args);

        return mFragment;
    }

}
