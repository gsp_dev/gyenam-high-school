package com.gsp.gyenamhighschool;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gsp.gyenamhighschool.AfterSchoolProgram.AfterSchoolProgramActivity;
import com.gsp.gyenamhighschool.Bap.BapActivity;
import com.gsp.gyenamhighschool.BroadCast.BroadCastActivity;
import com.gsp.gyenamhighschool.TimeTable.TimeTableActivity;
import com.gsp.gyenamhighschool.tool.BapTool;
import com.gsp.gyenamhighschool.tool.Preference;
import com.gsp.gyenamhighschool.tool.TimeTableTool;

import java.io.IOException;

import static com.gsp.gyenamhighschool.tool.Tools.isOnline_Fixed;

@SuppressWarnings("ValidFragment")
public class fragment1 extends Fragment {

    private Context mContext;
    private View view;
    private ViewGroup rowContainer;

    private ConnectivityManager cManager;
    private NetworkInfo mobile;
    private NetworkInfo wifi;
    boolean isAdmin_stu, isAdmin_broad, isAdmin_student, isAdmin_developer;

    com.gsp.gyenamhighschool.tool.Preference prefTheme;
    int color;


    public static Fragment getInstance(int index) {
        fragment1 pane = new fragment1();

        Bundle args = new Bundle();
        args.putInt("code", index);
        pane.setArguments(args);

        return pane;
    }


    public void fillRow(View view, final String title, final String description) {


        TextView textView1 = (TextView) view.findViewById(R.id.item_title);
        TextView textView2 = (TextView) view.findViewById(R.id.sub_title);
        textView1.setText(title);
        textView2.setText(description);

    }


    public void fillText(View view, String text, String sub_text, String kcal) {

        TextView txt = (TextView) view.findViewById(R.id.mSimpleTitle);
        txt.setText(text);
        TextView txt2 = (TextView) view.findViewById(R.id.mSimpleText);
        txt2.setText(sub_text);
        TextView txt3 = (TextView) view.findViewById(R.id.mKcalText);
        txt3.setText(kcal);

    }

    public void fillTextTimeTable(View view, String text, String sub_text) {

        TextView txt = (TextView) view.findViewById(R.id.mSimpleTitle_Time);
        txt.setText(text);
        TextView txt2 = (TextView) view.findViewById(R.id.mSimpleText_Time);
        txt2.setText(sub_text);

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        prefTheme = new com.gsp.gyenamhighschool.tool.Preference(getActivity(),"theme");

        color = prefTheme.getInt("theme",0);



        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment1, container, false);


        rowContainer = (LinearLayout) rootView.findViewById(R.id.rowContainer);

        Preference mPref = new Preference(getActivity());

        isAdmin_stu = new Preference(getActivity()).getBoolean("userAdmin_stu", false);
        isAdmin_broad = new Preference(getActivity()).getBoolean("userAdmin_broad", false);
        isAdmin_student = new Preference(getActivity()).getBoolean("userAdmin_student", false);
        isAdmin_developer = new Preference(getActivity()).getBoolean("userAdmin_developer", false);

        view = rowContainer.findViewById(R.id.card1);
        fillRow(view, "급식", "급식을 확인합니다.");

        if (color == Color.parseColor("#F44336")) {
            /*빨간색 테마*/
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_red);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.meal_icon_red);

        } else if (color == Color.parseColor("#E91E63")){
            /*분홍색 테마*/
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_pink);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.meal_icon_pink);

        } else if (color == Color.parseColor("#FFC107")){
            /*연한 주황색(노란색) 테마*/
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_yellow);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.meal_icon_yellow);

        } else if (color == Color.parseColor("#8BC34A")){
            /*연두색 테마*/
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_green);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.meal_icon_green);

        } else if (color == Color.parseColor("#03A9F4")){
            /*하늘색(기본색) 테마*/
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.meal_icon);

        } else if (color == Color.parseColor("#3F51B5")){
            /*남색 테마*/
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_indigo);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.meal_icon_indigo);

        } else {
            /*기본색(하늘색) 테마*/
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.meal_icon);
        }



            if (mPref.getBoolean("simpleShowBap", false)) {
                BapTool.todayBapData mBapData = BapTool.getTodayBap(getActivity());
                LinearLayout mSimpleLayout = (LinearLayout) rowContainer.findViewById(R.id.mSimpleLayout);
                fillText(rowContainer, mBapData.title, mBapData.info, mBapData.kcal);
                mSimpleLayout.setVisibility(View.VISIBLE);
            } else {
                LinearLayout mSimpleLayout = (LinearLayout) rowContainer.findViewById(R.id.mSimpleLayout);
                mSimpleLayout.setVisibility(View.GONE);
            }


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), BapActivity.class));
            }
        });


        view = rowContainer.findViewById(R.id.card2);
        fillRow(view, "시간표", "시간표를 확인합니다.");

        if (color == Color.parseColor("#F44336")) {
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout); // 그대로
            linearLayout.setBackgroundResource(R.drawable.selector_red);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon); // 그대로
            imageView.setImageResource(R.drawable.timetable_icon_red);
        } else if (color == Color.parseColor("#E91E63")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout); // 그대로
            linearLayout.setBackgroundResource(R.drawable.selector_pink);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon); // 그대로
            imageView.setImageResource(R.drawable.timetable_icon_pink);
        } else if (color == Color.parseColor("#FFC107")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout); // 그대로
            linearLayout.setBackgroundResource(R.drawable.selector_yellow);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon); // 그대로
            imageView.setImageResource(R.drawable.timetable_icon_yellow);
        } else if (color == Color.parseColor("#8BC34A")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout); // 그대로
            linearLayout.setBackgroundResource(R.drawable.selector_green);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon); // 그대로
            imageView.setImageResource(R.drawable.timetable_icon_green);
        } else if (color == Color.parseColor("#03A9F4")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout); // 그대로
            linearLayout.setBackgroundResource(R.drawable.selector);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon); // 그대로
            imageView.setImageResource(R.drawable.timetable_icon);
        } else if (color == Color.parseColor("#3F51B5")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout); // 그대로
            linearLayout.setBackgroundResource(R.drawable.selector_indigo);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon); // 그대로
            imageView.setImageResource(R.drawable.timetable_icon_indigo);
        } else {
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout); // 그대로
            linearLayout.setBackgroundResource(R.drawable.selector);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon); // 그대로
            imageView.setImageResource(R.drawable.timetable_icon);

        } if (mPref.getBoolean("simpleShowTimeTable", false)) {
            TimeTableTool.todayTimeTableData timeTableData = TimeTableTool.getTodayTimeTable(getActivity());
            LinearLayout mSimpleLayout = (LinearLayout) rowContainer.findViewById(R.id.mSimpleLayout_Time);
            fillTextTimeTable(rowContainer, timeTableData.title, timeTableData.info);
            mSimpleLayout.setVisibility(View.VISIBLE);
        } else {
            LinearLayout mSimpleLayout = (LinearLayout) rowContainer.findViewById(R.id.mSimpleLayout_Time);
            mSimpleLayout.setVisibility(View.GONE);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 startActivity(new Intent(getActivity(), TimeTableActivity.class));
            }
        });


        view = rowContainer.findViewById(R.id.card3);
        fillRow(view, "방송부", "신청곡 및 사연을 확인, 제보 합니다.");

        if (color == Color.parseColor("#F44336")) {
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_red);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.broadcast_icon_red);

        } else if (color == Color.parseColor("#E91E63")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_pink);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.broadcast_icon_pink);

        } else if (color == Color.parseColor("#FFC107")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_yellow);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.broadcast_icon_yellow);

        } else if (color == Color.parseColor("#8BC34A")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_green);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.broadcast_icon_green);

        } else if (color == Color.parseColor("#03A9F4")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.broadcast_icon);

        } else if (color == Color.parseColor("#3F51B5")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_indigo);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.broadcast_icon_indigo);

        } else {
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.broadcast_icon);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // startActivity(new Intent(getActivity(), BroadCastActivity.class));
                if(isInternetCon()) {
                    Snackbar.make(v, "Wifi나 데이터를 켜주세요 :)", Snackbar.LENGTH_SHORT).show();
                } else {
                    if (isOnline_Fixed()) {
                        startActivity(new Intent(getActivity(), BroadCastActivity.class));
                    } else {
                        Snackbar.make(v, "네트워크가 올바르지 않아요 :(", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
        });

        view = rowContainer.findViewById(R.id.card4);
        fillRow(view, "방과 후 학교", "방과 후 학교를 신청합니다.");

        if (color == Color.parseColor("#F44336")) {
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_red);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.afterschool_program_icon_red);
        } else if (color == Color.parseColor("#E91E63")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_pink);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.afterschool_program_icon_pink);
        } else if (color == Color.parseColor("#FFC107")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_yellow);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.afterschool_program_icon_yellow);
        } else if (color == Color.parseColor("#8BC34A")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_green);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.afterschool_program_icon_green);
        } else if (color == Color.parseColor("#03A9F4")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.afterschool_program_icon);
        } else if (color == Color.parseColor("#3F51B5")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_indigo);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.afterschool_program_icon_indigo);
        } else {
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.afterschool_program_icon);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isInternetCon()) {
                    Snackbar.make(v, "Wifi나 데이터를 켜주세요 :)", Snackbar.LENGTH_SHORT).show();
                } else {

                    if (isAdmin_stu || isAdmin_broad || isAdmin_student || isAdmin_developer) { // 권한 체크

                        if (isOnline_Fixed()) { // 인터넷 연결여부와 권한 체크가 완료되었으므로 네트워크가 올바른지 체크한다
                            startActivity(new Intent(getActivity(), AfterSchoolProgramActivity.class));
                        } else {
                            Snackbar.make(v, "네트워크가 올바르지 않아요 :(", Snackbar.LENGTH_SHORT).show();
                        }

                    } else { // 권한이 없으면
                        Snackbar.make(v,"계남인 이상의 권한이 필요해요 :)",Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
        });

        return rootView;

    }

    public void onStart() {
        super.onStart();

        rowContainer = (LinearLayout) getActivity().findViewById(R.id.rowContainer);

        Bundle args = getArguments();
        int code = args.getInt("code");
        Preference mPref = new Preference(getActivity());

        isAdmin_stu = new Preference(getActivity()).getBoolean("userAdmin_stu", false);
        isAdmin_broad = new Preference(getActivity()).getBoolean("userAdmin_broad", false);
        isAdmin_student = new Preference(getActivity()).getBoolean("userAdmin_student", false);
        isAdmin_developer = new Preference(getActivity()).getBoolean("userAdmin_developer", false);


        view = rowContainer.findViewById(R.id.card1);
        fillRow(view, "급식", "급식을 확인합니다.");

            if (mPref.getBoolean("simpleShowBap", false)) {
                BapTool.todayBapData mBapData = BapTool.getTodayBap(getActivity());
                LinearLayout mSimpleLayout = (LinearLayout) rowContainer.findViewById(R.id.mSimpleLayout);
                fillText(rowContainer, mBapData.title, mBapData.info, mBapData.kcal);
                mSimpleLayout.setVisibility(View.VISIBLE);
            } else {
                LinearLayout mSimpleLayout = (LinearLayout) rowContainer.findViewById(R.id.mSimpleLayout);
                mSimpleLayout.setVisibility(View.GONE);
            }
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), BapActivity.class));
            }
        });



        view = rowContainer.findViewById(R.id.card2);
        fillRow(view, "시간표", "시간표를 확인합니다.");
        if (mPref.getBoolean("simpleShowTimeTable", false)) {
            TimeTableTool.todayTimeTableData timeTableData = TimeTableTool.getTodayTimeTable(getActivity());
            LinearLayout mSimpleLayout = (LinearLayout) rowContainer.findViewById(R.id.mSimpleLayout_Time);
            fillTextTimeTable(rowContainer, timeTableData.title, timeTableData.info);
            mSimpleLayout.setVisibility(View.VISIBLE);
        } else {
            LinearLayout mSimpleLayout = (LinearLayout) rowContainer.findViewById(R.id.mSimpleLayout_Time);
            mSimpleLayout.setVisibility(View.GONE);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 startActivity(new Intent(getActivity(), TimeTableActivity.class));
            }
        });




        view = rowContainer.findViewById(R.id.card3);
        fillRow(view, "방송부", "신청곡 및 사연을 확인, 제보 합니다.");
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isInternetCon()) {
                    Snackbar.make(v, "Wifi나 데이터를 켜주세요 :)", Snackbar.LENGTH_SHORT).show();
                } else {
                    if (isOnline_Fixed()) {
                        startActivity(new Intent(getActivity(), BroadCastActivity.class));
                    } else {
                        Snackbar.make(v, "네트워크가 올바르지 않아요 :(", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
        });



        view = rowContainer.findViewById(R.id.card4);
        fillRow(view, "방과 후 학교", "방과 후 학교를 신청합니다.");
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isInternetCon()) {
                    Snackbar.make(v, "Wifi나 데이터를 켜주세요 :)", Snackbar.LENGTH_SHORT).show();
                } else {
                    if (isAdmin_stu || isAdmin_broad || isAdmin_student || isAdmin_developer) {

                        if (isOnline_Fixed()) {
                            startActivity(new Intent(getActivity(), AfterSchoolProgramActivity.class));
                        } else {
                            Snackbar.make(v, "네트워크가 올바르지 않아요 :(", Snackbar.LENGTH_SHORT).show();
                        }

                    } else {
                        Snackbar.make(v,"계남인 이상의 권한이 필요해요\n설정에서 사용자 등급을 설정해주세요 :)",Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
        });
        }

    private boolean isInternetCon() {
        cManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        mobile = cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE); //모바일 데이터 여부
        wifi = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI); //와이파이 여부
        return !mobile.isConnected() && !wifi.isConnected(); //결과값을 리턴
    }
    }