package com.gsp.gyenamhighschool;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gsp.gyenamhighschool.Schedule.ScheduleActivity;
import com.gsp.gyenamhighschool.notice.NoticeActivity;
import com.gsp.gyenamhighschool.school.School_Main;

import java.io.IOException;

import static com.gsp.gyenamhighschool.tool.Tools.isOnline_Fixed;

@SuppressWarnings("ValidFragment")
public class fragment2 extends Fragment {

/**fragment1과 원리가 같습니다.*/

    private View view;
    private ViewGroup rowContainer;
    com.gsp.gyenamhighschool.tool.Preference prefTheme;
    int color;

    private ConnectivityManager cManager;
    private NetworkInfo mobile;
    private NetworkInfo wifi;


    public void fillRow(View view, final String title, final String description) {

        TextView textView1 = (TextView) view.findViewById(R.id.item_title);
        TextView textView2 = (TextView) view.findViewById(R.id.sub_title);
        textView1.setText(title);
        textView2.setText(description);

    }

    public static Fragment getInstance(int index) {
        fragment2 pane = new fragment2();

        Bundle args = new Bundle();
        args.putInt("code", index);
        pane.setArguments(args);

        return pane;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        prefTheme = new com.gsp.gyenamhighschool.tool.Preference(getActivity(),"theme");

        color = prefTheme.getInt("theme",0);

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment2, container, false);

        rowContainer = (LinearLayout) rootView.findViewById(R.id.rowContainer2);


        view = rowContainer.findViewById(R.id.card2_1);
        fillRow(view, "공지사항", "여러 공지사항을 확인합니다.");

        if (color == Color.parseColor("#F44336")) {
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_red);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.notice_icon_red);
        } else if (color == Color.parseColor("#E91E63")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_pink);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.notice_icon_pink);
        } else if (color == Color.parseColor("#FFC107")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_yellow);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.notice_icon_yellow);
        } else if (color == Color.parseColor("#8BC34A")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_green);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.notice_icon_green);
        } else if (color == Color.parseColor("#03A9F4")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.notice_icon);
        } else if (color == Color.parseColor("#3F51B5")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_indigo);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.notice_icon_indigo);
        } else {
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.notice_icon);
        }
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isInternetCon()) {
                    Snackbar.make(v, "Wifi나 데이터를 켜주세요 :)", Snackbar.LENGTH_SHORT).show();
                } else {
                    if (isOnline_Fixed()) {
                        startActivity(new Intent(getActivity(), NoticeActivity.class));
                    } else {
                        Snackbar.make(v, "네트워크가 올바르지 않아요 :(", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
        });





        view = rowContainer.findViewById(R.id.card2_2);
        fillRow(view, "학사 일정", "학사 일정을 확인합니다.");


        if (color == Color.parseColor("#F44336")) {
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_red);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.schedule_icon_red);
        } else if (color == Color.parseColor("#E91E63")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_pink);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.schedule_icon_pink);
        } else if (color == Color.parseColor("#FFC107")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_yellow);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.schedule_icon_yellow);
        } else if (color == Color.parseColor("#8BC34A")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_green);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.schedule_icon_green);
        } else if (color == Color.parseColor("#03A9F4")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.schedule_icon);
        } else if (color == Color.parseColor("#3F51B5")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_indigo);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.schedule_icon_indigo);
        } else {
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.schedule_icon);
        }
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ScheduleActivity.class));
            }
        });





        view = rowContainer.findViewById(R.id.card2_3);
        fillRow(view, "동아리", "동아리에 대한 정보를 확인합니다.");

        if (color == Color.parseColor("#F44336")) {
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_red);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.club_icon_red);
        } else if (color == Color.parseColor("#E91E63")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_pink);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.club_icon_pink);
        } else if (color == Color.parseColor("#FFC107")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_yellow);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.club_icon_yellow);
        } else if (color == Color.parseColor("#8BC34A")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_green);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.club_icon_green);
        } else if (color == Color.parseColor("#03A9F4")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.club_icon);
        } else if (color == Color.parseColor("#3F51B5")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_indigo);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.club_icon_indigo);
        } else {
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector);
            ImageView imageView = (ImageView)view.findViewById(R.id.item_icon);
            imageView.setImageResource(R.drawable.club_icon);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "준비중인 기능입니다 :)", Snackbar.LENGTH_SHORT).show();
            }
        });






        view = rowContainer.findViewById(R.id.card2_4);
        fillRow(view, "학교 정보", "계남고의 정보를 확인합니다.");

        if (color == Color.parseColor("#F44336")) {
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_red);
        } else if (color == Color.parseColor("#E91E63")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_pink);
        } else if (color == Color.parseColor("#FFC107")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_yellow);
        } else if (color == Color.parseColor("#8BC34A")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_green);
        } else if (color == Color.parseColor("#03A9F4")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector);
        } else if (color == Color.parseColor("#3F51B5")){
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector_indigo);
        } else {
            LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.selector_linearlayout);
            linearLayout.setBackgroundResource(R.drawable.selector);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isInternetCon()) {
                    Snackbar.make(v, "Wifi나 데이터를 켜주세요 :)", Snackbar.LENGTH_SHORT).show();
                } else {

                    if (isOnline_Fixed()) {
                        startActivity(new Intent(getActivity(), School_Main.class));
                    } else {
                        Snackbar.make(v, "네트워크가 올바르지 않아요 :(", Snackbar.LENGTH_SHORT).show();
                    }

                }
            }
        });
        ((ImageView) view.findViewById(R.id.item_icon)).setImageResource(R.drawable.gyenam_icon_clean);

        return rootView;

    }

    private boolean isInternetCon() {
        cManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        mobile = cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE); //모바일 데이터 여부
        wifi = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI); //와이파이 여부
        return !mobile.isConnected() && !wifi.isConnected(); //결과값을 리턴
    }
}