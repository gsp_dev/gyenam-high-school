package com.gsp.gyenamhighschool.notice;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.gsp.gyenamhighschool.R;
import com.gsp.gyenamhighschool.tool.Preference;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.util.Vector;

public class DeveloperSendActivity extends AppCompatActivity {

    com.gsp.gyenamhighschool.tool.Preference prefTheme;
    int color;
    EditText mTitle, mMessage;
    ProgressDialog mDialog;
    boolean isAdmin_developer;
    private ConnectivityManager cManager;
    private NetworkInfo mobile;
    private NetworkInfo wifi;

    /**원리는 BroadCastNoticeSendActivity.java와 같습니다.*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefTheme = new com.gsp.gyenamhighschool.tool.Preference(getApplicationContext(), "theme");

        color = prefTheme.getInt("theme", 0);


        if (color == Color.parseColor("#F44336"))
            setTheme(R.style.AppTheme_Red);
        if (color == Color.parseColor("#E91E63"))
            setTheme(R.style.AppTheme_Pink);
        if (color == Color.parseColor("#FFC107"))
            setTheme(R.style.AppTheme_Yellow);
        if (color == Color.parseColor("#8BC34A"))
            setTheme(R.style.AppTheme_Green);
        if (color == Color.parseColor("#03A9F4"))
            setTheme(R.style.AppTheme);
        if (color == Color.parseColor("#3F51B5"))
            setTheme(R.style.AppTheme_Indigo);

        setContentView(R.layout.developer_notice_send);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.mToolbar);
        setSupportActionBar(mToolbar);

        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setHomeButtonEnabled(true);
            mActionBar.setDisplayHomeAsUpEnabled(true);

            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        boolean isAdmin = getIntent().getBooleanExtra("userAdmin_1", false);
        isAdmin_developer = new Preference(getApplicationContext()).getBoolean("userAdmin_developer",false);
        if (!isAdmin) { //isAdmin이 False라면
            finish();
        } else { //isAdmin이 True면
            if (!isAdmin_developer) { // 개발자 등급이 아니라면
                finish();
            } else {
                LinearLayout linearLayout = (LinearLayout) findViewById(R.id.broadcast_send_need_permission_layout);
                linearLayout.setVisibility(View.GONE);

                LinearLayout linearLayout2 = (LinearLayout) findViewById(R.id.broadcast_send_layout);
                linearLayout2.setVisibility(View.VISIBLE);

                mTitle = (EditText) findViewById(R.id.mTitle);
                mMessage = (EditText) findViewById(R.id.mMessage);

                Button btn = (Button) findViewById(R.id.send);

                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (mTitle.length() == 0 || mMessage.length() == 0) {
                            Toast.makeText(getApplicationContext(), "내용을 입력해주세요 :)", Toast.LENGTH_SHORT).show();
                        } else {

                            if (isInternetCon()) {
                                Toast.makeText(DeveloperSendActivity.this, "Wifi나 데이터를 켜주세요 :)", Toast.LENGTH_SHORT).show();
                            } else {

                                MaterialDialog.Builder builder = new MaterialDialog.Builder(DeveloperSendActivity.this);
                                builder.title("공지사항 작성");
                                builder.content(getString(R.string.post_notice_alert));
                                builder.negativeText(android.R.string.cancel);
                                builder.positiveText(android.R.string.ok);
                                builder.onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        String title = mTitle.getText().toString().trim();
                                        String message = mMessage.getText().toString().trim();

                                        if (!title.isEmpty() && !message.isEmpty()) {
                                            (new HttpTask()).execute(title, message);
                                        }
                                    }
                                });
                                builder.show();
                            }
                        }
                    }
                });
            }
        }
    }

    private boolean isInternetCon() {
        cManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        mobile = cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE); //모바일 데이터 여부
        wifi = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI); //와이파이 여부
        return !mobile.isConnected() && !wifi.isConnected(); //결과값을 리턴
    }

    private class HttpTask extends AsyncTask<String, Void, Boolean> {

        /** http://itmir.tistory.com/613를 참조해주세요. */

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mDialog = new ProgressDialog(DeveloperSendActivity.this);
            mDialog.setIndeterminate(true);
            mDialog.setMessage(getString(R.string.post_notice_posting));
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpPost postRequest = new HttpPost("");


                //전달할 값들
                Vector<NameValuePair> nameValue = new Vector<>();
                nameValue.add(new BasicNameValuePair("", ""));
                nameValue.add(new BasicNameValuePair("", params[0]));
                nameValue.add(new BasicNameValuePair("", params[1]));
                nameValue.add(new BasicNameValuePair("deviceId", Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID)));

//                nameValue.add(new BasicNameValuePair("code", HiddenCode.getHiddenCode()));

                //웹 접속 - UTF-8으로
                HttpEntity Entity = new UrlEncodedFormEntity(nameValue, "UTF-8");
                postRequest.setEntity(Entity);

                HttpClient mClient = new DefaultHttpClient();
                mClient.execute(postRequest);

                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }

        protected void onPostExecute(Boolean value) {
            super.onPostExecute(value);

            if (mDialog != null) {
                mDialog.dismiss();
                mDialog = null;
            }

            if (value) {

                MaterialDialog.Builder dialog = new MaterialDialog.Builder(DeveloperSendActivity.this);
                dialog.title(R.string.post_notice_title);
                dialog.content(R.string.post_notice_success);
                dialog.positiveText(android.R.string.ok);
                dialog.show();

                mTitle.setText("");
                mMessage.setText("");
            } else {

                MaterialDialog.Builder dialog = new MaterialDialog.Builder(DeveloperSendActivity.this);
                dialog.title(R.string.post_notice_title);
                dialog.titleColorRes(R.color.red);
                dialog.content(R.string.post_notice_failed);
                dialog.contentColorRes(R.color.red);
                dialog.positiveText(android.R.string.ok);
                dialog.positiveColorRes(R.color.red);
                dialog.show();

            }
        }
    }

}

