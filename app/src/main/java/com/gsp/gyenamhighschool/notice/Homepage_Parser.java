package com.gsp.gyenamhighschool.notice;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;
import net.htmlparser.jericho.StartTag;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public abstract class Homepage_Parser extends AsyncTask<String, Integer, Long> {
    /**
     * --------- Row
     * --------- Row
     * | | | | | Column
     * | | | | | Column
     */
//    public int startRowNumber = 0;
 //   public int startColumnNumber = 0;

    public static ArrayList<ListData> mListData = new ArrayList<>();
    private int BBSlocate;

//    private String url;
    private URL URL;

    private Source source;

    public abstract void onPreDownload();

    public abstract void onUpdate(int params);

    public abstract void onFinish(long result);

//    public abstract void onRow(int startRowNumber, int position, String[] row);

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        onPreDownload();
    }

    @Override
    protected Long doInBackground(String... params) {

            try {
                URL = new URL(params[0]);
                InputStream html = URL.openStream();
                source = new Source(new InputStreamReader(html, "UTF-8")); //소스를 UTF-8 인코딩으로 불러온다.
                source.fullSequentialParse(); //순차적으로 구문분석

            } catch (Exception e) {
                Log.d("ERROR", e + "");
            }


            List<StartTag> tabletags = source.getAllStartTags(HTMLElementName.DIV); // DIV 타입의 모든 태그들을 불러온다.

            publishProgress(10);

            for (int arrnum = 0; arrnum < tabletags.size(); arrnum++) { //DIV 모든 태그중 bbsContent 태그가 몇번째임을 구한다.

                if (tabletags.get(arrnum).toString().equals("<div class=\"bbsContent\">")) {
                    BBSlocate = arrnum; //DIV 클래스가 bbsContent 면 arrnum 값을 BBSlocate 로 몇번째인지 저장한다.
                    Log.d("BBSLOCATES", arrnum + ""); //arrnum 로깅
                    break;
                }

            }

            publishProgress(30);

            Element BBS_DIV = (Element) source.getAllElements(HTMLElementName.DIV).get(BBSlocate); //BBSlocate 번째 의 DIV 를 모두 가져온다.
            Element BBS_TABLE = (Element) BBS_DIV.getAllElements(HTMLElementName.TABLE).get(0); //테이블 접속
            Element BBS_TBODY = (Element) BBS_TABLE.getAllElements(HTMLElementName.TBODY).get(0); //데이터가 있는 TBODY 에 접속

            publishProgress(70);

            for (int C_TR = 0; C_TR < BBS_TBODY.getAllElements(HTMLElementName.TR).size(); C_TR++) { //여기서는 이제부터 게시된 게시물 데이터를 불러와 게시판 인터페이스를 구성할 것이다.

                // 소스의 효율성을 위해서는 for 문을 사용하는것이 좋지만 , 이해를 돕기위해 소스를 일부로 늘려 두었다.

                try {

                    Element BBS_TR = (Element) BBS_TBODY.getAllElements(HTMLElementName.TR).get(C_TR); //TR 접속

                    Element BC_TYPE = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(0); //타입 을 불러온다.

                    Element BC_File = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(1); //TR의 첨부파일 URL을 불러온다

                    Element BC_info = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(2); //URL(herf) TITLE(title) 을 담은 정보를 불러온다.
                    Element BC_a = (Element) BC_info.getAllElements(HTMLElementName.A).get(0); //BC_info 안의 a 태그를 가져온다.
                    String BCS_url = BC_a.getAttributeValue("href"); //a 태그의 herf 는 BCS_url 로 선언
                    String BCS_title = BC_a.getAttributeValue("title"); //a 태그의 title 은 BCS_title 로 선언

                    Element BC_writer = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(3); //글쓴이를 불러온다.
                    Element BC_date = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(4); // 날짜를 불러온다.

                    String BCS_type = BC_TYPE.getContent().toString(); // 타입값을 담은 엘레먼트의 컨텐츠를 문자열로 변환시켜 가져온다.
                    String BCS_writer = BC_writer.getContent().toString(); // 작성자값을 담은 엘레먼트의 컨텐츠를 문자열로 변환시켜 가져온다.
                    String BCS_date = BC_date.getContent().toString(); // 작성일자값을 담은 엘레먼트의 컨텐츠를 문자열로 변환시켜 가져온다.
                    String BCS_file = BC_File.getContent().toString(); // 파일 값을 담은 엘레먼트의 컨텐츠를 문자열로 변환시켜 가져온다.


                    mListData.add(new ListData(BCS_type, BCS_title, BCS_url, BCS_writer, BCS_date, BCS_file)); //데이터가 모이면 데이터 리스트 클래스에 데이터들을 등록한다.
                        /* Log.d("BCSARR","타입:"+BCS_type+"\n제목:" +BCS_title +"\n주소:"+BCS_url +"\n글쓴이:" + BCS_writer + "\n날짜:" + BCS_date);*/

                } catch (Exception e) {
                    Log.d("BCSERROR", e + "");
                    return -1l;
                }
            }

        publishProgress(100);
        return 0l;
    }
    
    @Override
    protected void onProgressUpdate(Integer... params) {
        onUpdate(params[0]);
    }

    @Override
    protected void onPostExecute(Long result) {
        super.onPostExecute(result);
        onFinish(result);
    }



    public String RemoveHTMLTag(String changeStr) {
        if (changeStr != null && !changeStr.equals("")) {
            changeStr = changeStr.replaceAll("<(/)?([a-zA-Z]*)(\\s[a-zA-Z]*=[^>]*)?(\\s)*(/)?>", "");
        } else {
            changeStr = "";
        }
        return changeStr;
    }
}