package com.gsp.gyenamhighschool.notice;


public class ListData { // 데이터를 받는 클래스

    public String mType;
    public String mTitle;
    public String mUrl;
    public String mWriter;
    public String mDate;
    public String mfile_str;

    public ListData()  {

    }

    public ListData(String mType,String mTitle,String mUrl,String mWriter,String mDate, String file_str)  { //데이터를 받는 클래스 메서드
        this.mType = mType;
        this.mTitle = mTitle;
        this.mUrl = mUrl;
        this.mWriter = mWriter;
        this.mDate = mDate;
        this.mfile_str = file_str;

    }

}