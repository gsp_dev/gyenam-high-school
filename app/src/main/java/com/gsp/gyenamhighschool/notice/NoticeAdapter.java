package com.gsp.gyenamhighschool.notice;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gsp.gyenamhighschool.R;
import java.util.ArrayList;


public class NoticeAdapter extends RecyclerView.Adapter<NoticeAdapter.NoticeAdapterViewHolder> {
    //    private int mBackground;
    private Context mContext = null;
    private ArrayList<NoticeAdapterInfo> mValues = new ArrayList<>();

    public NoticeAdapter(Context mContext, ArrayList<NoticeAdapterInfo> itemsData) {
        super();
        this.mContext = mContext;
        this.mValues = itemsData;
    }


    public void addItem(String title, String message, String date) {
        NoticeAdapterInfo addInfo = new NoticeAdapterInfo();

        addInfo.title = title;
        addInfo.message = message;
        addInfo.date = date;

        mValues.add(0, addInfo);
    }

    public void clearData() {
        mValues.clear();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public NoticeAdapter.NoticeAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notice_item, parent, false);


//        mView.setBackgroundResource(mBackground);

        return new NoticeAdapterViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(final NoticeAdapterViewHolder holder, int position) {
        NoticeAdapterInfo mInfo = getItemData(position);

        String title = mInfo.title;
        String message = mInfo.message;
        String date = mInfo.date;

        holder.mTitle.setText(title);
        holder.mMessage.setText(message);
        holder.mDate.setText(date);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public NoticeAdapterInfo getItemData(int position) {
        return mValues.get(position);
    }

    public class NoticeAdapterViewHolder extends RecyclerView.ViewHolder {
        //        public final View mView;
        public final TextView mTitle, mMessage, mDate;

        public NoticeAdapterViewHolder(View mView) {
            super(mView);
//            this.mView = mView;

            mTitle = (TextView) mView.findViewById(R.id.mTitle);
            mMessage = (TextView) mView.findViewById(R.id.mMessage);
            mDate = (TextView) mView.findViewById(R.id.mDate);
        }
    }

    public class NoticeAdapterInfo {
        public String title;
        public String message;
        public String date;
    }
}


