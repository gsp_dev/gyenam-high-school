package com.gsp.gyenamhighschool.notice;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gsp.gyenamhighschool.R;
import com.gsp.gyenamhighschool.spreadsheets.GoogleSheetTask;
import com.gsp.gyenamhighschool.tool.Database;
import com.gsp.gyenamhighschool.tool.Preference;
import com.gsp.gyenamhighschool.tool.TimeTableTool;
import com.gsp.gyenamhighschool.tool.Tools;

import java.io.File;
import java.util.ArrayList;

import static com.gsp.gyenamhighschool.tool.Tools.isOnline_Fixed;

public class NoticeFragment_app extends Fragment {

    public static final String NoticeDBName = "AppNotice.db";
    public static final String NoticeTableName = "AppNoticeInfo";

    RecyclerView recyclerView;
    NoticeAdapter mAdapter;
    ProgressDialog mDialog;

    SwipeRefreshLayout mSwipeRefreshLayout;

    private ConnectivityManager cManager;
    private NetworkInfo mobile;
    private NetworkInfo wifi;

    int c = 0;

    ArrayList<NoticeAdapter.NoticeAdapterInfo> mListData = new ArrayList<>();

    com.gsp.gyenamhighschool.tool.Preference prefTheme;
    int color;

    public static Fragment getInstance(int index) {
        NoticeFragment_app noticeFragment_app = new NoticeFragment_app();

        Bundle args = new Bundle();
        args.putInt("code", index);
        noticeFragment_app.setArguments(args);

        return noticeFragment_app;
    }

    /**setUserVisibleHint 메소드는 해당 Fragment가 사용자에게 보여졌을 때의
     * 작업을 설정할 수 있는 메소드이다.
     *
     * Fragment는 양 옆(2번째 Fragment라면 1번째와 3번째)의 Fragment들이 사용자에게 보여지기 전 미리 로딩되기 때문에
     * setUserVisibleHint 메소드를 이용하여 보여졌을 때만 수행될 코드를 작성했다.*/

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) { // 보여졌을 때

            /** 변수 c에 대입 후 1을 더해주는 이유는 처음 보여졌을 때만 다음 작업들을 수행하기 위해서 이다.
             * 즉, 다른 Fragment가 보여졌다가 이 Fragment가 다시 보여졌을 때 코드들이 재작동되는 것을 막기 위해서
             * c가 1일때만 작동하게 코드를 작성했다.(2번째 보여졌을 때는 c값이 2라 코드가 조건에 맞지 않아 작동하지 않음.)
             * 사실 String 값을 이용하는게 더 안정적일 것 같다.*/

            c = c + 1;
            if (c == 1) {

                if (isInternetCon()) { //false 반환시 if 문안의 로직 실행
                    Toast.makeText(getActivity(), "인터넷에 연결되지않아 불러오기를 중단합니다.", Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                } else { //인터넷 체크 통과시 실행할 로직

                    if (isOnline_Fixed()) { //네트워크가 올바르면
                        try {
                            showNoticeData(false); //네트워크 관련은 따로 쓰레드를 생성해야 UI 쓰레드와 겹치지 않는다. 그러므로 Thread 가 선언된 process 메서드를 호출한다.
                            mAdapter.notifyDataSetChanged();
                        } catch (Exception e) {
                            Log.d("ERROR", e + "");
                        }
                    } else {
                        Toast.makeText(getActivity(), "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        prefTheme = new com.gsp.gyenamhighschool.tool.Preference(getActivity(),"theme");

        color = prefTheme.getInt("theme",0);

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.app, container, false);


        recyclerView = (RecyclerView) rootView.findViewById(R.id.app_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new NoticeAdapter(getActivity(), mListData); // * 매우 중요!!! recyclerView가 선언 되기 전에 선언해주어야한다.
        recyclerView.setAdapter(mAdapter);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.mSwipeRefreshLayout);

        if (color == Color.parseColor("#F44336"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.red);
        else if (color == Color.parseColor("#E91E63"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.pink);
        else if (color == Color.parseColor("#FFC107"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.yellow);
        else if (color == Color.parseColor("#8BC34A"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.green);
        else if (color == Color.parseColor("#03A9F4"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        else if (color == Color.parseColor("#3F51B5"))
            mSwipeRefreshLayout.setColorSchemeResources(R.color.indigo);
        else
            mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showNoticeData(true);

                if (mSwipeRefreshLayout.isRefreshing())
                    mSwipeRefreshLayout.setRefreshing(false);
            }
        });


//        showNoticeData(false);

        return rootView;
    }

    private void showNoticeData(boolean forceUpdate) {
        mAdapter.clearData();
        mAdapter.notifyDataSetChanged();

        if (Tools.isOnline(getActivity())) {
            if (Tools.isWifi(getActivity()) || forceUpdate) {
                if (isOnline_Fixed()) {
                    getNoticeDownloadTask mTask = new getNoticeDownloadTask();
                    mTask.execute(""); //파싱할 구글 스프레드 시트 URL을 입력해주세요
                } else {
                    Toast.makeText(getActivity(), "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (isOnline_Fixed()) {
                    getNoticeDownloadTask mTask = new getNoticeDownloadTask();
                    mTask.execute(""); // 파싱할 구글 스프레드 시트 URL을 입력해주세요
                } else {
                    Toast.makeText(getActivity(), "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            offlineData();
        }
    }

    private void offlineData() {
        if (new File(TimeTableTool.mFilePath + NoticeDBName).exists()) {
            showListViewDate();
        } else {

                MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
                builder.title(R.string.no_network_title);
                builder.titleColorRes(R.color.red);
                builder.content(getString(R.string.no_network_msg));
                builder.contentColorRes(R.color.red);
                builder.positiveText(android.R.string.ok);
                builder.positiveColorRes(R.color.red);
                builder.show();

        }

        if (mSwipeRefreshLayout.isRefreshing())
            mSwipeRefreshLayout.setRefreshing(false);
    }

    class getNoticeDownloadTask extends GoogleSheetTask {
        private Database mDatabase;
        private String[] columnFirstRow;

        @Override
        public void onPreDownload() {
            mDialog = new ProgressDialog(getActivity());
            mDialog.setIndeterminate(true);
            mDialog.setMessage(getString(R.string.loading_title));
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.show();

            new File(TimeTableTool.mFilePath + NoticeDBName).delete();
            mDatabase = new Database();
        }

        @Override
        public void onFinish(long result) {
            if (mDialog != null) {
                mDialog.dismiss();
                mDialog = null;
            }

            if (mDatabase != null)
                mDatabase.release();

            showListViewDate();
        }

        @Override
        public void onRow(int startRowNumber, int position, String[] row) {
            if (startRowNumber == position) {
                columnFirstRow = row;

                StringBuilder Column = new StringBuilder();

                // remove deviceId
                for (int i = 0; i < row.length - 1; i++) {
                    Column.append(row[i]);
                    Column.append(" text, ");
                }

                mDatabase.openOrCreateDatabase(TimeTableTool.mFilePath, NoticeDBName, NoticeTableName, Column.substring(0, Column.length() - 2));
            } else {
                int length = row.length;
                for (int i = 0; i < length - 1; i++) {
                    mDatabase.addData(columnFirstRow[i], row[i]);
                }
                mDatabase.commit(NoticeTableName);
            }
        }
    }

    private void showListViewDate() {
        Database mDatabase = new Database();
        mDatabase.openDatabase(TimeTableTool.mFilePath, NoticeDBName);
        Cursor mCursor = mDatabase.getData(NoticeTableName, "*");

        for (int i = 0; i < mCursor.getCount(); i++) {
            mCursor.moveToNext();

            String date = mCursor.getString(1);
            String title = mCursor.getString(2);
            String message = mCursor.getString(3);

            mAdapter.addItem(title, message, date);
        }

        mAdapter.notifyDataSetChanged();

        if (mSwipeRefreshLayout.isRefreshing())
            mSwipeRefreshLayout.setRefreshing(false);
    }
    private boolean isInternetCon() {
        cManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        mobile = cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE); //모바일 데이터 여부
        wifi = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI); //와이파이 여부
        return !mobile.isConnected() && !wifi.isConnected(); //결과값을 리턴
    }


}
