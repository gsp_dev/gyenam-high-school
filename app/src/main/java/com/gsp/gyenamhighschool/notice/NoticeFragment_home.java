package com.gsp.gyenamhighschool.notice;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.gsp.gyenamhighschool.R;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;
import net.htmlparser.jericho.StartTag;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.gsp.gyenamhighschool.tool.Tools.isOnline_Fixed;

public class NoticeFragment_home extends Fragment {



    private static String URL_PRIMARY = "http://www.gyenam.hs.kr";
    private static String GETNOTICE = "/main.php?menugrp=040200&master=bbs&act=list&master_sid=16";

    private String url;
    private java.net.URL URL;

    private Source source;
    private MaterialDialog materialDialog;
    private Notice_Web_Adapter BBSAdapter = null;
    private RecyclerView BBSList;
    private int BBSlocate;

    private ConnectivityManager cManager;
    private NetworkInfo mobile;
    private NetworkInfo wifi;
    int b = 0;

    String check_str;

    com.gsp.gyenamhighschool.tool.Preference prefTheme;
    int color;

    SwipeRefreshLayout swipeRefreshLayout;

    ArrayList<ListData> mListData = new ArrayList<>();

    public static Fragment getInstance(int index) {
        NoticeFragment_home noticeFragment_home = new NoticeFragment_home();

        Bundle args = new Bundle();
        args.putInt("code", index);
        noticeFragment_home.setArguments(args);

        return noticeFragment_home;
    }

    /** setUserVisibleHint 메소드는 해당 Fragment가 사용자에게 보여졌을 때의
     * 작업을 설정할 수 있는 메소드이다.
     *
     * Fragment는 양 옆(2번째 Fragment라면 1번째와 3번째)의 Fragment들이 사용자에게 보여지기 전 미리 로딩되기 때문에
     * setUserVisibleHint 메소드를 이용하여 보여졌을 때만 수행될 코드를 작성했다.*/

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {

            /** 변수 b에 대입 후 1을 더해주는 이유는 처음 보여졌을 때만 다음 작업들을 수행하기 위해서 이다.
             * 즉, 다른 Fragment가 보여졌다가 이 Fragment가 다시 보여졌을 때 코드들이 재작동되는 것을 막기 위해서
             * b가 1일때만 작동하게 코드를 작성했다.(2번째 보여졌을 때는 b값이 2라 코드가 조건에 맞지 않아 작동하지 않음.)
             * 사실 String 값을 이용하는게 더 안정적일 것 같다.*/

            b = b + 1;
            if (b == 1) {
                url = URL_PRIMARY + GETNOTICE; //파싱하기전 PRIMARY URL 과 공지사항 URL 을 합쳐 완전한 URL 을만든다.

                if (isInternetCon()) { //false 반환시 if 문안의 로직 실행
                    Toast.makeText(getActivity(), "인터넷에 연결되지않아 불러오기를 중단합니다.", Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                } else { //인터넷 체크 통과시 실행할 로직

                    if (isOnline_Fixed()) { //네트워크가 올바르면
                        try {
                            process(); //네트워크 관련은 따로 쓰레드를 생성해야 UI 쓰레드와 겹치지 않는다. 그러므로 Thread 가 선언된 process 메서드를 호출한다.
                            BBSAdapter.notifyDataSetChanged();
                        } catch (Exception e) {
                            Log.d("ERROR", e + "");
                        }
                    } else {
                        Toast.makeText(getActivity(), "네트워크가 올바르지 않아 불러오기를 중단합니다.", Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    }
                }
            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        prefTheme = new com.gsp.gyenamhighschool.tool.Preference(getActivity(),"theme");

        color = prefTheme.getInt("theme",0);

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.homepage, container, false);

        BBSList = (RecyclerView) rootView.findViewById(R.id.homepage_ListView); //리스트선언

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.mSwipeRefreshLayout_homepage);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                mListData.clear();
                BBSAdapter.notifyDataSetChanged();

                if (isInternetCon()) { //false 반환시 if 문안의 로직 실행
                    Toast.makeText(getActivity(), "인터넷에 연결해주세요 :)", Toast.LENGTH_SHORT).show();
                //    getActivity().finish();
                } else { //인터넷 체크 통과시 실행할 로직

                    if (isOnline_Fixed()) { // 네트워크가 올바르면
                        try {
                            process(); //네트워크 관련은 따로 쓰레드를 생성해야 UI 쓰레드와 겹치지 않는다. 그러므로 Thread 가 선언된 process 메서드를 호출한다.
                        } catch (Exception e) {
                            Log.d("ERROR", e + "");

                        }
                    } else {
                        Toast.makeText(getActivity(), "네트워크가 올바르지 않아 불러오기를 중단합니다.", Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    }
                }

                if (swipeRefreshLayout.isRefreshing())
                    swipeRefreshLayout.setRefreshing(false);
            }
        });

        if (color == Color.parseColor("#F44336"))
            swipeRefreshLayout.setColorSchemeResources(R.color.red);
        else if (color == Color.parseColor("#E91E63"))
            swipeRefreshLayout.setColorSchemeResources(R.color.pink);
        else if (color == Color.parseColor("#FFC107"))
            swipeRefreshLayout.setColorSchemeResources(R.color.yellow);
        else if (color == Color.parseColor("#8BC34A"))
            swipeRefreshLayout.setColorSchemeResources(R.color.green);
        else if (color == Color.parseColor("#03A9F4"))
            swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        else if (color == Color.parseColor("#3F51B5"))
            swipeRefreshLayout.setColorSchemeResources(R.color.indigo);
        else
            swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);

        BBSList.setLayoutManager(new LinearLayoutManager(getActivity()));
        BBSList.setItemAnimator(new DefaultItemAnimator());
        BBSAdapter = new Notice_Web_Adapter(getActivity(), mListData);
        BBSList.setAdapter(BBSAdapter); //리스트에 어댑터를 먹여준다.
        BBSAdapter.SetOnItemClickListener( //리스트 클릭시 실행될 로직 선언
                new Notice_Web_Adapter.OnItemClickListener() {

                    @Override
                    public void onItemClick(View v, int position) {

                        if (mListData != null) {

                            ListData mData = mListData.get(position); // 클릭한 포지션의 데이터를 가져온다.
                            String URL_BCS = mData.mUrl; //가져온 데이터 중 url 부분만 적출해낸다.
                            Intent intent = new Intent(getActivity(), Notice_homepage.class);
                            intent.putExtra("url", URL_BCS); // url 값을 실행할 액티비티에 보낸다.
                            intent.putExtra("number", "1"); // "1"을 실행할 액티비티에 보낸다.
                            startActivity(intent); // 액티비티 실행

                        } else {
                            Toast.makeText(getActivity(), "아직 불러오는 중 입니다 :)", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

        return rootView;
    }

    private void process() throws IOException { // 파싱

        new Thread() {

            @Override
            public void run() {

                Handler Progress = new Handler(Looper.getMainLooper()); //네트워크 쓰레드와 별개로 따로 핸들러를 이용하여 쓰레드를 생성한다.
                Progress.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        materialDialog = new MaterialDialog.Builder(getActivity())
                                .content("게시판 정보를 불러오는 중 이에요")
                                .cancelable(false)
                                .progress(true, 100)
                                .show();
                    }
                }, 0);

                try {
                    URL = new URL(url);
                    InputStream html = URL.openStream();
                    source = new Source(new InputStreamReader(html, "UTF-8")); //소스를 UTF-8 인코딩으로 불러온다.
                    source.fullSequentialParse(); //순차적으로 구문분석
                } catch (Exception e) {
                    Log.d("ERROR", e + "");
                }


                List<StartTag> tabletags = source.getAllStartTags(HTMLElementName.DIV); // DIV 타입의 모든 태그들을 불러온다.

                if (tabletags != null) {

                    for (int arrnum = 0; arrnum < tabletags.size(); arrnum++) { //DIV 모든 태그중 bbsContent 태그가 몇번째임을 구한다.

                        if (tabletags.get(arrnum).toString().equals("<div class=\"bbsContent\">")) {
                            BBSlocate = arrnum; //DIV 클래스가 bbsContent 면 arrnum 값을 BBSlocate 로 몇번째인지 저장한다.
                            Log.d("BBSLOCATES", arrnum + ""); //arrnum 로깅
                            break;
                        }
                    }



                Element BBS_DIV = (Element) source.getAllElements(HTMLElementName.DIV).get(BBSlocate); //BBSlocate 번째 의 DIV 를 모두 가져온다.
                Element BBS_TABLE = (Element) BBS_DIV.getAllElements(HTMLElementName.TABLE).get(0); //테이블 접속
                Element BBS_TBODY = (Element) BBS_TABLE.getAllElements(HTMLElementName.TBODY).get(0); //데이터가 있는 TBODY 에 접속


                for (int C_TR = 0; C_TR < BBS_TBODY.getAllElements(HTMLElementName.TR).size(); C_TR++) { //여기서는 이제부터 게시된 게시물 데이터를 불러와 게시판 인터페이스를 구성할 것이다.


                    // 소스의 효율성을 위해서는 for 문을 사용하는것이 좋지만 , 이해를 돕기위해 소스를 일부로 늘려 두었다.

                    try {
                        Element BBS_TR = (Element) BBS_TBODY.getAllElements(HTMLElementName.TR).get(C_TR); //TR 접속

                        Element BC_TYPE = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(0); //타입 을 불러온다.

                        Element BC_File = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(1);

                        Element BC_info = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(2); //URL(herf) TITLE(title) 을 담은 정보를 불러온다.
                        Element BC_a = (Element) BC_info.getAllElements(HTMLElementName.A).get(0); //BC_info 안의 a 태그를 가져온다.
                        String BCS_url = BC_a.getAttributeValue("href"); //a 태그의 herf 는 BCS_url 로 선언
                        String BCS_title = BC_a.getAttributeValue("title"); //a 태그의 title 은 BCS_title 로 선언

                        Element BC_writer = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(3); //글쓴이를 불러온다.
                        Element BC_date = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(4); // 날짜를 불러온다.

                        String BCS_type = BC_TYPE.getContent().toString(); // 타입값을 담은 엘레먼트의 컨텐츠를 문자열로 변환시켜 가져온다.
                        String BCS_writer = BC_writer.getContent().toString(); // 작성자값을 담은 엘레먼트의 컨텐츠를 문자열로 변환시켜 가져온다.
                        String BCS_date = BC_date.getContent().toString(); // 작성일자값을 담은 엘레먼트의 컨텐츠를 문자열로 변환시켜 가져온다.
                        String BCS_file = BC_File.getContent().toString(); // 파일 값을 담은 엘레먼트의 컨텐츠를 문자열로 변환시켜 가져온다.


                        mListData.add(new ListData(BCS_type, BCS_title, BCS_url, BCS_writer, BCS_date, BCS_file)); //데이터가 모이면 데이터 리스트 클래스에 데이터들을 등록한다.
                        /* Log.d("BCSARR","타입:"+BCS_type+"\n제목:" +BCS_title +"\n주소:"+BCS_url +"\n글쓴이:" + BCS_writer + "\n날짜:" + BCS_date);*/


                    } catch (Exception e) {
                        Log.d("BCSERROR", e + "");
                    }
                }
                Handler mHandler = new Handler(Looper.getMainLooper());
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        BBSAdapter.notifyDataSetChanged(); //모든 작업이 끝나면 리스트 갱신
                        materialDialog.dismiss(); //모든 작업이 끝나면 다이어로그 종료
                        check_str = "already";
                    }
                }, 0);


            }

        }
        }.start();


    }


    private boolean isInternetCon() {
        cManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        mobile = cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE); //모바일 데이터 여부
        wifi = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI); //와이파이 여부
        return !mobile.isConnected() && !wifi.isConnected(); //결과값을 리턴
    }

}
