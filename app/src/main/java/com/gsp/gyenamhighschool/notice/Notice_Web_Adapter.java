package com.gsp.gyenamhighschool.notice;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gsp.gyenamhighschool.R;

import org.w3c.dom.Text;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class Notice_Web_Adapter extends RecyclerView.Adapter<Notice_Web_Adapter.ViewHolder> {

    com.gsp.gyenamhighschool.tool.Preference prefTheme;
    int color;
    private ArrayList<ListData> listData;
    Context mContext;
    OnItemClickListener mItemClickListener;

    public Notice_Web_Adapter(Context context, ArrayList<ListData> itemsData) {
        this.listData = itemsData;
        this.mContext = context;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view , int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public int getItemCount() {
        return this.listData.size();
    }


    @Override
    public Notice_Web_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        prefTheme = new com.gsp.gyenamhighschool.tool.Preference(mContext, "theme");

        color = prefTheme.getInt("theme", 0);

        View sampleItemLayout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.itemstyle, parent, false);

        TextView textView = (TextView) sampleItemLayout.findViewById(R.id.item_title);
        textView.setSelected(true);


        /**레이아웃을 색상에 맞게 지정하는 코드입니다.*/
        if (color == Color.parseColor("#F44336")) {
            RelativeLayout relativeLayout = (RelativeLayout) sampleItemLayout.findViewById(R.id.relative);
            relativeLayout.setBackgroundResource(R.drawable.selector_red);
        } else if (color == Color.parseColor("#E91E63")){
            RelativeLayout relativeLayout = (RelativeLayout) sampleItemLayout.findViewById(R.id.relative);
            relativeLayout.setBackgroundResource(R.drawable.selector_pink);
        } else if (color == Color.parseColor("#FFC107")){
            RelativeLayout relativeLayout = (RelativeLayout) sampleItemLayout.findViewById(R.id.relative);
            relativeLayout.setBackgroundResource(R.drawable.selector_yellow);
        } else if (color == Color.parseColor("#8BC34A")){
            RelativeLayout relativeLayout = (RelativeLayout) sampleItemLayout.findViewById(R.id.relative);
            relativeLayout.setBackgroundResource(R.drawable.selector_green);
        } else if (color == Color.parseColor("#03A9F4")){
            RelativeLayout relativeLayout = (RelativeLayout) sampleItemLayout.findViewById(R.id.relative);
            relativeLayout.setBackgroundResource(R.drawable.selector);
        } else if (color == Color.parseColor("#3F51B5")){
            RelativeLayout relativeLayout = (RelativeLayout) sampleItemLayout.findViewById(R.id.relative);
            relativeLayout.setBackgroundResource(R.drawable.selector_indigo);
        } else {
            RelativeLayout relativeLayout = (RelativeLayout) sampleItemLayout.findViewById(R.id.relative);
            relativeLayout.setBackgroundResource(R.drawable.selector);
        }


        return new Notice_Web_Adapter.ViewHolder(sampleItemLayout);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        ListData mData = listData.get(position);

        if(mData.mType.equals("공지")){
            viewHolder.mTitle.setText(Html.fromHtml("<font color=#FF0000>[공지] </font>" +mData.mTitle)); //"공지" 의 색깔을 부분적으로 약간 진하게 수정.
        }else{
            viewHolder.mTitle.setText(mData.mTitle);
        }

        if(mData.mfile_str.contains("<img src")) {
            viewHolder.imageView.setImageResource(R.drawable.ic_file); //<img src의 태그를 가지고 있으면 첨부파일 아이콘을 설정해줌
        }

        viewHolder.mDate.setText(mData.mDate);
        viewHolder.mWriter.setText("작성자 : " + mData.mWriter);

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView mTitle;
        public TextView mWriter;
        public TextView mDate;
        public ImageView imageView;


        public ViewHolder(View view) {
            super(view);
            this.mTitle = (TextView)view.findViewById(R.id.item_title);
            this.mWriter = (TextView)view.findViewById(R.id.item_writer);
            this.mDate = (TextView)view.findViewById(R.id.item_date);
            this.imageView = (ImageView) view.findViewById(R.id.file_icon);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());

            }
        }

    }


}
