package com.gsp.gyenamhighschool.notice;


import android.Manifest;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.gsp.gyenamhighschool.MainActivity;
import com.gsp.gyenamhighschool.R;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;
import net.htmlparser.jericho.StartTag;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.List;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import static com.gsp.gyenamhighschool.tool.Tools.isOnline_Fixed;


public class Notice_homepage extends AppCompatActivity {

    private static String URL_PRIMARY_SUB = "http://www.gyenam.hs.kr/"; //홈페이지 원본 주소이다.
    private static String GETNOTICE_SUB; //홈페이지 의 게시판을 나타내는 뒤 주소, 비슷한 게시판들은 거의 파싱이 가능하므로 응용하여 사용하자.
    private String url;
    private URL URL;

    private Source source;
    private ProgressDialog progressDialog;
    private int BBSlocate2;
    private int BBSlocate;
    private ConnectivityManager cManager;
    private NetworkInfo mobile;
    private NetworkInfo wifi;
    private TextView title;
    private TextView writer;
    private TextView date;
    private TextView content;
    private TextView file, file2, file3, file4, file5, file6, file7, file8, file9, file10;
    private String title_str, writer_str, date_str, content_str;
    private String file_str, file_str2, file_str3, file_str4, file_str5, file_str6, file_str7, file_str8, file_str9, file_str10;
    private String file_link, file_link2, file_link3, file_link4, file_link5, file_link6, file_link7, file_link8, file_link9, file_link10;
    CardView cardView, cardView2, cardView3, cardView4, cardView5, cardView6, cardView7, cardView8, cardView9, cardView10;

    com.gsp.gyenamhighschool.tool.Preference prefTheme;
    int color;

    @Override
    protected void onStop() { //멈추었을때 다이어로그를 제거해주는 메서드
        super.onStop();
        if ( progressDialog != null)
            progressDialog.dismiss(); //다이어로그가 켜져있을경우 (!null) 종료시켜준다
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefTheme = new com.gsp.gyenamhighschool.tool.Preference(getApplicationContext(), "theme");

        color = prefTheme.getInt("theme", 0);


        if (color == Color.parseColor("#F44336"))
            setTheme(R.style.AppTheme_Red);
        if (color == Color.parseColor("#E91E63"))
            setTheme(R.style.AppTheme_Pink);
        if (color == Color.parseColor("#FFC107"))
            setTheme(R.style.AppTheme_Yellow);
        if (color == Color.parseColor("#8BC34A"))
            setTheme(R.style.AppTheme_Green);
        if (color == Color.parseColor("#03A9F4"))
            setTheme(R.style.AppTheme);
        if (color == Color.parseColor("#3F51B5"))
            setTheme(R.style.AppTheme_Indigo);

        String a = getIntent().getStringExtra("number");

        if (a.equals("1")){
            setTitle("공지사항(학교 홈페이지)");
        } else if (a.equals("2")) {
            setTitle("가정통신문(학교 홈페이지)");
        }


        setContentView(R.layout.notice_homepage_item);

        title = (TextView) findViewById(R.id.title);
        writer = (TextView) findViewById(R.id.writer);
        date = (TextView) findViewById(R.id.date);
        content = (TextView) findViewById(R.id.content);

        file = (TextView) findViewById(R.id.file);
        file2 = (TextView) findViewById(R.id.file2);
        file3 = (TextView) findViewById(R.id.file3);
        file4 = (TextView) findViewById(R.id.file4);
        file5 = (TextView) findViewById(R.id.file5);
        file6 = (TextView) findViewById(R.id.file6);
        file7 = (TextView) findViewById(R.id.file7);
        file8 = (TextView) findViewById(R.id.file8);
        file9 = (TextView) findViewById(R.id.file9);
        file10 = (TextView) findViewById(R.id.file10);



        final int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);


        cardView = (CardView) findViewById(R.id.file_cardview);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(permissionCheck == PackageManager.PERMISSION_DENIED) {
                    //권한 없음
                    Toast.makeText(Notice_homepage.this, "저장공간 권한이 없습니다.\n설정-어플리케이션-계남고등학교-권한에 들어가서 권한을 체크해주세요", Toast.LENGTH_SHORT).show();
                } else {
                    //권한 있음

                    if (isInternetCon()) {
                        Toast.makeText(Notice_homepage.this, "인터넷에 연결해주세요 :)", Toast.LENGTH_SHORT).show();
                    } else {
                        if (isOnline_Fixed()) { //네트워크 올바른지 체크
                            Toast.makeText(Notice_homepage.this, file_str + "를 다운로드 합니다.", Toast.LENGTH_SHORT).show();
                            Downjob(URL_PRIMARY_SUB + "/" + file_link, file_str, file_str);
                        } else {
                            Toast.makeText(Notice_homepage.this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                if(permissionCheck== PackageManager.PERMISSION_DENIED){
                    // 권한 없음
                    Toast.makeText(Notice_homepage.this, "저장공간 권한이 없습니다.\n설정-어플리케이션-계남고등학교-권한에 들어가서 권한을 체크해주세요 :)", Toast.LENGTH_LONG).show();
                } else {
                    // 권한 있음

                    if (isInternetCon()) {
                        Toast.makeText(Notice_homepage.this, "인터넷에 연결해주세요 :)", Toast.LENGTH_SHORT).show();
                    } else {
                        if (isOnline_Fixed()) { //네트워크 올바른지 체크
                            Toast.makeText(Notice_homepage.this, file_str + "를 다운로드 합니다.", Toast.LENGTH_SHORT).show();
                            Downjob(URL_PRIMARY_SUB + "/" + file_link, file_str, file_str);
                        } else {
                            Toast.makeText(Notice_homepage.this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                        }
                    }
                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URL_PRIMARY_SUB + file_link)));

                }
            }
        });
        cardView2 = (CardView) findViewById(R.id.file_cardview2);
        cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (permissionCheck == PackageManager.PERMISSION_DENIED) {
                    // 권한 없음
                    Toast.makeText(Notice_homepage.this, "저장공간 권한이 없습니다.\n설정-어플리케이션-계남고등학교-권한에 들어가서 권한을 체크해주세요 :)", Toast.LENGTH_LONG).show();
                } else {
                    // 권한 있음

                    if (isInternetCon()) {
                        Toast.makeText(Notice_homepage.this, "인터넷에 연결해주세요 :)", Toast.LENGTH_SHORT).show();
                    } else {
                        if (isOnline_Fixed()) { //네트워크 올바른지 체크
                            Toast.makeText(Notice_homepage.this, file_str2 + "를 다운로드 합니다.", Toast.LENGTH_SHORT).show();
                            Downjob(URL_PRIMARY_SUB + "/" + file_str2, file_str2, file_str2);
                        } else {
                            Toast.makeText(Notice_homepage.this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                        }
                    }
                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URL_PRIMARY_SUB + file_link)));
                }
            }
        });
        cardView3 = (CardView) findViewById(R.id.file_cardview3);
        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(permissionCheck== PackageManager.PERMISSION_DENIED){
                    // 권한 없음
                    Toast.makeText(Notice_homepage.this, "저장공간 권한이 없습니다.\n설정-어플리케이션-계남고등학교-권한에 들어가서 권한을 체크해주세요 :)", Toast.LENGTH_LONG).show();
                } else {
                    // 권한 있음

                    if (isInternetCon()) {
                        Toast.makeText(Notice_homepage.this, "인터넷에 연결해주세요 :)", Toast.LENGTH_SHORT).show();
                    } else {
                        if (isOnline_Fixed()) { //네트워크 올바른지 체크
                            Toast.makeText(Notice_homepage.this, file_str3 + "를 다운로드 합니다.", Toast.LENGTH_SHORT).show();
                            Downjob(URL_PRIMARY_SUB + "/" + file_link3, file_str3, file_str3);
                        } else {
                            Toast.makeText(Notice_homepage.this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                        }
                    }
                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URL_PRIMARY_SUB + file_link)));
                }
            }
        });
        cardView4 = (CardView) findViewById(R.id.file_cardview4);
        cardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(permissionCheck== PackageManager.PERMISSION_DENIED){
                    // 권한 없음
                    Toast.makeText(Notice_homepage.this, "저장공간 권한이 없습니다.\n설정-어플리케이션-계남고등학교-권한에 들어가서 권한을 체크해주세요 :)", Toast.LENGTH_LONG).show();
                } else {
                    // 권한 있음

                    if (isInternetCon()) {
                        Toast.makeText(Notice_homepage.this, "인터넷에 연결해주세요 :)", Toast.LENGTH_SHORT).show();
                    } else {
                        if (isOnline_Fixed()) { //네트워크 올바른지 체크
                            Toast.makeText(Notice_homepage.this, file_str4 + "를 다운로드 합니다.", Toast.LENGTH_SHORT).show();
                            Downjob(URL_PRIMARY_SUB + "/" + file_link4, file_str4, file_str4);
                        } else {
                            Toast.makeText(Notice_homepage.this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                        }
                    }
                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URL_PRIMARY_SUB + file_link)));
                }
            }
        });
        cardView5 = (CardView) findViewById(R.id.file_cardview5);
        cardView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(permissionCheck== PackageManager.PERMISSION_DENIED){
                    // 권한 없음
                    Toast.makeText(Notice_homepage.this, "저장공간 권한이 없습니다.\n설정-어플리케이션-계남고등학교-권한에 들어가서 권한을 체크해주세요 :)", Toast.LENGTH_LONG).show();
                } else {
                    // 권한 있음

                    if (isInternetCon()) {
                        Toast.makeText(Notice_homepage.this, "인터넷에 연결해주세요 :)", Toast.LENGTH_SHORT).show();
                    } else {
                        if (isOnline_Fixed()) { //네트워크 올바른지 체크
                            Toast.makeText(Notice_homepage.this, file_str5 + "를 다운로드 합니다.", Toast.LENGTH_SHORT).show();
                            Downjob(URL_PRIMARY_SUB + "/" + file_link5, file_str5, file_str5);
                        } else {
                            Toast.makeText(Notice_homepage.this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                        }
                    }
                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URL_PRIMARY_SUB + file_link)));
                }
            }
        });
        cardView6 = (CardView) findViewById(R.id.file_cardview6);
        cardView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(permissionCheck== PackageManager.PERMISSION_DENIED){
                    // 권한 없음
                    Toast.makeText(Notice_homepage.this, "저장공간 권한이 없습니다.\n설정-어플리케이션-계남고등학교-권한에 들어가서 권한을 체크해주세요 :)", Toast.LENGTH_LONG).show();
                } else {
                    // 권한 있음

                    if (isInternetCon()) {
                        Toast.makeText(Notice_homepage.this, "인터넷에 연결해주세요 :)", Toast.LENGTH_SHORT).show();
                    } else {
                        if (isOnline_Fixed()) { //네트워크 올바른지 체크
                            Toast.makeText(Notice_homepage.this, file_str6 + "를 다운로드 합니다.", Toast.LENGTH_SHORT).show();
                            Downjob(URL_PRIMARY_SUB + "/" + file_link6, file_str6, file_str6);
                        } else {
                            Toast.makeText(Notice_homepage.this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                        }
                    }
                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URL_PRIMARY_SUB + file_link)));
                }
            }
        });
        cardView7 = (CardView) findViewById(R.id.file_cardview7);
        cardView7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(permissionCheck== PackageManager.PERMISSION_DENIED){
                    // 권한 없음
                    Toast.makeText(Notice_homepage.this, "저장공간 권한이 없습니다.\n설정-어플리케이션-계남고등학교-권한에 들어가서 권한을 체크해주세요 :)", Toast.LENGTH_LONG).show();
                } else {
                    // 권한 있음

                    if (isInternetCon()) {
                        Toast.makeText(Notice_homepage.this, "인터넷에 연결해주세요 :)", Toast.LENGTH_SHORT).show();
                    } else {
                        if (isOnline_Fixed()) { //네트워크 올바른지 체크
                            Toast.makeText(Notice_homepage.this, file_str7 + "를 다운로드 합니다.", Toast.LENGTH_SHORT).show();
                            Downjob(URL_PRIMARY_SUB + "/" + file_link7, file_str7, file_str7);
                        } else {
                            Toast.makeText(Notice_homepage.this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                        }
                    }
                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URL_PRIMARY_SUB + file_link)));
                }
            }
        });
        cardView8 = (CardView) findViewById(R.id.file_cardview8);
        cardView8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(permissionCheck== PackageManager.PERMISSION_DENIED){
                    // 권한 없음
                    Toast.makeText(Notice_homepage.this, "저장공간 권한이 없습니다.\n설정-어플리케이션-계남고등학교-권한에 들어가서 권한을 체크해주세요 :)", Toast.LENGTH_LONG).show();
                } else {
                    // 권한 있음

                    if (isInternetCon()) {
                        Toast.makeText(Notice_homepage.this, "인터넷에 연결해주세요 :)", Toast.LENGTH_SHORT).show();
                    } else {
                        if (isOnline_Fixed()) { //네트워크 올바른지 체크
                            Toast.makeText(Notice_homepage.this, file_str8 + "를 다운로드 합니다.", Toast.LENGTH_SHORT).show();
                            Downjob(URL_PRIMARY_SUB + "/" + file_link8, file_str8, file_str8);
                        } else {
                            Toast.makeText(Notice_homepage.this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                        }
                    }
                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URL_PRIMARY_SUB + file_link)));
                }
            }
        });
        cardView9 = (CardView) findViewById(R.id.file_cardview9);
        cardView9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(permissionCheck== PackageManager.PERMISSION_DENIED){
                    // 권한 없음
                    Toast.makeText(Notice_homepage.this, "저장공간 권한이 없습니다.\n설정-어플리케이션-계남고등학교-권한에 들어가서 권한을 체크해주세요 :)", Toast.LENGTH_LONG).show();
                } else {
                    // 권한 있음

                    if (isInternetCon()) {
                        Toast.makeText(Notice_homepage.this, "인터넷에 연결해주세요 :)", Toast.LENGTH_SHORT).show();
                    } else {
                        if (isOnline_Fixed()) { //네트워크 올바른지 체크
                            Toast.makeText(Notice_homepage.this, file_str9 + "를 다운로드 합니다.", Toast.LENGTH_SHORT).show();
                            Downjob(URL_PRIMARY_SUB + "/" + file_link9, file_str9, file_str9);
                        } else {
                            Toast.makeText(Notice_homepage.this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                        }
                    }
                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URL_PRIMARY_SUB + file_link)));
                }
            }
        });
        cardView10 = (CardView) findViewById(R.id.file_cardview10);
        cardView10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(permissionCheck== PackageManager.PERMISSION_DENIED){
                    // 권한 없음
                    Toast.makeText(Notice_homepage.this, "저장공간 권한이 없습니다.\n설정-어플리케이션-계남고등학교-권한에 들어가서 권한을 체크해주세요 :)", Toast.LENGTH_LONG).show();
                } else {
                    // 권한 있음

                    if (isInternetCon()) {
                        Toast.makeText(Notice_homepage.this, "인터넷에 연결해주세요 :)", Toast.LENGTH_SHORT).show();
                    } else {
                        if (isOnline_Fixed()) { //네트워크 올바른지 체크
                            Toast.makeText(Notice_homepage.this, file_str10 + "를 다운로드 합니다.", Toast.LENGTH_SHORT).show();
                            Downjob(URL_PRIMARY_SUB + "/" + file_link10, file_str10, file_str10);
                        } else {
                            Toast.makeText(Notice_homepage.this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
                        }
                    }
                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URL_PRIMARY_SUB + file_link)));
                }
            }
        });



        if (color == Color.parseColor("#F44336")) { //빨간색 테마
            RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.title_background);
            LinearLayout linearLayout1 = (LinearLayout) findViewById(R.id.content_background);
            LinearLayout linearLayout2 = (LinearLayout) findViewById(R.id.file_background);
            LinearLayout linearLayout3 = (LinearLayout) findViewById(R.id.file_background2);
            LinearLayout linearLayout4 = (LinearLayout) findViewById(R.id.file_background3);
            LinearLayout linearLayout5 = (LinearLayout) findViewById(R.id.file_background4);
            LinearLayout linearLayout6 = (LinearLayout) findViewById(R.id.file_background5);
            LinearLayout linearLayout7 = (LinearLayout) findViewById(R.id.file_background6);
            LinearLayout linearLayout8 = (LinearLayout) findViewById(R.id.file_background7);
            LinearLayout linearLayout9 = (LinearLayout) findViewById(R.id.file_background8);
            LinearLayout linearLayout10 = (LinearLayout) findViewById(R.id.file_background9);
            LinearLayout linearLayout11 = (LinearLayout) findViewById(R.id.file_background10);
            relativeLayout.setBackgroundResource(R.drawable.selector_red);
            linearLayout1.setBackgroundResource(R.drawable.selector_red);
            linearLayout2.setBackgroundResource(R.drawable.selector_red);
            linearLayout3.setBackgroundResource(R.drawable.selector_red);
            linearLayout4.setBackgroundResource(R.drawable.selector_red);
            linearLayout5.setBackgroundResource(R.drawable.selector_red);
            linearLayout6.setBackgroundResource(R.drawable.selector_red);
            linearLayout7.setBackgroundResource(R.drawable.selector_red);
            linearLayout8.setBackgroundResource(R.drawable.selector_red);
            linearLayout9.setBackgroundResource(R.drawable.selector_red);
            linearLayout10.setBackgroundResource(R.drawable.selector_red);
            linearLayout11.setBackgroundResource(R.drawable.selector_red);
             content.setBackgroundResource(R.drawable.selector_red);

        } else if (color == Color.parseColor("#E91E63")){ //분홍색 테마
            RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.title_background);
            LinearLayout linearLayout1 = (LinearLayout) findViewById(R.id.content_background);
            LinearLayout linearLayout2 = (LinearLayout) findViewById(R.id.file_background);
            LinearLayout linearLayout3 = (LinearLayout) findViewById(R.id.file_background2);
            LinearLayout linearLayout4 = (LinearLayout) findViewById(R.id.file_background3);
            LinearLayout linearLayout5 = (LinearLayout) findViewById(R.id.file_background4);
            LinearLayout linearLayout6 = (LinearLayout) findViewById(R.id.file_background5);
            LinearLayout linearLayout7 = (LinearLayout) findViewById(R.id.file_background6);
            LinearLayout linearLayout8 = (LinearLayout) findViewById(R.id.file_background7);
            LinearLayout linearLayout9 = (LinearLayout) findViewById(R.id.file_background8);
            LinearLayout linearLayout10 = (LinearLayout) findViewById(R.id.file_background9);
            LinearLayout linearLayout11 = (LinearLayout) findViewById(R.id.file_background10);
            relativeLayout.setBackgroundResource(R.drawable.selector_pink);
            linearLayout1.setBackgroundResource(R.drawable.selector_pink);
            linearLayout2.setBackgroundResource(R.drawable.selector_pink);
            linearLayout3.setBackgroundResource(R.drawable.selector_pink);
            linearLayout4.setBackgroundResource(R.drawable.selector_pink);
            linearLayout5.setBackgroundResource(R.drawable.selector_pink);
            linearLayout6.setBackgroundResource(R.drawable.selector_pink);
            linearLayout7.setBackgroundResource(R.drawable.selector_pink);
            linearLayout8.setBackgroundResource(R.drawable.selector_pink);
            linearLayout9.setBackgroundResource(R.drawable.selector_pink);
            linearLayout10.setBackgroundResource(R.drawable.selector_pink);
            linearLayout11.setBackgroundResource(R.drawable.selector_pink);
            content.setBackgroundResource(R.drawable.selector_pink);

        } else if (color == Color.parseColor("#FFC107")){ // 연한 주황색(노란색) 테마
            RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.title_background);
            LinearLayout linearLayout1 = (LinearLayout) findViewById(R.id.content_background);
            LinearLayout linearLayout2 = (LinearLayout) findViewById(R.id.file_background);
            LinearLayout linearLayout3 = (LinearLayout) findViewById(R.id.file_background2);
            LinearLayout linearLayout4 = (LinearLayout) findViewById(R.id.file_background3);
            LinearLayout linearLayout5 = (LinearLayout) findViewById(R.id.file_background4);
            LinearLayout linearLayout6 = (LinearLayout) findViewById(R.id.file_background5);
            LinearLayout linearLayout7 = (LinearLayout) findViewById(R.id.file_background6);
            LinearLayout linearLayout8 = (LinearLayout) findViewById(R.id.file_background7);
            LinearLayout linearLayout9 = (LinearLayout) findViewById(R.id.file_background8);
            LinearLayout linearLayout10 = (LinearLayout) findViewById(R.id.file_background9);
            LinearLayout linearLayout11 = (LinearLayout) findViewById(R.id.file_background10);
            relativeLayout.setBackgroundResource(R.drawable.selector_yellow);
            linearLayout1.setBackgroundResource(R.drawable.selector_yellow);
            linearLayout2.setBackgroundResource(R.drawable.selector_yellow);
            linearLayout3.setBackgroundResource(R.drawable.selector_yellow);
            linearLayout4.setBackgroundResource(R.drawable.selector_yellow);
            linearLayout5.setBackgroundResource(R.drawable.selector_yellow);
            linearLayout6.setBackgroundResource(R.drawable.selector_yellow);
            linearLayout7.setBackgroundResource(R.drawable.selector_yellow);
            linearLayout8.setBackgroundResource(R.drawable.selector_yellow);
            linearLayout9.setBackgroundResource(R.drawable.selector_yellow);
            linearLayout10.setBackgroundResource(R.drawable.selector_yellow);
            linearLayout11.setBackgroundResource(R.drawable.selector_yellow);
            content.setBackgroundResource(R.drawable.selector_yellow);

        } else if (color == Color.parseColor("#8BC34A")){ // 연두색 테마
            RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.title_background);
            LinearLayout linearLayout1 = (LinearLayout) findViewById(R.id.content_background);
            LinearLayout linearLayout2 = (LinearLayout) findViewById(R.id.file_background);
            LinearLayout linearLayout3 = (LinearLayout) findViewById(R.id.file_background2);
            LinearLayout linearLayout4 = (LinearLayout) findViewById(R.id.file_background3);
            LinearLayout linearLayout5 = (LinearLayout) findViewById(R.id.file_background4);
            LinearLayout linearLayout6 = (LinearLayout) findViewById(R.id.file_background5);
            LinearLayout linearLayout7 = (LinearLayout) findViewById(R.id.file_background6);
            LinearLayout linearLayout8 = (LinearLayout) findViewById(R.id.file_background7);
            LinearLayout linearLayout9 = (LinearLayout) findViewById(R.id.file_background8);
            LinearLayout linearLayout10 = (LinearLayout) findViewById(R.id.file_background9);
            LinearLayout linearLayout11 = (LinearLayout) findViewById(R.id.file_background10);
            relativeLayout.setBackgroundResource(R.drawable.selector_green);
            linearLayout1.setBackgroundResource(R.drawable.selector_green);
            linearLayout2.setBackgroundResource(R.drawable.selector_green);
            linearLayout3.setBackgroundResource(R.drawable.selector_green);
            linearLayout4.setBackgroundResource(R.drawable.selector_green);
            linearLayout5.setBackgroundResource(R.drawable.selector_green);
            linearLayout6.setBackgroundResource(R.drawable.selector_green);
            linearLayout7.setBackgroundResource(R.drawable.selector_green);
            linearLayout8.setBackgroundResource(R.drawable.selector_green);
            linearLayout9.setBackgroundResource(R.drawable.selector_green);
            linearLayout10.setBackgroundResource(R.drawable.selector_green);
            linearLayout11.setBackgroundResource(R.drawable.selector_green);
            content.setBackgroundResource(R.drawable.selector_green);

        } else if (color == Color.parseColor("#03A9F4")){ //하늘색 테마
            RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.title_background);
            LinearLayout linearLayout1 = (LinearLayout) findViewById(R.id.content_background);
            LinearLayout linearLayout2 = (LinearLayout) findViewById(R.id.file_background);
            LinearLayout linearLayout3 = (LinearLayout) findViewById(R.id.file_background2);
            LinearLayout linearLayout4 = (LinearLayout) findViewById(R.id.file_background3);
            LinearLayout linearLayout5 = (LinearLayout) findViewById(R.id.file_background4);
            LinearLayout linearLayout6 = (LinearLayout) findViewById(R.id.file_background5);
            LinearLayout linearLayout7 = (LinearLayout) findViewById(R.id.file_background6);
            LinearLayout linearLayout8 = (LinearLayout) findViewById(R.id.file_background7);
            LinearLayout linearLayout9 = (LinearLayout) findViewById(R.id.file_background8);
            LinearLayout linearLayout10 = (LinearLayout) findViewById(R.id.file_background9);
            LinearLayout linearLayout11= (LinearLayout) findViewById(R.id.file_background10);
            relativeLayout.setBackgroundResource(R.drawable.selector);
            linearLayout1.setBackgroundResource(R.drawable.selector);
            linearLayout2.setBackgroundResource(R.drawable.selector);
            linearLayout3.setBackgroundResource(R.drawable.selector);
            linearLayout4.setBackgroundResource(R.drawable.selector);
            linearLayout5.setBackgroundResource(R.drawable.selector);
            linearLayout6.setBackgroundResource(R.drawable.selector);
            linearLayout7.setBackgroundResource(R.drawable.selector);
            linearLayout8.setBackgroundResource(R.drawable.selector);
            linearLayout9.setBackgroundResource(R.drawable.selector);
            linearLayout10.setBackgroundResource(R.drawable.selector);
            linearLayout11.setBackgroundResource(R.drawable.selector);
            content.setBackgroundResource(R.drawable.selector);

        } else if (color == Color.parseColor("#3F51B5")){ // 남색 테마
            RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.title_background);
            LinearLayout linearLayout1 = (LinearLayout) findViewById(R.id.content_background);
            LinearLayout linearLayout2 = (LinearLayout) findViewById(R.id.file_background);
            LinearLayout linearLayout3 = (LinearLayout) findViewById(R.id.file_background2);
            LinearLayout linearLayout4 = (LinearLayout) findViewById(R.id.file_background3);
            LinearLayout linearLayout5 = (LinearLayout) findViewById(R.id.file_background4);
            LinearLayout linearLayout6 = (LinearLayout) findViewById(R.id.file_background5);
            LinearLayout linearLayout7 = (LinearLayout) findViewById(R.id.file_background6);
            LinearLayout linearLayout8 = (LinearLayout) findViewById(R.id.file_background7);
            LinearLayout linearLayout9 = (LinearLayout) findViewById(R.id.file_background8);
            LinearLayout linearLayout10 = (LinearLayout) findViewById(R.id.file_background9);
            LinearLayout linearLayout11 = (LinearLayout) findViewById(R.id.file_background10);
            relativeLayout.setBackgroundResource(R.drawable.selector_indigo);
            linearLayout1.setBackgroundResource(R.drawable.selector_indigo);
            linearLayout2.setBackgroundResource(R.drawable.selector_indigo);
            linearLayout3.setBackgroundResource(R.drawable.selector_indigo);
            linearLayout4.setBackgroundResource(R.drawable.selector_indigo);
            linearLayout5.setBackgroundResource(R.drawable.selector_indigo);
            linearLayout6.setBackgroundResource(R.drawable.selector_indigo);
            linearLayout7.setBackgroundResource(R.drawable.selector_indigo);
            linearLayout8.setBackgroundResource(R.drawable.selector_indigo);
            linearLayout9.setBackgroundResource(R.drawable.selector_indigo);
            linearLayout10.setBackgroundResource(R.drawable.selector_indigo);
            linearLayout11.setBackgroundResource(R.drawable.selector_indigo);
            content.setBackgroundResource(R.drawable.selector_indigo);

        } else { // 기본색(하늘색)테마
            RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.title_background);
            LinearLayout linearLayout1 = (LinearLayout) findViewById(R.id.content_background);
            LinearLayout linearLayout2 = (LinearLayout) findViewById(R.id.file_background);
            LinearLayout linearLayout3 = (LinearLayout) findViewById(R.id.file_background2);
            LinearLayout linearLayout4 = (LinearLayout) findViewById(R.id.file_background3);
            LinearLayout linearLayout5 = (LinearLayout) findViewById(R.id.file_background4);
            LinearLayout linearLayout6 = (LinearLayout) findViewById(R.id.file_background5);
            LinearLayout linearLayout7 = (LinearLayout) findViewById(R.id.file_background6);
            LinearLayout linearLayout8 = (LinearLayout) findViewById(R.id.file_background7);
            LinearLayout linearLayout9 = (LinearLayout) findViewById(R.id.file_background8);
            LinearLayout linearLayout10 = (LinearLayout) findViewById(R.id.file_background9);
            LinearLayout linearLayout11= (LinearLayout) findViewById(R.id.file_background10);
            relativeLayout.setBackgroundResource(R.drawable.selector);
            linearLayout1.setBackgroundResource(R.drawable.selector);
            linearLayout2.setBackgroundResource(R.drawable.selector);
            linearLayout3.setBackgroundResource(R.drawable.selector);
            linearLayout4.setBackgroundResource(R.drawable.selector);
            linearLayout5.setBackgroundResource(R.drawable.selector);
            linearLayout6.setBackgroundResource(R.drawable.selector);
            linearLayout7.setBackgroundResource(R.drawable.selector);
            linearLayout8.setBackgroundResource(R.drawable.selector);
            linearLayout9.setBackgroundResource(R.drawable.selector);
            linearLayout10.setBackgroundResource(R.drawable.selector);
            linearLayout11.setBackgroundResource(R.drawable.selector);
            content.setBackgroundResource(R.drawable.selector);
        }

        GETNOTICE_SUB = getIntent().getStringExtra("url"); // NoticeActivity에서 받아온 뒷부분의 url을 저장함

        Toolbar mToolbar = (Toolbar) findViewById(R.id.mToolbar);
        setSupportActionBar(mToolbar);

        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setHomeButtonEnabled(true);
            mActionBar.setDisplayHomeAsUpEnabled(true);

            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }



        url = URL_PRIMARY_SUB + GETNOTICE_SUB; //파싱하기전 PRIMARY URL 과 공지사항 URL 을 합쳐 완전한 URL 을만든다.

        if(isInternetCon()) { //false 반환시 if 문안의 로직 실행
            Toast.makeText(Notice_homepage.this, "인터넷에 연결해주세요 :)", Toast.LENGTH_SHORT).show();
        } else { //인터넷 체크 통과시 실행할 로직

            if (isOnline_Fixed()) { // 네트워크가 올바르면
                try {
                    process(); //네트워크 관련은 따로 쓰레드를 생성해야 UI 쓰레드와 겹치지 않는다. 그러므로 Thread 가 선언된 process 메서드를 호출한다.
                } catch (Exception e) {
                    Log.d("ERROR", e + "");
                }
            } else {
                Toast.makeText(Notice_homepage.this, "네트워크가 올바르지 않아요 :(", Toast.LENGTH_SHORT).show();
            }
        }



    }



/*
    public void onClick(View v) {
        // 클릭한 포지션의 데이터를 가져온다.
        String URL_BCS = null; //가져온 데이터 중 url 부분만 적출해낸다.
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URL_PRIMARY_SUB + file_str))); //적출해낸 url 을 이용해 URL_PRIMARY 와 붙이고
    }
*/


    private void process() throws IOException {

        new Thread() {

            @Override
            public void run() {

                Handler Progress = new Handler(Looper.getMainLooper()); //네트워크 쓰레드와 별개로 따로 핸들러를 이용하여 쓰레드를 생성한다.
                Progress.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog = ProgressDialog.show(Notice_homepage.this, "", "게시판 정보를 가져오는중 이에요");
                    }
                }, 0);


                /*    public void handleMessage(Message msg) {
                    progressDialog.dismiss();
                    String message = null;
                    switch (msg.what) {

                        case 1:
                            message ="불러오는 데 실패했어요 :(";
                            break;
                    }}
*/
                try {
                    URL = new URL(url);
                    InputStream html = URL.openStream();
                    source = new Source(new InputStreamReader(html, "UTF-8")); //소스를 UTF-8 인코딩으로 불러온다.
                    source.fullSequentialParse(); //순차적으로 구문분석
                } catch (Exception e) {
                    Log.e("ERROR", e + "");
                    e.printStackTrace();
               //     Progress.sendEmptyMessage(1);
//                    Toast.makeText(Notice_homepage.this, "불러오기에 실패했어요 :(", Toast.LENGTH_SHORT).show();
                }

                try {

                    List<StartTag> tabletags = source.getAllStartTags(HTMLElementName.DIV); // DIV 타입의 모든 태그들을 불러온다.

                    for (int arrnum = 0; arrnum < tabletags.size(); arrnum++) { //DIV 모든 태그중 bbsContent 태그가 몇번째임을 구한다.


                        if (tabletags.get(arrnum).toString().equals("<div class=\"bbsContent\">")) {
                            BBSlocate = arrnum; //DIV 클래스가 bbsContent 면 arrnum 값을 BBSlocate 로 몇번째인지 저장한다.
                            Log.d("BBSLOCATES", arrnum + ""); //arrnum 로깅
                            break;

                        }// else {}
                    }


                    Element BBS_DIV = (Element) source.getAllElements(HTMLElementName.DIV).get(BBSlocate); //BBSlocate 번째 의 DIV 를 모두 가져온다.
                    Element BBS_TABLE = (Element) BBS_DIV.getAllElements(HTMLElementName.TABLE).get(0); //테이블 접속
                    //               Element BBS_TR= (Element) BBS_TABLE.getAllElements(HTMLElementName.TR).get(0); //테이블 접속


                    for (int C_TR = 0; C_TR < BBS_TABLE.getAllElements(HTMLElementName.TR).size(); C_TR++) { //여기서는 이제부터 게시된 게시물 데이터를 불러와 게시판 인터페이스를 구성할 것이다.



                        //   try {

                        Element BBS_TR = (Element) BBS_TABLE.getAllElements(HTMLElementName.TR).get(C_TR); //TR 접속


                        Element BBS_TYPE = (Element) BBS_TR.getAllElements(HTMLElementName.TH).get(0); //TR의 1번째 TH 값을 불러옴

                        Element BBS_TD = (Element) BBS_TR.getAllElements(HTMLElementName.TD).get(0); //TR의 1번째 TD 값을 불러옴



                        /*파일 다운로드 관련 코드 시작*/
                        if (BBS_TYPE.getContent().toString().contains("첨부파일 1")) { //TH의 값이 "첨부파일 1"이면

                            Element download_file = (Element) BBS_TD.getAllElements(HTMLElementName.A).get(0); //TD의 A의 값을 불러옴
                            file_link = download_file.getAttributeValue("href"); //A 태그속 href의 값(url)을 불러옴
                            if (file_link != null) { //url이 null이 아니면
                                file_str = download_file.getContent().toString(); //파일이름을 불러온 뒤 문자열로 변환
                            }
                        }
                        if (BBS_TYPE.getContent().toString().contains("첨부파일 2")) { //위의 주석과 같은 원리이다.

                            Element download_file = (Element) BBS_TD.getAllElements(HTMLElementName.A).get(0);
                            file_link2 = download_file.getAttributeValue("href");
                            if (file_link2 != null) {
                                file_str2 = download_file.getContent().toString();
                            }
                        }
                        if (BBS_TYPE.getContent().toString().contains("첨부파일 3")) {

                            Element download_file = (Element) BBS_TD.getAllElements(HTMLElementName.A).get(0);
                            file_link3 = download_file.getAttributeValue("href");
                            if (file_link3 != null) {
                                file_str3 = download_file.getContent().toString();
                            }
                        }
                        if (BBS_TYPE.getContent().toString().contains("첨부파일 4")) {

                            Element download_file = (Element) BBS_TD.getAllElements(HTMLElementName.A).get(0);
                            file_link4 = download_file.getAttributeValue("href");
                            if (file_link4 != null) {
                                file_str4 = download_file.getContent().toString();
                            }
                        }
                        if (BBS_TYPE.getContent().toString().contains("첨부파일 5")) {

                            Element download_file = (Element) BBS_TD.getAllElements(HTMLElementName.A).get(0);
                            file_link5 = download_file.getAttributeValue("href");
                            if (file_link5 != null) {
                                file_str5 = download_file.getContent().toString();
                            }
                        }
                        if (BBS_TYPE.getContent().toString().contains("첨부파일 6")) {

                            Element download_file = (Element) BBS_TD.getAllElements(HTMLElementName.A).get(0);
                            file_link6 = download_file.getAttributeValue("href");
                            if (file_link6 != null) {
                                file_str6 = download_file.getContent().toString();
                            }
                        }
                        if (BBS_TYPE.getContent().toString().contains("첨부파일 7")) {

                            Element download_file = (Element) BBS_TD.getAllElements(HTMLElementName.A).get(0);
                            file_link7 = download_file.getAttributeValue("href");
                            if (file_link7 != null) {
                                file_str7 = download_file.getContent().toString();
                            }
                        }
                        if (BBS_TYPE.getContent().toString().contains("첨부파일 8")) {

                            Element download_file = (Element) BBS_TD.getAllElements(HTMLElementName.A).get(0);
                            file_link8 = download_file.getAttributeValue("href");
                            if (file_link8 != null) {
                                file_str8 = download_file.getContent().toString();
                            }
                        }
                        if (BBS_TYPE.getContent().toString().contains("첨부파일 9")) {

                            Element download_file = (Element) BBS_TD.getAllElements(HTMLElementName.A).get(0);
                            file_link9 = download_file.getAttributeValue("href");
                            if (file_link9 != null) {
                                file_str9 = download_file.getContent().toString();
                            }
                        }
                        if (BBS_TYPE.getContent().toString().contains("첨부파일 10")) {

                            Element download_file = (Element) BBS_TD.getAllElements(HTMLElementName.A).get(0);
                            file_link10 = download_file.getAttributeValue("href");
                            if (file_link10 != null) {
                                file_str10 = download_file.getContent().toString();
                            }
                        }
                        /*파일 다운로드 관련 끝*/





                        String BCS_type = BBS_TYPE.getContent().toString(); //TH(Type)의 값을 문자열로 변환


                        if (BBS_TYPE.getContent().toString().contains("제목")) { //TH의 값이 "제목"이면
                            title_str = BBS_TD.getContent().toString(); // title_str에 TD를 문자열로 변환 후 저장
                        }
                        if (BBS_TYPE.getContent().toString().contains("이름")) {
                            writer_str = "작성자 : " + BBS_TD.getContent().toString();
                        }
                        if (BBS_TYPE.getContent().toString().contains("등록일")) {
                            date_str = BBS_TD.getContent().toString();
                        }
                        if (BBS_TYPE.getContent().toString().contains("내용")) {
                            content_str = BBS_TD.getContent().toString().trim();
                        }

                        Log.d("BCSARR", "타입:" + BCS_type + "\n제목:" + title_str + "\n다운:" + file_str + "\n글쓴이:" + writer_str + "\n날짜:" + date_str + "\n내용:" + content_str/* + "\n"+ BCS_abc4*/);

                    }

                } catch(Exception e) {
                    Log.e("ERROR", e + "");
                    e.printStackTrace();
                 //   Progress.sendEmptyMessage(1);
                }
                Handler mHandler = new Handler(Looper.getMainLooper());
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //모든 작업이 끝나면 리스트 갱신


                        if (title_str != null && writer_str != null && date_str != null && content_str != null) { // 제목, 작성자, 날짜, 내용이 모두 null값이 아니면
                            NestedScrollView nestedScrollView = (NestedScrollView) findViewById(R.id.nested_scrollview);
                            nestedScrollView.setVisibility(View.VISIBLE); //보이게 한다

                            if (file_link != null) {
                                cardView.setVisibility(View.VISIBLE); //첨부파일1 보이게
                            }
                            if (file_link2 != null) {
                                cardView2.setVisibility(View.VISIBLE); //첨부파일2 보이게
                            }
                            if (file_link3 != null) {
                                cardView3.setVisibility(View.VISIBLE); //첨부파일3 보이게
                            }
                            if (file_link4 != null) {
                                cardView4.setVisibility(View.VISIBLE); //첨부파일4 보이게
                            }
                            if (file_link5 != null) {
                                cardView5.setVisibility(View.VISIBLE); //첨부파일5 보이게
                            }
                            if (file_link6 != null) {
                                cardView6.setVisibility(View.VISIBLE); //첨부파일6 보이게
                            }
                            if (file_link7 != null) {
                                cardView7.setVisibility(View.VISIBLE); //첨부파일7 보이게
                            }
                            if (file_link8 != null) {
                                cardView8.setVisibility(View.VISIBLE); //첨부파일8 보이게
                            }
                            if (file_link9 != null) {
                                cardView9.setVisibility(View.VISIBLE); //첨부파일9 보이게
                            }
                            if (file_link10 != null) {
                                cardView10.setVisibility(View.VISIBLE); //첨부파일10 보이게
                            }

                            title.setText(Html.fromHtml(title_str));
                            writer.setText(Html.fromHtml(writer_str));
                            date.setText(Html.fromHtml(date_str));
                            content.setMovementMethod(LinkMovementMethod.getInstance());
                            content.setText(Html.fromHtml(content_str));
                            file.setText(" " + file_str);
                            file2.setText(" " +file_str2);
                            file3.setText(" " + file_str3);
                            file4.setText(" " + file_str4);
                            file5.setText(" " + file_str5);
                            file6.setText(" " + file_str6);
                            file7.setText(" " + file_str7);
                            file8.setText(" " + file_str8);
                            file9.setText(" " + file_str9);
                            file10.setText(" " + file_str10);

                        } else {
                            Toast.makeText(Notice_homepage.this, "이 게시글은 불러올 수 없는 관계로 홈페이지에서 직접 보셔야 해요..", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                        }

                        progressDialog.dismiss(); //모든 작업이 끝나면 다이어로그 종료
                    }
                }, 0);



            }

        }.start();


    }

    private File makeDirectory(String dir_path){
        File dir = new File(dir_path);
        if (!dir.exists()){
            dir.mkdirs();
        }
        return dir;
    }




    public void Downjob(String url, String filename, String name){
        File direct = new File(String.valueOf(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)));

        if (!direct.exists()) {
            direct.mkdirs();
        } else {

                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                request.setDescription(filename);
                request.setTitle(name); // ""안에는 저장될 파일의 이름을 쓰시는게 적합합니다
                // in order for this if to run, you must use the android 3.2 to compile your app

       /*     MimeTypeMap map = MimeTypeMap.getSingleton();
            String ext = MimeTypeMap.getFileExtensionFromUrl((filename));
            String type = map.getMimeTypeFromExtension(ext);

            request.setMimeType(type);
*/
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    request.allowScanningByMediaScanner();
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                }
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, filename);


                // get download service and enqueue file
                DownloadManager dm = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                dm.enqueue(request);

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_online, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_notice) {
            new MaterialDialog.Builder(this)
                    .title("주의사항")
                    .content("\"파일을 열 수 없습니다.\"가 발생할 경우 해당 파일을 열 수 있는 리더앱을 따로 설치해주셔야 해요")
                    .negativeText("닫기")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        }
                    })
                    .show();
        } else if (id == R.id.action_settings) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }

        return super.onOptionsItemSelected(item);
    }


private boolean isInternetCon() {
        cManager=(ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        mobile = cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE); //모바일 데이터 여부
        wifi = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI); //와이파이 여부
        return !mobile.isConnected() && !wifi.isConnected(); //결과값을 리턴
    }
}