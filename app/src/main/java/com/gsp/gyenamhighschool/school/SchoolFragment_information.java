package com.gsp.gyenamhighschool.school;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.gsp.gyenamhighschool.R;

public class SchoolFragment_information extends Fragment {

    public static Fragment getInstance(int index) {
        SchoolFragment_information schoolFragment_information = new SchoolFragment_information();

        Bundle args = new Bundle();
        args.putInt("code", index);
        schoolFragment_information.setArguments(args);

        return schoolFragment_information;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.school_information, container, false);

        ImageView imageView = (ImageView) rootView.findViewById(R.id.info_image_);
        Glide.with(this).load("http://www.gyenam.hs.kr/_design/html/cur/images/010900.jpg").placeholder(R.drawable.loading).error(R.drawable.error).into(imageView);

        LinearLayout linearLayout1 = (LinearLayout) rootView.findViewById(R.id.tel);
        LinearLayout linearLayout2 = (LinearLayout) rootView.findViewById(R.id.fax);
        LinearLayout linearLayout3 = (LinearLayout) rootView.findViewById(R.id.address);
        CardView cardView = (CardView) rootView.findViewById(R.id.map_way);
        linearLayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:032-322-9176"));
                startActivity(intent);
            }
        });
        linearLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:032-322-9175"));
                startActivity(intent);
            }
        });
        linearLayout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getActivity(), School_Image_zoom.class);
                intent1.putExtra("code","map");
                startActivity(intent1);
            }
        });
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getActivity(), School_Image_zoom.class);
                intent1.putExtra("code","map_way");
                startActivity(intent1);
            }
        });



        return rootView;
    }
}