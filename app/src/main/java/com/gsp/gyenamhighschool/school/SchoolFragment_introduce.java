package com.gsp.gyenamhighschool.school;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.gsp.gyenamhighschool.R;

public class SchoolFragment_introduce extends Fragment {

    private View view;
    private ViewGroup rowContainer;

    public static Fragment getInstance(int index) {
        SchoolFragment_introduce schoolFragment_introduce = new SchoolFragment_introduce();

        Bundle args = new Bundle();
        args.putInt("code", index);
        schoolFragment_introduce.setArguments(args);

        return schoolFragment_introduce;
    }

    public void fillRow(View view, String title, String content) {

        TextView textView1 = (TextView) view.findViewById(R.id.info_title);
        textView1.setText(title);
        TextView textView2 = (TextView) view.findViewById(R.id.info_content_text);
        textView2.setText(content);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.school_introduce, container, false);

        rowContainer = (LinearLayout) rootView.findViewById(R.id.rowContainer_info);


        view = rowContainer.findViewById(R.id.say_hello);
        fillRow(view, "학교장 인사말", "이곳은 『正道 誠實』을 교훈으로\n항상 좋은 질문(?) 큰 감동(!) 참 잘했다는 마침표(.)가 있는 학교, 행복한 교육을 이루어 가는 조화롭고\n격조 있는 계남고등학교 꽃밭입니다.\n\n「미래를 주도하는 창의적 인재육성」의 꽃밭에서 적당한 거름과 물과 바람과 햇빛을 받으며 각각의 아름다운 꽃을 피워가는 행복을 함께 공감하시기 바랍니다.\n\n2014. 9.\n\n계남고등학교장 나길수(羅吉洙)");
        ImageView imageView = (ImageView) view.findViewById(R.id.info_image);
        Glide.with(this).load("http://www.gyenam.hs.kr/_design/html/cur/images/boss.jpg").placeholder(R.drawable.loading).error(R.drawable.error).into(imageView);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getActivity(),School_Image_zoom.class);
                intent1.putExtra("code", "say_hello");
                startActivity(intent1);
            }
        });


        view = rowContainer.findViewById(R.id.gyo_hun);
        fillRow(view, "교훈", "정도성실\n\n계남고인으로서 지켜야 할 마음가짐.\n正道成實의 正은 바를정, 道은 길도,\n成은 이룰성, 實은 열매실.");
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getActivity(), School_Image_zoom.class);
                intent1.putExtra("code", "gyo_hun");
                startActivity(intent1);
            }
        });
        ImageView imageView2 = (ImageView) view.findViewById(R.id.info_image);
        Glide.with(this).load("http://www.gyenam.hs.kr/_design/html/cur/images/010300_1.jpg").placeholder(R.drawable.loading).error(R.drawable.error).into(imageView2);


        view = rowContainer.findViewById(R.id.gyo_pyo);
        fillRow(view, "교표", "校木인 월계수나무와 월계관을 상징 녹색\n- 꿈과 稀望 상징\n펼쳐진 책을 상징(배움의 터전)\n교명인 桂南의 桂는 계수나무계, 南은 남녁남");
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getActivity(), School_Image_zoom.class);
                intent1.putExtra("code","gyo_pyo");
                startActivity(intent1);
            }
        });
        ImageView imageView3 = (ImageView) view.findViewById(R.id.info_image);
        Glide.with(this).load("http://www.gyenam.hs.kr/_design/html/cur/images/010300_2.jpg").placeholder(R.drawable.loading).error(R.drawable.error).into(imageView3);



        view = rowContainer.findViewById(R.id.gyo_flower);
        fillRow(view, "교화", "장미\n\n붉은 색은 젊음과 이상실현을 위한 열정을 상징.\n아름다운 꽃모양은 계남고인으로서\n외면적 아름다움뿐 아니라 내면적 아름다움을 함께 지녀야 한다는 의미를 내포.\n또한 장미의 가시를 통한 교훈은 순간적 유혹과 감정을\n절제하고 이성적으로 행동할 것을 경계함.");
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getActivity(), School_Image_zoom.class);
                intent1.putExtra("code","gyo_flower");
                startActivity(intent1);
            }
        });
        ImageView imageView4 = (ImageView) view.findViewById(R.id.info_image);
        Glide.with(this).load("http://www.gyenam.hs.kr/_design/html/cur/images/010300_3.jpg").placeholder(R.drawable.loading).error(R.drawable.error).into(imageView4);



        view = rowContainer.findViewById(R.id.gyo_tree);
        fillRow(view, "교목", "계수나무\n\n학교 건물이 들어선 옛지명이\n부천군 계남면(富川郡 桂南面)이었던\n명칭을 살리고, 본교 학생들이 각자가\n훌륭한 인생을 펼쳐서 모두 월계관을 쓸 수 있는\n인재가 되라는 의미를 함축.\n고난과 역경을 물리치고 극기의 자세로\n열심히 자신의 삶을 개척하고 항상 명예를\n존중하는 사람이 되라는 의미도 내포");
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getActivity(), School_Image_zoom.class);
                intent1.putExtra("code","gyo_tree");
                startActivity(intent1);
            }
        });
        ImageView imageView5 = (ImageView) view.findViewById(R.id.info_image);
        Glide.with(this).load("http://www.gyenam.hs.kr/_design/html/cur/images/010300_4.jpg").placeholder(R.drawable.loading).error(R.drawable.error).into(imageView5);


/*
        view = rowContainer.findViewById(R.id.map);
        fillRow(view, "오시는 길", "▶  신중동역 - 3번 출구에서 도보 15분 (중동신도시 그린타운 방향)\n" +
                "▶  중동역 북부 - 2번 출구에서 도보 15분 (중동신도시 그린타운 방향)\n" +
                "▶  부천역 북부 - 4번 출구에서 소신여객 차고로 이동\n" +
                "버스 5-4번(10분) 계남고 앞 하차");
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getActivity(), School_Image_zoom.class);
                intent1.putExtra("code","map");
                startActivity(intent1);
            }
        });
        ImageView imageView6 = (ImageView) view.findViewById(R.id.info_image);
        Glide.with(this).load("http://www.gyenam.hs.kr/_design/html/cur/images/010900.jpg").placeholder(R.drawable.loading).error(R.drawable.error).into(imageView6);
*/
        return rootView;
    }
}
