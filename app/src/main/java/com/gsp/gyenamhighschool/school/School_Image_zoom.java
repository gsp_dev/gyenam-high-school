package com.gsp.gyenamhighschool.school;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gsp.gyenamhighschool.R;

import org.w3c.dom.Text;

import uk.co.senab.photoview.PhotoViewAttacher;

public class School_Image_zoom extends AppCompatActivity {


    PhotoViewAttacher mAttacher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.school_image_zoom);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.mToolbar);
        setSupportActionBar(mToolbar);

        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setHomeButtonEnabled(true);
            mActionBar.setDisplayHomeAsUpEnabled(true);

            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        Intent intent = getIntent();
        String code = intent.getStringExtra("code");

        ImageView imageView = (ImageView) findViewById(R.id.img_zoom);
        TextView textView = (TextView) findViewById(R.id.description_zoom);

        switch (code) {
            case "say_hello":
                Glide.with(this).load("http://www.gyenam.hs.kr/_design/html/cur/images/boss.jpg").placeholder(R.drawable.loading).error(R.drawable.error).into(imageView);
                break;
            case "gyo_hun":
                Glide.with(this).load("http://www.gyenam.hs.kr/_design/html/cur/images/010300_1.jpg").placeholder(R.drawable.loading).error(R.drawable.error).into(imageView);
                break;
            case "gyo_pyo":
                Glide.with(this).load("http://www.gyenam.hs.kr/_design/html/cur/images/010300_2.jpg").placeholder(R.drawable.loading).error(R.drawable.error).into(imageView);
                break;
            case "gyo_flower":
                Glide.with(this).load("http://www.gyenam.hs.kr/_design/html/cur/images/010300_3.jpg").placeholder(R.drawable.loading).error(R.drawable.error).into(imageView);
                break;
            case "gyo_tree":
                Glide.with(this).load("http://www.gyenam.hs.kr/_design/html/cur/images/010300_4.jpg").placeholder(R.drawable.loading).error(R.drawable.error).into(imageView);
                break;
            case "map":
                Glide.with(this).load("http://www.gyenam.hs.kr/_design/html/cur/images/010900.jpg").placeholder(R.drawable.loading).error(R.drawable.error).into(imageView);
                textView.setText("경기도 부천시 부흥로 261 중2동 1185-1");
                break;
            case "map_way":
                Glide.with(this).load("http://www.gyenam.hs.kr/_design/html/cur/images/010900.jpg").placeholder(R.drawable.loading).error(R.drawable.error).into(imageView);
                textView.setText("▶  신중동역 - 3번 출구에서 도보 15분 (중동신도시 그린타운 방향)\n" +
                        "▶  중동역 북부 - 2번 출구에서 도보 15분 (중동신도시 그린타운 방향)\n" +
                        "▶  부천역 북부 - 4번 출구에서 소신여객 차고로 이동\n" +
                        "버스 5-4번(10분) 계남고 앞 하차");
                textView.setTextAppearance(this, android.support.v7.appcompat.R.style.TextAppearance_AppCompat_Small);
                break;
        }

        mAttacher = new PhotoViewAttacher(imageView);
        mAttacher.setScaleType(ImageView.ScaleType.FIT_CENTER);

    }
}
