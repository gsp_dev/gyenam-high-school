package com.gsp.gyenamhighschool.tool;

import java.math.BigDecimal;


public class HiddenCode {

    private static final int HIDDEN_FIRST_CODE = PrivateCode.MY_HIDDEN_FIRST_CODE;
    private static final int HIDDEN_SECOND_CODE = PrivateCode.MY_HIDDEN_SECOND_CODE;

    public static String getHiddenCode() {
        BigDecimal bigDecimal = new BigDecimal(getFirstHiddenCode())
                .multiply(new BigDecimal(getSecondHiddenCode()));
        return bigDecimal.toString();
    }

    private static int getFirstHiddenCode() {
        SecurityXor securityXor = new SecurityXor();
        return securityXor.getSecurityXor(securityXor.getSecurityXor(719, HIDDEN_FIRST_CODE), 284); //416
    }

    private static int getSecondHiddenCode() {
        SecurityXor securityXor = new SecurityXor();
        return securityXor.getSecurityXor(securityXor.getSecurityXor(198, HIDDEN_SECOND_CODE), 647); //2033
    }
}