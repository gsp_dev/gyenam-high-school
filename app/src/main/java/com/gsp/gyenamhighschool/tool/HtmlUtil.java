package com.gsp.gyenamhighschool.tool;


public class HtmlUtil {

    public static String RemoveHTMLTag(String changeStr) {
        if (changeStr != null && !changeStr.equals("")) {
            changeStr = changeStr.replaceAll("<(/)?([a-zA-Z]*)(\\s[a-zA-Z]*=[^>]*)?(\\s)*(/)?>", "");
        } else {
            changeStr = "";
        }
        return changeStr;
    }

    public static String toText(String str) {
        if (str == null)
            return null;

        String returnStr = str;
        returnStr = returnStr.replaceAll("<br>", "\n");
        returnStr = returnStr.replaceAll("&gt;", ">");
        returnStr = returnStr.replaceAll("&lt;", "<");
        returnStr = returnStr.replaceAll("&quot;", "\"");
        returnStr = returnStr.replaceAll("&nbsp;", " ");
        returnStr = returnStr.replaceAll("&amp;", "&");
        returnStr = returnStr.replaceAll("&#34;", "\"");
        returnStr = returnStr.replaceAll("&#39;", "\'");
        returnStr = returnStr.replaceAll("&#33;","!");
        returnStr = returnStr.replaceAll("&#35;","#");
        returnStr = returnStr.replaceAll("&#36;","$");
        returnStr = returnStr.replaceAll("&#37;","%");
        returnStr = returnStr.replaceAll("&#38;","&");
        returnStr = returnStr.replaceAll("&#40;","(");
        returnStr = returnStr.replaceAll("&#41;",")");
        returnStr = returnStr.replaceAll("&#42;","*");
        returnStr = returnStr.replaceAll("&#43;","+");
        returnStr = returnStr.replaceAll("&#44;",",");
        returnStr = returnStr.replaceAll("&#45;","-");
        returnStr = returnStr.replaceAll("&#46;",".");
        returnStr = returnStr.replaceAll("&#47","/");
        returnStr = returnStr.replaceAll("&#58;",":");
        returnStr = returnStr.replaceAll("&#59;",";");
        returnStr = returnStr.replaceAll("&#60;","<");
        returnStr = returnStr.replaceAll("&#61;","=");
        returnStr = returnStr.replaceAll("&#62;",">");
        returnStr = returnStr.replaceAll("&#63;","?");
        returnStr = returnStr.replaceAll("&#64;","@");
        returnStr = returnStr.replaceAll("&#91;","[");
        returnStr = returnStr.replaceAll("&#92;","\\");
        returnStr = returnStr.replaceAll("&#93;","]");
        returnStr = returnStr.replaceAll("&#94;","^");
        returnStr = returnStr.replaceAll("&#95;","_");
        returnStr = returnStr.replaceAll("&#96;","`");
        returnStr = returnStr.replaceAll("&#123;","{");
        returnStr = returnStr.replaceAll("&#124;","|");
        returnStr = returnStr.replaceAll("&#125;","}");
        returnStr = returnStr.replaceAll("&#126;","~");


        // returnStr = returnStr.replaceAll("&#34;", "\"");

        return returnStr;
    }
}